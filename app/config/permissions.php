<?php

return array(

	'Global' => array(
		array(
			'permission' => 'admin',
			'label'      => 'Administrator',
		),
		array(
			'permission' => 'user',
			'label'      => 'User',
		),
	),

);
