<?php

use Cartalyst\Sentry\Throttling\Eloquent\Throttle;

class AuthController extends BaseController {

	public function __construct()
	{
		parent::__construct();
	}

	public function getUser()
	{
		$user = Sentry::getUser();

		if ($user)
		{
			return Response::json($user, 200);
		}
		else
		{
			return NULL;
		}
	}

	public function isLoggedin()
	{
		if (!Sentry::check())
		{
			$httpResponse = new HttpResponse();
			$httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('auth/message.timeout_session_title'), Lang::get('auth/message.timeout_session'));

			return Response::json($httpResponse->getAsArray(), 403);
		}
		else
		{
			$httpResponse = new HttpResponse();
			$httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('general.ok'));

			return Response::json(array("response" => TRUE), 200);
		}
	}

	/**
	 * Do Login
	 *
	 * @return HttpResponse
	 */
	public function login()
	{
		$this->beforeFilter('checkCSRF');

		// Get the Throttle Provider and enable this feature
		$throttleProvider = Sentry::getThrottleProvider();
		$throttleProvider->enable();

		Throttle::setAttemptLimit(5);
		Throttle::setSuspensionTime(5);

		try
		{
			$credentials = array(
				'username' => Input::get('username'),
				'password' => Input::get('password'),
			);

			// Try to login
			Sentry::authenticate($credentials, Input::get('remember', FALSE));

			$user = Sentry::getUserProvider()->findByLogin(Input::get('username'));

			if (Sentry::getUser()->hasAccess('admin'))
			{
				$user->is_admin = 'y';
			}
			else
			{
				$user->is_admin = 'n';
			}

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::AUTH_LOGIN,
				'user_id'       => $user->id,
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));


			if (File::exists(base_path() . '/' . Config::get('app.dir_image_user') . $user->id . '.jpg'))
			{
				$user->has_portrait = 'y';
			}
			else
			{
				$user->has_portrait = 'n';
			}

			// controller.js -> WebsiteLoginController: AuthService.save()
			return Response::json(['user' => $user], 200);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.login_failed'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('general.attention'), Lang::get('auth/message.account_not_activated'));
		}
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.account_suspended'));
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.account_banned'));
		}

		return Response::json($this->httpResponse->getAsArray(), 401);
	}

	/**
	 * Do Logoff
	 *
	 * @return HttpResponse
	 */
	public function logoff()
	{
		// Event auslösen
		$user = Sentry::getUser();
		if ($user)
		{
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::AUTH_LOGOFF,
				'user_id'       => $user->id,
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}

		// Log the user out
		Sentry::logout();

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('auth/message.logoff_success_title'), Lang::get('auth/message.logoff_success'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	/**
	 * Register a new user
	 *
	 * @return HttpResponse
	 */
	public function register()
	{
		$this->beforeFilter('checkCSRF');


		// Obwohl der Client (AngularJS) diese Überprüfungen bereits gemacht hat, zur Sicherheit nochmals prüfen. Man weiss nie!
		$rules = array(
			'username'         => 'required|min:4|unique:users',
			'first_name'       => 'required',
			'last_name'        => 'required',
			'email'            => 'required|email|unique:users',
			'password'         => 'required|between:6,32',
			'password_confirm' => 'required|same:password',
		);


		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Registrierung fehlgeschlagen!");

			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		try
		{
			// Register the user
			$user = Sentry::register(array(
				'username'   => Input::get('username'),
				'first_name' => Input::get('first_name'),
				'last_name'  => Input::get('last_name'),
				'email'      => Input::get('email'),
				'password'   => Input::get('password'),
				'sex'        => Input::get('sex'),
			));

			// add default group: User
			$group = Sentry::findGroupByName('User');
			$user->addGroup($group);


			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::AUTH_REGISTER,
				'user_id'       => $user->id,
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));



			// Data to be used on the email view
			$data = array(
				'user'          => $user,
				'activationUrl' => URL::to('/') . '/#/auth/activateaccount/' . $user->getActivationCode(),
			);

			if (App::environment('local'))
			{
				Log::info("url=" . $data['activationUrl']);
			}

			// Send the activation code through email
			Mail::send('emails.register-activate', $data, function ($m) use ($user)
			{
				$m->to($user->email, $user->first_name . ' ' . $user->last_name);
				$m->subject(Config::get('app.name') . ': ' . Lang::get('auth/message.register_success_email_subject'));
			});

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('auth/message.register_success'));
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.account_already_exists'));
		}

		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	public function getGroups()
	{

		// Get a list of all the available groups
		$groups = Sentry::getGroupProvider()->findAll();

		return Response::json($groups, 200);
	}

	/**
	 * Check whether the user has admin rights
	 */
	public function isAdmin()
	{
		// Logged in?
		if (Sentry::check())
		{
			try
			{
				// has Admin rights?
				if (Sentry::getUser()->hasAccess('admin'))
				{
					return Response::json(array("response" => TRUE), 200);
				}
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				return Response::json(array("response" => FALSE), 200);
			}
		}

		return Response::json(array("response" => FALSE), 200);
	}

	/**
	 * Check whether the username is unique
	 *
	 * @return mixed
	 */
	public function isUsernameUnique()
	{
		try
		{
			if (Input::get('except') === TRUE)
			{
				$this->beforeFilter('checkAuth');

				// Der Benutzername des angemeldeten Benutzers nicht prüfen
				$userLoggedin = Sentry::getUser();
				$user         = Sentry::getUserProvider()->findByLogin(Input::get('username'));

				if (!$user)
				{
					$resp = array("response" => TRUE);
				}

				if ($userLoggedin->username == $user->username)
				{
					$resp = array("response" => TRUE);
				}
				else
				{
					$resp = array("response" => FALSE);
				}
			}
			else if (Input::get('except') === FALSE)
			{
				$user = Sentry::getUserProvider()->findByLogin(Input::get('username'));
				$resp = array("response" => FALSE);
			}
			else
			{
				$user = Sentry::getUserProvider()->findByLogin(Input::get('username'));

				if ($user->username == Input::get('except'))
				{
					$resp = array("response" => TRUE);
				}
				else
				{
					$resp = array("response" => FALSE);
				}
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$resp = array("response" => TRUE);
		}

		return Response::json($resp, 200);
	}


	/**
	 * Check whether the email is unique
	 *
	 * @return mixed
	 */
	public function isEmailUnique()
	{

		$model  = new User();
		$result = $model->newQuery()->where('email', '=', Input::get('email'))->get();

		if (($count = $result->count()) == 0)
		{
			$resp = array("response" => TRUE);
		}

		if (Input::get('except') === TRUE)
		{

			$this->beforeFilter('checkAuth');

			try
			{
				$userLoggedin = Sentry::getUser();
				$user         = $result->first();

				if ($user == NULL)
				{
					$resp = array("response" => TRUE);
				}
				else if ($userLoggedin->email == $user->email)
				{
					$resp = array("response" => TRUE);
				}
				else
				{
					$resp = array("response" => FALSE);
				}
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				$resp = array("response" => TRUE);
			}
		}
		else if (Input::get('except') === FALSE)
		{

			if (($count = $result->count()) > 0)
			{
				$resp = array("response" => FALSE);
			}

			if (!$user = $result->first())
			{
				$resp = array("response" => TRUE);
			}
		}
		else
		{

			try
			{
				$user = $result->first();

				if ($user == NULL)
				{
					$resp = array("response" => TRUE);
				}
				else if (Input::get('except') == $user->email)
				{
					$resp = array("response" => TRUE);
				}
				else
				{
					$resp = array("response" => FALSE);
				}
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				$resp = array("response" => TRUE);
			}
		}

		return Response::json($resp, 200);
	}


	/**
	 * Activate an account
	 *
	 * @return
	 */
	public function activateAccount()
	{
		try
		{
			// Get the user we are trying to activate
			$user = Sentry::getUserProvider()->findByActivationCode(Input::get('activation_code'));

			// Try to activate this user account
			if ($user->attemptActivation(Input::get('activation_code')))
			{
				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::AUTH_ACTIVATEACCOUNT,
					'user_id'       => $user->id,
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('auth/message.activate_success'));
			}
			else
			{
				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::AUTH_ACTIVATEACCOUNT_FAILED,
					'user_id'       => $user->id,
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.activate_error'));
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.activate_error'));
		}

		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	/**
	 * Request a new password
	 *
	 * @return Redirect
	 */
	public function requestPassword()
	{
		try
		{
			// Get the user password recovery code
			$user = Sentry::getUserProvider()->findByLogin(Input::get('username'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::AUTH_REQUESTPASSWORD,
				'user_id'       => $user->id,
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));


			// Data to be used on the email view
			$data = array(
				'user'              => $user,
				// http://www.______.ch/WebAppStarter/#/auth/resetpwd/fXaFkM6lIyWTerDKxf0KIWvcegoVw3EZ0ZUUgoOWal
				'forgotPasswordUrl' => URL::to('/') . '/#/auth/resetpwd/' . $user->getResetPasswordCode(),
			);

			if (App::environment('local'))
			{
				Log::info("url=" . $data['forgotPasswordUrl']);
			}

			// Send the activation code through email
			Mail::send('emails.forgot-password', $data, function ($m) use ($user)
			{
				$m->to($user->email, $user->first_name . ' ' . $user->last_name);
				$m->subject(Config::get('app.name') . ': ' . Lang::get('auth/message.forgot_password_email_subject'));
			});
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			// Even though the email was not found, we will pretend
			// we have sent the password reset code through email,
			// this is a security measure against hackers.
		}

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('auth/message.forgot_password_success'));

		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * Save the new password
	 *
	 * @return View
	 */
	public function resetPassword()
	{
		// Declare the rules for the form validation
		$rules = array(
			'password'         => 'required',
			'password_confirm' => 'required|same:password'
		);

		// Create a new validator instance from our dynamic rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), 'Neues Passwort konnte nicht gespeichert werden');

			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		try
		{
			// Find the user using the password reset code
			$user = Sentry::getUserProvider()->findByResetPasswordCode(Input::get('reset_code'));

			// Attempt to reset the user password
			if ($user->attemptResetPassword(Input::get('reset_code'), Input::get('password')))
			{
				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::AUTH_RESETPASSWORD,
					'user_id'       => $user->id,
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				// Password successfully reseted
				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('auth/message.forgot_password_renew_success'));
			}
			else
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.forgot_password_renew_error'));
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.forgot_password_renew_error'));
		}

		return Response::json($this->httpResponse->getAsArray(), 200);
	}
}
