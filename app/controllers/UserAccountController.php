<?php



class UserAccountController extends AuthorizedController {


	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Get the logged in user
	 *
	 * @return mixed
	 */
	public function get()
	{

		try
		{
			$user = Sentry::getUser();

			// Portrait vorhanden?
			if ($user->hasPortrait(base_path() . '/' . Config::get('app.dir_image_user')))
			{
				$user->has_portrait = 'y';
			}
			else
			{
				$user->has_portrait = 'n';
			}

			return Response::json(['user' => $user], 200);
		}
		catch (UserNotFoundException $e)
		{

			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));

			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * Save user profile (username, first and lastname, email)
	 *
	 * @return mixed
	 */
	public function save()
	{
		$this->beforeFilter('checkCSRF');

		try
		{
			$user = Sentry::getUser();
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Speicherung fehlgeschlagen!");

			return Response::json($this->httpResponse->getAsArray(), 200);
		}


		$user->username   = Input::get('username');
		$user->first_name = Input::get('first_name');
		$user->last_name  = Input::get('last_name');
		$user->email      = Input::get('email');
		$user->sex        = Input::get('sex');

		// Was the user updated?
		if ($user->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::ACCOUNT_UPDATEPROFILE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('auth/message.profile_update_success'));
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Speicherung fehlgeschlagen!");
		}

		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	/**
	 * Save password
	 *
	 * @return mixed
	 */
	protected function savePassword()
	{
		$this->beforeFilter('checkCSRF');

		try
		{
			$user = Sentry::getUser();
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Speicherung fehlgeschlagen!");

			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$user->password = Input::get('password');

		// Was the user updated?
		if ($user->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::ACCOUNT_UPDATEPASSWORD,
				'user_id'       => $this->getUserId(),
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('auth/message.password_update_success'));
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Speicherung fehlgeschlagen!");
		}

		return Response::json($this->httpResponse->getAsArray(), 200);
	}




	/**
	 * Upload/Save Portrait
	 *
	 * @return mixed
	 */
	public function uploadPortrait()
	{
		if ($_FILES['file']['type'] != 'image/jpeg')
		{
			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.error'), "Nur JPG Bilder erlaubt!");

			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		if ($_FILES['file']['size'] > 4194300)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.error'), "Das Bild darf nicht grösser als 4 MB sein!");

			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		$user        = Sentry::getUser();
		$destination = base_path() . '/' . Config::get('app.dir_image_user') . $user->id . ".jpg";
		move_uploaded_file($_FILES['file']['tmp_name'], $destination);

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::ACCOUNT_UPDATEPORTRAIT,
			'user_id'       => $this->getUserId(),
			'foreign_table' => User::getTableName(),
			'foreign_key'   => $user->id
		));

		Event::fire(core\EventType::USER, array($userEvent));


		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Portrait gespeichert");

		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * Delete Portrait
	 *
	 * @return mixed
	 */
	public function deletePortrait()
	{
		$user = Sentry::getUser();

		try
		{
			unlink(Config::get('app.dir_image_user') . $user->id . '.jpg');
		}
		catch (Exception $e)
		{
		}

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::ACCOUNT_DELETEPORTRAIT,
			'user_id'       => $this->getUserId(),
			'foreign_table' => User::getTableName(),
			'foreign_key'   => $user->id
		));

		Event::fire(core\EventType::USER, array($userEvent));

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Ok, Portrait gelöscht");

		return Response::json($this->httpResponse->getAsArray(), 200);
	}
}
