<?php

use Carbon\Carbon;

class AppointmentController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	public function getAppointments()
	{
		$rows = Appointment::orderBy('startdate', 'desc')->get();

		$rows->each(function ($row)
		{
			// fnc
			$btnEdit   = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button> ';
			$btnDelete = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button> ';

			$row->fnc = $btnEdit;
			$row->fnc .= $btnDelete;
		});

		return Response::json($rows, 200);
	}


	/**
	 * @return mixed
	 */
	public function getAppointment()
	{
		$row = Appointment::withTrashed()->find(Input::get('id'));

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		return Response::json($row, 200);
	}


	/**
	 * @return mixed
	 */
	public function saveAppointment()
	{
		$this->beforeFilter('checkCSRF');

		$row = Appointment::withTrashed()->find(Input::get('id'));

		if ($row->deleted_at != NULL)
		{
			$row->restore();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::APPOINTMENT_RESTORE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Appointment::getTableName(),
				'foreign_key'   => $row->id,
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}

		$row->title       = Input::get('title');
		$row->description = Input::get('description');
		$row->startdate   = Input::get('startdate');
		$row->starttime   = Input::get('starttime');
		$row->enddate     = Input::get('enddate');
		$row->endtime     = Input::get('endtime');

		if ($row->enddate == "")
		{
			$row->enddate = $row->startdate;
		}

		if ($row->starttime == "") // 13-30-00 // 12-00-00
		{
			$row->starttime = NULL;
		}
		if ($row->endtime == "")
		{
			$row->endtime = NULL;
		}

		// Was the user updated?
		if ($row->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::APPOINTMENT_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Appointment::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/appointment/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/appointment/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function insertAppointment()
	{
		$this->beforeFilter('checkCSRF');

		$row              = new Appointment();
		$row->title       = Input::get('title');
		$row->description = Input::get('description');
		$row->startdate   = Input::get('startdate');
		$row->starttime   = Input::get('starttime');
		$row->enddate     = Input::get('enddate');
		$row->endtime     = Input::get('endtime');

		if ($row->enddate == "")
		{
			$row->enddate = $row->startdate;
		}

		if ($row->starttime == "")
		{
			$row->starttime = NULL;
		}
		if ($row->endtime == "")
		{
			$row->endtime = NULL;
		}

		if ($row->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::APPOINTMENT_CREATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Appointment::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/appointment/message.success.create'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/appointment/message.error.create'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function deleteAppointment()
	{
		$row = Appointment::find(Input::get('id'));

		if ($row->delete())
		{
			$appointmentName = '<strong>"' . $row->title . " " . $row->startdate . '"</strong>';
			$successMsg  = Lang::get('admin/appointment/message.success.delete', compact('appointmentName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::APPOINTMENT_DELETE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Appointment::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/appointment/message.error.delete'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}
}
