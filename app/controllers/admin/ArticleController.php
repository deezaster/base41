<?php

use Carbon\Carbon;

class ArticleController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	public function getArticles()
	{
		$rows = Article::orderBy('articledate', 'desc')->get();

		$rows->each(function ($row)
		{
			// fnc
			$btnEdit   = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button> ';
			$btnDelete = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button> ';

			$row->fnc = $btnEdit;
			$row->fnc .= $btnDelete;
		});

		return Response::json($rows, 200);
	}


	/**
	 * @return mixed
	 */
	public function getArticle()
	{
		$row = Article::withTrashed()->find(Input::get('id'));

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		return Response::json($row, 200);
	}


	/**
	 * @return mixed
	 */
	public function saveArticle()
	{
		$this->beforeFilter('checkCSRF');

		$row = Article::withTrashed()->find(Input::get('id'));

		if ($row->deleted_at != NULL)
		{
			$row->restore();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::ARTICLE_RESTORE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Article::getTableName(),
				'foreign_key'   => $row->id,
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}

		$row->title       = Input::get('title');
		$row->content     = Input::get('content');
		$row->articledate = Input::get('articledate');
		$row->publishdate = Input::get('publishdate');

	   if ($row->publishdate == "") {
			$row->publishdate = null;
		}

		// Was the user updated?
		if ($row->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::ARTICLE_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Article::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/article/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/article/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function insertArticle()
	{
		$this->beforeFilter('checkCSRF');

		$row              = new Article();
		$row->title       = Input::get('title');
		$row->content     = Input::get('content');
		$row->articledate = Input::get('articledate');
		$row->publishdate = Input::get('publishdate');

		if ($row->publishdate == "") {
			$row->publishdate = null;
		}

		if ($row->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::ARTICLE_CREATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Article::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/article/message.success.create'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/article/message.error.create'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function deleteArticle()
	{
		$row = Article::find(Input::get('id'));

		if ($row->delete())
		{
			$articleName = '<strong>"' . $row->title . '"</strong>';
			$successMsg  = Lang::get('admin/article/message.success.delete', compact('articleName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::ARTICLE_DELETE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Article::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/article/message.error.delete'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}
}
