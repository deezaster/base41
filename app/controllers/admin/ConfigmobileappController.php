<?php

use Carbon\Carbon;

class ConfigmobileappController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	public function getConfigmobileapps()
	{
		$rows = Configmobileapp::orderBy('key', 'asc')->get();

		$rows->each(function ($row)
		{
			// fnc

			$btnEdit   = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button> ';

			if ($row->is_editable == 0) {
				$btnEdit = "";
			}

			$row->fnc = $btnEdit;
		});

		return Response::json($rows, 200);
	}


	/**
	 * @return mixed
	 */
	public function getConfigmobileapp()
	{
		$row = Configmobileapp::withTrashed()->find(Input::get('id'));

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		return Response::json($row, 200);
	}


	/**
	 * @return mixed
	 */
	public function saveConfigmobileapp()
	{
		$this->beforeFilter('checkCSRF');

		$row = Configmobileapp::withTrashed()->find(Input::get('id'));

		$row->key       = Input::get('key');
		$row->value     = Input::get('value');

		// Was the user updated?
		if ($row->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::CONFIGMOBILEAPP_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Configmobileapp::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/configmobileapp/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/configmobileapp/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}
	


}
