<?php

use Illuminate\Support\Collection;

class DashboardController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	public function init()
	{
	}


	/**
	 * @return mixed
	 */
	public function getUserEvents()
	{
		$rows = UserEvent::orderBy('event_at', 'desc')->get();

		$rows->each(function ($row)
		{

			// User ermitteln
			$row->user     = Sentry::getUserProvider()->findById($row->user_id);
			$row->username = $row->user->username;

			// Tabelle/Modell
			$row->detail = "";

			// admin/events/message.user.ban
			//$row->eventtranslated = Lang::get('admin/events/message.' . $row->event);
			$msg = explode(".", Lang::get('admin/events/message.' . $row->event));


			$row->topictranslated = $msg[0];
			if (count($msg) > 1)
			{
				$row->eventtranslated = $msg[1];
			}
			else
			{
				$row->eventtranslated = "???";
			}

			if ($row->foreign_table != "" && $row->foreign_key != "")
			{

				// Document
				if ($row->foreign_table == "document")
				{
					$model = Document::withTrashed()->find($row->foreign_key);
					if ($model)
					{
						$row->detail = $model->filename;
					}
					else
					{
						$row->detail = $row->eventtext1;
						if ($row->eventtext2 != "")
						{
							$row->detail .= " > " . $row->eventtext2;
						}
					}
				}

				// Filestructure
				if ($row->foreign_table == "filestructure")
				{
					if ($row->eventtext1 != "")
					{
						$row->detail = $row->eventtext1;
						if ($row->eventtext2 != "")
						{
							$row->detail .= " > " . $row->eventtext2;
						}
					}
				}
			}

			// User
			if ($row->foreign_table == "users")
			{
				$model = User::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/user/edit/' . $model->id . '">' . $model->first_name . " " . $model->last_name . "</a> (#" . $model->id . ")";
				}
			}

			// Massemail
			if ($row->foreign_table == "massemail")
			{
				$model = Massemail::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/massemail/edit/' . $model->id . '">' . $model->subject . '</a> (#' . $model->id . ")";
				}
			}

			// MassemailSent
			if ($row->foreign_table == "massemail_sent")
			{
				$model = MassemailSent::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$email       = Massemail::withTrashed()->find($model->massemail_id);
					$row->detail = '<a href="#/admin/massemail/edit/' . $email->id . '">' . $email->subject . "</a> (#" . $email->id . ")";
				}
			}

			// Article
			if ($row->foreign_table == "article")
			{
				$model = Article::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/article/edit/' . $model->id . '">' . $model->title . '</a> (#' . $model->id . ")";
				}
			}

			// Appointment
			if ($row->foreign_table == "appointment")
			{
				$model = Article::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/appointment/edit/' . $model->id . '">' . $model->title . ' ' . $model->startdate . '</a> (#' . $model->id . ")";
				}
			}

			// Member
			if ($row->foreign_table == "member")
			{
				$model = Member::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/member/edit/' . $model->id . '">' . $model->vorname . " " . $model->nachname . "</a> (#" . $model->id . ")";
				}

				$row->detail .= ' ' . $row->eventtext1 . " > " . $row->eventtext2;
			}

			// Member
			if ($row->foreign_table == "member_locations")
			{
				$model = Member::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/member/edit/' . $model->id . '">' . $model->vorname . " " . $model->nachname . "</a> (#" . $model->id . ")";
				}

				$row->detail .= ' ' . $row->eventtext1;
				$row->detail .= strlen($row->eventtext2) > 0 ? ' > ' . $row->eventtext2 : '';
			}

			// Member Notifications
			if ($row->foreign_table == "member_notifications")
			{
				$model = MemberNotifications::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$device      = $model->device;
					$member      = $device->member;
					$row->detail = $model->message . " (#" . $model->id . ') an: <a href="#/admin/member/edit/' . $member->id . '">' . $member->vorname . " " . $member->nachname . "</a> (#" . $member->id . ")";

					if (strlen($row->eventtext1) > 0) {
						$sender = Member::withTrashed()->find($row->eventtext1);
						$row->detail .= ' von: <a href="#/admin/member/edit/' . $sender->id . '">' . $sender->vorname . " " . $sender->nachname . "</a> (#" . $sender->id . ")";
					}
				}
			}

			// Email
			if ($row->foreign_table == "email")
			{
				$model = Email::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					//$row->detail = '<a href="#/admin/email/edit/' . $model->id. '">'  . $model->subject . "</a> (#" . $model->id . ")";
					$row->detail = $model->subject . " (#" . $model->id . ")";
				}
			}

			// Email Template
			if ($row->foreign_table == "templates_email")
			{
				$model = TemplatesEmail::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/emailtemplate/edit/' . $model->id . '">' . $model->subject . "</a> (#" . $model->id . ")";
				}
			}

			// Config MobileApp
			if ($row->foreign_table == "configmobileapp")
			{
				$model = Configmobileapp::withTrashed()->find($row->foreign_key);
				if ($model)
				{
					$row->detail = '<a href="#/admin/configmobileapp/edit/' . $model->id . '">' . $model->key . '</a> (#' . $model->id . ")";
				}
			}

		});


		return Response::json($rows, 200);
	}
}
