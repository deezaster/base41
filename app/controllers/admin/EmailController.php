<?php

use Carbon\Carbon;
use Illuminate\Support\Collection;

class EmailController extends AdminController {

	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	public function getEmail()
	{
		$row = Email::withTrashed()->find(Input::get('id'));

		$row->sender = $row->sender_name . " <" . $row->sender_email . ">";

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		$row->attachments;

		foreach ($row->members as $recipient)
		{
			$row->recipient .= $recipient->vorname . " " . $recipient->nachname . " &lt;" . $recipient->email . "&gt;#";
		};
		foreach ($row->users as $recipient)
		{
			$row->recipient .= $recipient->first_name . " " . $recipient->last_name . " &lt;" . $recipient->email . "&gt;#";
		};


		$row->files = "";
		foreach ($row->attachments as $doc)
		{
			$row->files .= $doc->filename . "#";
		};

		return Response::json($row, 200);
	}

	public function getEmails()
	{

		$rows = Email::orderBy('sent_at', 'desc')->get();

		$rows->each(function ($row)
		{
			$row->sender             = $row->sender_name . " &lt;" . $row->sender_email . "&gt;";
			$row->email_type_display = "E-Mail";
			$row->email_type         = "email";

			foreach ($row->members as $recipient)
			{
				$row->recipient .= $recipient->email . ' ';
			};
			foreach ($row->users as $recipient)
			{
				$row->recipient .= $recipient->email . ' ';
			};

			// fnc
			$btnShow    = '<button class="btn btn-xs btn-primary" ng-click="grid.appScope.onShow(row)" tooltips="" tooltip-title="EMAIL ANZEIGEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-eye"></span></button> ';
			$btnEdit    = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button> ';
			$btnDelete  = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button> ';
			$btnArchive = '<button class="btn btn-xs btn-warning" ng-click="grid.appScope.onArchive(row)" tooltips="" tooltip-title="ARCHIVIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-database"></span></button> ';

			$row->fnc = $btnShow;

			for ($x = 1; $x <= $row->attachments->count(); $x++)
			{
				$row->attachmentsIcon .= '<i class="fa fa-paperclip"></i>';
			}

			if ($row->archived_at == NULL || $row->archived_at == "0000-00-00 00:00:00")
			{
				$row->archived_at = "";
				$row->fnc .= $btnArchive;
			}

			$row->fnc .= $btnDelete;
		});

		return Response::json($rows, 200);
	}

	public function getRecipients()
	{
		$sql = "SELECT u.id, u.email email, concat(u.first_name, ' ', u.last_name) name, 'User' type, 'Benutzer' typetxt
					FROM users u
					UNION ALL
					SELECT m.id, m.email email, concat(m.vorname, ' ', m.nachname) name, 'Member' type, 'Mitglied' typetxt
					FROM member m
					ORDER BY 2,3";

		$rows = DB::select(DB::raw($sql));

		return Response::json($rows);
	}

	public function getEmailTemplates()
	{
		$rows = TemplatesEmail::orderBy('name', 'sc')->get();

		$rows->each(function ($row)
		{
			$btnEdit  = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button>';
			$row->fnc = $btnEdit;
		});

		return Response::json($rows, 200);
	}


	public function getEmailTemplate()
	{
		$row = TemplatesEmail::withTrashed()->find(Input::get('id'));

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		return Response::json($row, 200);
	}

	public function getEmailsByMemberId()
	{
		$user = Member::find(Input::get('id'));

		if ($user)
		{
			$emails = $user->emails;

			$emails->each(function ($row)
			{
				$row->sender             = $row->sender_name . " <" . $row->sender_email . ">";
				$row->email_type_display = "E-Mail";
				$row->email_type         = "email";

				for ($x = 1; $x <= $row->attachments->count(); $x++)
				{
					$row->attachmentsIcon .= '<i class="fa fa-paperclip"></i>';
				}

				// fnc
				$btnShow = '<button type="button" class="btn btn-xs btn-primary" ng-click="grid.appScope.onShow(row)" tooltips="" tooltip-title="EMAIL ANZEIGEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-eye"></span></button>';

				$row->fnc = $btnShow;
			});


			$massemails = $user->massemails;

			$massemails->each(function ($row)
			{
				$email                   = $row->massemail;
				$row->sender             = $email->sender_name . " <" . $email->sender_email . ">";
				$row->email_type_display = "Massen E-Mail";
				$row->email_type         = "massemail";
				$row->subject            = $email->subject;
				$row->sender_email       = $email->sender_email;
				$row->sender_name        = $email->sender_name;

				for ($x = 1; $x <= $email->attachments->count(); $x++)
				{
					$row->attachmentsIcon .= '<i class="fa fa-paperclip"></i>';
				}

				// fnc
				$btnShow = '<button type="button" class="btn btn-xs btn-primary" ng-click="grid.appScope.onShow(row)" tooltips="" tooltip-title="EMAIL ANZEIGEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-eye"></span></button>';

				$row->fnc = $btnShow;
			});

			$coll = $emails->merge($massemails);
			return Response::json($coll, 200);
		}
	}


	public function getEmailsByUserId()
	{
		$member = Member::find(Input::get('id'));

		if ($member)
		{
			$emails = $member->emails;

			$emails->each(function ($row)
			{
				$row->sender             = $row->sender_name . " <" . $row->sender_email . ">";
				$row->email_type_display = "E-Mail";
				$row->email_type         = "email";

				for ($x = 1; $x <= $row->attachments->count(); $x++)
				{
					$row->attachmentsIcon .= '<i class="fa fa-paperclip"></i>';
				}

				// fnc
				$btnShow = '<button type="button" class="btn btn-xs btn-primary" ng-click="grid.appScope.onShow(row)" tooltips="" tooltip-title="EMAIL ANZEIGEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-eye"></span></button>';

				$row->fnc = $btnShow;
			});


			$massemails = $member->massemails;

			$massemails->each(function ($row)
			{
				$email                   = $row->massemail;
				$row->sender             = $email->sender_name . " <" . $email->sender_email . ">";
				$row->email_type_display = "Massen E-Mail";
				$row->email_type         = "massemail";
				$row->subject            = $email->subject;
				$row->sender_email       = $email->sender_email;
				$row->sender_name        = $email->sender_name;

				for ($x = 1; $x <= $email->attachments->count(); $x++)
				{
					$row->attachmentsIcon .= '<i class="fa fa-paperclip"></i>';
				}

				// fnc
				$btnShow = '<button type="button" class="btn btn-xs btn-primary" ng-click="grid.appScope.onShow(row)" tooltips="" tooltip-title="EMAIL ANZEIGEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-eye"></span></button>';

				$row->fnc = $btnShow;
			});

			$coll = $emails->merge($massemails);
			return Response::json($coll, 200);
		}
	}

	/**
	 *
	 */
	public function getMassemailBySentId()
	{
		// Empfänger der Sendung
		$sent = MassemailSent::find(Input::get('id'));

		$email                  = $sent->massemail;
		$email->created_by_user = $email->getCreatedByUser();
		$email->updated_by_user = $email->getUpdatedByUser();
		$email->deleted_by_user = $email->getDeletedByUser();

		$email->sender  = $email->sender_name . " &lt;" . $email->sender_email . "&gt;";
		$email->sent_at = $sent->sent_at;


		$recipients = $sent->users;

		if (count($recipients) == 0)
		{
			$recipients       = $sent->members;
			$email->recipient = "Massen E-Mail: " . count($recipients) . " Mitglieder";
		}
		else
		{
			$email->recipient = "Massen E-Mail: " . count($recipients) . " Benutzer";
		}

		$email->files = "";
		foreach ($email->attachments as $doc)
		{
			$email->files .= $doc->filename . "#";
		};


		return Response::json($email, 200);
	}

	public function sendEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email = new Email();

		$email->subject      = Input::get('subject');
		$email->sender_name  = Input::get('sender_name');
		$email->sender_email = Input::get('sender_email');
		$email->content      = Input::get('content');
		$email->sent_at      = new Carbon();

		if ($email->save())
		{
			// Dokumente anhängen
			foreach (Input::get('docs') as $doc)
			{
				$email->attachments()->attach((int)$doc);
			}

			// Empfänger anhängen
			foreach (Input::get('recipients') as $recipient)
			{
				if ($recipient['type'] == "Member")
				{
					$member = Member::find($recipient['id']);
					$email->members()->attach($member->id);
				}
				if ($recipient['type'] == "User")
				{
					$user = User::find($recipient['id']);
					$email->users()->attach($user->id);
				}
			}


			// CONTENT
			$data = array(
				'content' => $email->content
			);

			// TO
			$to = array();
			foreach ($email->members as $recipient)
			{
				array_push($to, $recipient->email);
			};
			foreach ($email->users as $recipient)
			{
				array_push($to, $recipient->email);
			};

			// CC
			$cc = array();

			// BCC
			$bcc = array();

			if ($this->send($data, $email, $to, $cc, $bcc))
			{
				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::EMAIL_SEND,
					'user_id'       => $this->getUserId(),
					'foreign_table' => Email::getTableName(),
					'foreign_key'   => $email->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.send'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.send'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	public function deleteEmail()
	{
		$email = Email::find(Input::get('id'));

		if ($email->delete())
		{
			$emailName  = '<strong>"' . $email->subject . '"</strong>';
			$successMsg = Lang::get('admin/email/message.success.delete', compact('emailName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::EMAIL_DELETE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Email::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.delete'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	public function archiveEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email              = Email::find(Input::get('id'));
		$email->archived_at = new Carbon;

		if ($email->save())
		{
			$emailName  = '<strong>"' . $email->subject . '"</strong>';
			$successMsg = Lang::get('admin/email/message.success.archive', compact('emailName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::EMAIL_ARCHIVE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Email::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.archive'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	public function restoreEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email = Email::withTrashed()->find(Input::get('id'));

		if ($email->deleted_at != NULL)
		{
			$email->restore();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::EMAIL_RESTORE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Email::getTableName(),
				'foreign_key'   => $email->id,
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.restore'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	public function saveEmailTemplate()
	{
		$this->beforeFilter('checkCSRF');

		$template          = TemplatesEmail::find(Input::get('id'));
		$template->subject = Input::get('subject');
		$template->content = Input::get('content');


		// Was the user updated?
		if ($template->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::EMAILTEMPLATE_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => TemplatesEmail::getTableName(),
				'foreign_key'   => $template->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/emailtemplates/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/emailtemplates/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	public function sendEmailLoginCredentials()
	{
		$email               = new Email();
		$email->sender_email = Input::get('sender_email');
		$email->sender_name  = Input::get('sender_name');
		$email->subject      = Input::get('subject');
		$email->content      = Input::get('content');
		$email->save();

		$member = Member::find(Input::get('memberId'));
		$email->members()->attach($member->id);

		// CONTENT
		$data = array(
			'content' => $email->content
		);

		// TO
		$to = array();
		array_push($to, $email->recipient_email);

		// CC
		$cc = array();

		// BCC
		$bcc = array();

		// send the emails
		if ($this->send($data, $email, $to, $cc, $bcc))
		{
			$email->sent_at = new Carbon();
			$email->save();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_SENDLOGINCREDENTIALS,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Member::getTableName(),
				'foreign_key'   => Input::get('memberId')
			));

			Event::fire(core\EventType::USER, array($userEvent));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_SENDLOGINCREDENTIALS,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Email::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.send'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.send'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 * @return mixed
	 */
	private function send($data, $email, $to, $cc, $bcc)
	{
		try
		{
			Mail::send('emails.empty', $data, function ($message) use ($email, $to, $cc, $bcc)
			{
				$message->from($email->sender_email, $email->sender_name);
				$message->to($to)->cc($cc)->bcc($bcc);
				$message->subject($email->subject);
			});
		}
		catch (Swift_RfcComplianceException $e)
		{
			Log::error("Email Error: " . $e->getMessage());
			return FALSE;
		}
		catch (Swift_TransportException $e)
		{
			Log::error("Email Error: " . $e->getMessage());
			return FALSE;
		}

		return TRUE;
	}
}