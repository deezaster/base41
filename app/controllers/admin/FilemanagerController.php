<?php

use Illuminate\Support\Collection;

class FilemanagerController extends AdminController {

	protected $coll;

	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	/**
	 * @return mixed
	 */
	public function getStructure()
	{
		$rows = FileStructure::orderBy('node_id_parent', 'asc')->orderBy('name', 'desc')->get();

		$this->coll = new Collection();

		$rows->each(function ($rec)
		{
			$obj         = new stdClass;
			$obj->id     = (string)$rec->node_id;
			$obj->parent = $rec->node_id_parent == NULL ? '#' : (string)$rec->node_id_parent;
			$obj->text   = $rec->name;

			$cfg = Configmobileapp::where('key', '=', 'DocumentsDirectoryId')->first();
			if ($cfg->value == $rec->id)
			{
				$obj->type = 'downloaddir';
			}
			else
			{
				$obj->type = $rec->type;
			}

			$obj->state = array("opened" => TRUE);
			$this->coll->push($obj);
		}
		);

		return Response::json($this->coll, 200);
	}

	/**
	 * @return mixed
	 */
	public function getFiles()
	{
		$docs = Document::all();

		$docs->each(function ($row)
		{
			$row->value = (string)$row->id;
		}
		);

		return Response::json($docs, 200);
	}


	/**
	 * @return mixed
	 */
	public function getFilesPerNode()
	{
		$FileStructure = FileStructure::where("node_id", "=", Input::get('id'))->first();

		$docs = $FileStructure->documents;
		$docs->each(function ($row)
		{
			$btnDelete = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button>';
			$row->fnc  = $btnDelete;
		}
		);


		return Response::json($docs, 200);
	}

	/**
	 * @return mixed
	 */
	public function uploadFile()
	{
		if ($_FILES['file']['type'] != 'application/pdf' && $_FILES['file']['type'] != 'audio/mpeg')
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Nur PDF und MP3 Dateien sind erlaubt!");
			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		if ($_FILES['file']['size'] > 41943000)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), "Das Dokument darf nicht grösser als 40 MB sein!");
			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		$destination = base_path() . '/' . Config::get('app.dir_docs') . $_FILES['file']['name'];
		if (move_uploaded_file($_FILES['file']['tmp_name'], $destination))
		{
			$FileStructure = FileStructure::where("node_id", "=", Input::get('id'))->first();

			$document                   = new Document();
			$document->FileStructure_id = $FileStructure->id;
			$document->filename         = $_FILES['file']['name'];
			$document->filesize         = $_FILES['file']['size'];
			$document->type             = $_FILES['file']['type'];
			$document->description      = Input::get('description');


			if ($document->save())
			{
				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::FILEMANAGER_UPLOADFILE,
					'user_id'       => $this->getUserId(),
					'foreign_table' => Document::getTableName(),
					'foreign_key'   => $document->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.uploadFile'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}
			else
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/filemanager/message.error.uploadFile'));
				return Response::json($this->httpResponse->getAsArray(), 400);
			}
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * @return mixed
	 */
	public function deleteFile()
	{
		$document = Document::find(Input::get('id'));
		if ($document->delete())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_DELETEFILE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Document::getTableName(),
				'foreign_key'   => $document->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Dokument gelöscht");
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * @return mixed
	 */
	public function deleteDir()
	{
		$fileStructure = FileStructure::where("node_id", "=", Input::get('id'))->first();

		// Wenn Childs dran hängen, dann Löschung nicht erlauben
		$cntChilds = FileStructure::where("node_id_parent", "=", $fileStructure->node_id)->count();
		if ($cntChilds > 0)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/filemanager/message.error.deleteDirWithChilfs'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		if ($fileStructure->delete())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_DELETEDIR,
				'user_id'       => $this->getUserId(),
				'foreign_table' => FileStructure::getTableName(),
				'foreign_key'   => $fileStructure->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.deleteDir'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * @return mixed
	 */
	public function renameDir()
	{
		$fileStructure       = FileStructure::where("node_id", "=", Input::get('id'))->first();
		$oldName             = $fileStructure->name;
		$fileStructure->name = Input::get('name');

		if ($fileStructure->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_RENAMEDIR,
				'user_id'       => $this->getUserId(),
				'foreign_table' => FileStructure::getTableName(),
				'foreign_key'   => $fileStructure->id,
				'eventtext1'    => $oldName,
				'eventtext2'    => $fileStructure->name
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.renameDir'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * @return mixed
	 */
	public function createDir()
	{
		$fileStructure          = new FileStructure();
		$fileStructure->node_id = Input::get('id');
		$fileStructure->name    = Input::get('name');
		$fileStructure->type    = "dir";

		$parentId                      = Input::get('toId') == '#' ? NULL : Input::get('toId');
		$fileStructure->node_id_parent = $parentId;

		if ($fileStructure->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_CREATEDIR,
				'user_id'       => $this->getUserId(),
				'foreign_table' => FileStructure::getTableName(),
				'foreign_key'   => $fileStructure->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.createDir'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * @return mixed
	 */
	public function moveDir()
	{
		$fileStructure                 = FileStructure::where("node_id", "=", Input::get('id'))->first();
		$parentId                      = Input::get('toId') == '#' ? NULL : Input::get('toId');
		$fileStructure->node_id_parent = $parentId;

		if ($fileStructure->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_MOVEDIR,
				'user_id'       => $this->getUserId(),
				'foreign_table' => FileStructure::getTableName(),
				'foreign_key'   => $fileStructure->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.moveDir'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}


	/**
	 * @return mixed
	 */
	public function updateDescription()
	{
		$doc              = Document::find(Input::get('id'));
		$oldDescription   = $doc->description;
		$doc->description = Input::get('description');


		if ($doc->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_UPDATEDESCRIPTION,
				'user_id'       => $this->getUserId(),
				'foreign_table' => FileStructure::getTableName(),
				'foreign_key'   => $doc->id,
				'eventtext1'    => $oldDescription,
				'eventtext2'    => $doc->description
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.updateDescription'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('general.general_error'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}


	/**
	 * @return mixed
	 */
	public function setDownloadDir()
	{
		$cfg = Configmobileapp::where('key', '=', 'DocumentsDirectoryId')->first();
		$fs = FileStructure::where('node_id', '=', Input::get('id'))->first();

		$cfg->value = $fs->id;

		if ($cfg->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::FILEMANAGER_SETDOWNLOADDIR,
				'user_id'       => $this->getUserId(),
				'foreign_table' => FileStructure::getTableName(),
				'foreign_key'   => $cfg->value,
				'eventtext1'    => $fs->name
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/filemanager/message.success.setDownloadDir'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/filemanager/message.error.setDownloadDir'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}
}
