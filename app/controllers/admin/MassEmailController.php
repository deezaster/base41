<?php

use Carbon\Carbon;

class MassemailController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	public function getEmails()
	{

		$rows = Massemail::orderBy('updated_at', 'desc')->get();

		$rows->each(function ($row)
		{
			// fnc
			$btnEdit    = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button> ';
			$btnDelete  = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button> ';
			$btnArchive = '<button class="btn btn-xs btn-warning" ng-click="grid.appScope.onArchive(row)" tooltips="" tooltip-title="ARCHIVIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-database"></span></button> ';
			$btnSentLog = '<button class="btn btn-xs btn-primary" ng-click="grid.appScope.onSentLog(row)" tooltips="" tooltip-title="SENDEPROTOKOLL" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-transfer"></span></button> ';

			$row->fnc = $btnEdit;

			for ($x = 1; $x <= $row->attachments->count(); $x++)
			{
				$row->attachmentsIcon .= '<i class="fa fa-paperclip"></i>';
			}

			if ($row->archived_at == NULL || $row->archived_at == "0000-00-00 00:00:00")
			{
				$row->archived_at = "";
				$row->fnc .= $btnArchive;
			}

			$row->fnc .= $btnDelete . $btnSentLog;
		});

		return Response::json($rows, 200);
	}

	/**
	 *
	 */
	public function getSentLog()
	{
		// Sendungen
		$email      = Massemail::find(Input::get('emailId'));
		$emailSents = $email->sent; //->sortByDesc('sent_at');
		$emailSents->each(function ($row)
		{
			$sent = MassemailSent::find($row->id);

			$users = $sent->users;

			if (count($users) > 0)
			{
				$row->cnt  = count($users);
				$row->type = "Benutzer";
			}
			else
			{
				$members   = $sent->members;
				$row->cnt  = count($members);
				$row->type = "Mitglied";
			}
		});

		return Response::json($emailSents, 200);
	}


	/**
	 *
	 */
	public function getSentLogRecipients()
	{
		// Empfänger der Sendung
		$sent = MassemailSent::find(Input::get('sentId'));

		$recipients = $sent->users;

		if (count($recipients) == 0)
		{
			$recipients = $sent->members;

			$recipients->each(function ($row)
			{
				$row->recipient = '<a href="#/admin/member/edit/' . $row->id . '">' . $row->vorname . " " . $row->nachname . "</a> &lt;" . $row->email . "&gt;";
			});
		}
		else
		{
			$recipients->each(function ($row)
			{
				$row->recipient = '<a href="#/admin/user/edit/' . $row->id . '">' . $row->first_name . " " . $row->last_name . "</a> &lt;" . $row->email . "&gt;";
			});
		}

		return Response::json($recipients, 200);
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		$row = Massemail::withTrashed()->find(Input::get('id'));

		$row->sender = $row->sender_name . " <" . $row->sender_email . ">";

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		$row->attachments;

		return Response::json($row, 200);
	}

	/**
	 * @return mixed
	 */
	public function sendEmailToUsers()
	{
		$email = Massemail::find(Input::get('emailId'));

		$sent          = new MassemailSent();
		$sent->sent_at = new Carbon();

		$email->sent()->save($sent);

		foreach (Input::get('recipientIds') as $recipientId)
		{
			$recipient = User::find($recipientId);

			if (!$recipient->isSystemUser())
			{
				if (!filter_var($recipient->email, FILTER_VALIDATE_EMAIL))
				{
					Log::error("Ungültige EMail-Adresse : " . $recipient->email);
				}
				else
				{
					$sent->users()->attach($recipient);
				}
			}
		}

		// CONTENT
		$data = array(
			'content' => $email->content
		);

		// TO
		$to = array();
		array_push($to, $email->sender_email);

		// CC
		$cc = array();

		// BCC
		$bcc = array();
		foreach ($sent->users as $user)
		{
			array_push($bcc, $user->email);
		};

		// send the emails
		if ($this->sendEmail($sent, $data, $email, $to, $cc, $bcc))
		{
			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.send'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			// identisch: $email->sent()->delete($sent);
			$sent->delete();

			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.send'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}


	/**
	 * @return mixed
	 */
	public function sendEmailToMembers()
	{
		$email = Massemail::find(Input::get('emailId'));

		$sent          = new MassemailSent();
		$sent->sent_at = new Carbon();

		$email->sent()->save($sent);

		foreach (Input::get('recipientIds') as $recipientId)
		{
			$recipient = Member::find($recipientId);

			if (!filter_var($recipient->email, FILTER_VALIDATE_EMAIL))
			{
				Log::error("Ungültige EMail-Adresse : " . $recipient->email);
			}
			else
			{
				$sent->members()->attach($recipient);
			}
		}


		// CONTENT
		$data = array(
			'content' => $email->content
		);

		// TO
		$to = array();
		array_push($to, $email->sender_email);

		// CC
		$cc = array();

		// BCC
		$bcc = array();
		foreach ($sent->members as $user)
		{
			array_push($bcc, $user->email);
		};

		// send the emails
		if ($this->sendEmail($sent, $data, $email, $to, $cc, $bcc))
		{
			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.send'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			// identisch: $email->sent()->delete($sent);
			$sent->delete();

			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.send'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}


	/**
	 * @return mixed
	 */
	private function sendEmail($sent, $data, $email, $to, $cc, $bcc)
	{
		try
		{
			Mail::send('emails.empty', $data, function ($message) use ($email, $to, $cc, $bcc)
			{
				$message->from($email->sender_email, $email->sender_name);
				$message->to($to)->cc($cc)->bcc($bcc);
				$message->subject($email->subject);

				foreach ($email->attachments as $document)
				{
					$message->attach(base_path() . '/' . Config::get('app.dir_docs') . $document->filename);
				}
			});

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_SEND,
				'user_id'       => $this->getUserId(),
				'foreign_table' => MassemailSent::getTableName(),
				'foreign_key'   => $sent->id
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}
		catch (Swift_RfcComplianceException $e)
		{
			Log::error("Email Error: " . $e->getMessage());
			return FALSE;
		}
		catch (Swift_TransportException $e)
		{
			Log::error("Email Error: " . $e->getMessage());
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * @return mixed
	 */
	public function saveEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email = Massemail::withTrashed()->find(Input::get('id'));

		if ($email->deleted_at != NULL)
		{
			$email->restore();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_RESTORE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Massemail::getTableName(),
				'foreign_key'   => $email->id,
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}

		$email->subject      = Input::get('subject');
		$email->sender_name  = Input::get('sender_name');
		$email->sender_email = Input::get('sender_email');
		$email->content      = Input::get('content');


		// zuerst alle referenzen löschen...
		$email->attachments()->detach();

		// ... und neu anhängen
		foreach (Input::get('docs') as $doc)
		{
			$email->attachments()->attach((int)$doc);
		}

		// Was the user updated?
		if ($email->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Massemail::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function insertEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email               = new Massemail();
		$email->subject      = Input::get('subject');
		$email->sender_name  = Input::get('sender_name');
		$email->sender_email = Input::get('sender_email');
		$email->content      = Input::get('content');

		// zuerst alle referenzen löschen...
		$email->attachments()->detach();

		// ... und neu anhängen
		foreach (Input::get('docs') as $doc)
		{
			$email->attachments()->attach((int)$doc);
		}

		if ($email->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_CREATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Massemail::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/email/message.success.create'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.create'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function deleteEmail()
	{
		$email = Massemail::find(Input::get('id'));

		if ($email->delete())
		{
			$emailName  = '<strong>"' . $email->subject . '"</strong>';
			$successMsg = Lang::get('admin/email/message.success.delete', compact('emailName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_DELETE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Massemail::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.delete'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 * @return mixed
	 */
	public function archiveEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email              = Massemail::find(Input::get('id'));
		$email->archived_at = new Carbon;

		if ($email->save())
		{
			$emailName  = '<strong>"' . $email->subject . '"</strong>';
			$successMsg = Lang::get('admin/email/message.success.archive', compact('emailName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_ARCHIVE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Massemail::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.error.archive'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function unarchiveEmail()
	{
		$this->beforeFilter('checkCSRF');

		$email              = Massemail::find(Input::get('id'));
		$email->archived_at = NULL;

		if ($email->save())
		{
			$emailName  = '<strong>"' . $email->subject . '"</strong>';
			$successMsg = Lang::get('admin/email/message.success.unarchive', compact('emailName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MASSEMAIL_UNARCHIVE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Massemail::getTableName(),
				'foreign_key'   => $email->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/email/message.errorunarchive'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	/**
	 * @return mixed
	 */
	public function getEmailsForDropdown()
	{
		$rows = Massemail::whereRaw('archived_at = "0000-00-00 00:00:00" or archived_at is null')->orderBy('updated_at', 'desc')->get();

		return Response::json($rows, 200);
	}
}
