<?php

use core\Messages;

class MemberController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	/**
	 * @return mixed
	 */
	public function getMembers()
	{
		$rows = Member::orderBy('nachname', 'asc')->orderBy('vorname', 'asc')->get();

		$rows->each(function ($row)
		{

			// Portrait vorhanden?
			if ($row->hasPortrait(base_path() . '/' . Config::get('app.dir_image_member')))
			{
				$row->portrait = Config::get('app.dir_image_member') . $row->id . '.jpg';
			}
			else
			{
				if ($row->sex == 'm')
				{
					$row->portrait = Config::get('app.dir_image_member') . 'male.jpg';
				}
				else
				{
					$row->portrait = Config::get('app.dir_image_member') . 'female.jpg';
				}
			}

			$random = rand(1, 9999);
			$row->portrait .= "?v=" . $random;


			// fnc
			$btnEdit   = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button>';
			$btnDelete = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button>';

			$row->fnc = $btnEdit;
			$row->fnc .= " " . $btnDelete;
		});


		return Response::json($rows, 200);
	}

	/**
	 * @return mixed
	 */
	public function getMember()
	{
		$row = Member::withTrashed()->find(Input::get('id'));

		$row->created_by_user = $row->getCreatedByUser();
		$row->updated_by_user = $row->getUpdatedByUser();
		$row->deleted_by_user = $row->getDeletedByUser();

		// Portrait vorhanden?
		if ($row->hasPortrait(base_path() . '/' . Config::get('app.dir_image_member')))
		{
			$row->portrait          = Config::get('app.dir_image_member') . $row->id . '.jpg';
			$row->isDefaultPortrait = FALSE;
		}
		else
		{
			if ($row->sex == 'm')
			{
				$row->portrait = Config::get('app.dir_image_member') . 'male.jpg';
			}
			else
			{
				$row->portrait = Config::get('app.dir_image_member') . 'female.jpg';
			}
			$row->isDefaultPortrait = TRUE;
		}

		$random = rand(1, 9999);
		$row->portrait .= "?v=" . $random;

		return Response::json($row, 200);
	}

	/**
	 * @return mixed
	 */
	public function saveMember()
	{
		$this->beforeFilter('checkCSRF');

		$row = Member::withTrashed()->find(Input::get('id'));

		if ($row->deleted_at != NULL)
		{
			$row->restore();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_RESTORE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Member::getTableName(),
				'foreign_key'   => $row->id,
			));

			Event::fire(core\EventType::USER, array($userEvent));
		}

		$sex = Input::get('sex', 'm');
		if ($sex != 'm' && $sex != 'w')
		{
			$sex = 'm';
		}

		$row->vorname  = Input::get('vorname');
		$row->nachname = Input::get('nachname');
		$row->sex      = $sex;
		$row->email    = Input::get('email');
		$row->gebdatum = Input::get('gebdatum');
		$row->adresse1 = Input::get('adresse1');
		$row->adresse2 = Input::get('adresse2');
		$row->plz      = Input::get('plz');
		$row->ort      = Input::get('ort');
		$row->land     = Input::get('land');
		$row->mobile   = Input::get('mobile');
		$row->tel      = Input::get('tel');
		$row->fax      = Input::get('fax');

		$row->firma          = Input::get('firma');
		$row->position       = Input::get('position');
		$row->beruf          = Input::get('beruf');
		$row->firma_adresse1 = Input::get('firma_adresse1');
		$row->firma_adresse2 = Input::get('firma_adresse2');
		$row->firma_plz      = Input::get('firma_plz');
		$row->firma_ort      = Input::get('firma_ort');
		$row->firma_land     = Input::get('firma_land');
		$row->firma_tel      = Input::get('firma_tel');
		$row->firma_fax      = Input::get('firma_fax');

		$row->login_username = Input::get('login_username');

		$pwd = Input::get('login_password');
		if (strlen($pwd) > 0)
		{
			$row->login_password = $pwd;
		}


		// Was the member updated?
		if ($row->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Member::getTableName(),
				'foreign_key'   => $row->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/members/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/members/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function insertMember()
	{
		$this->beforeFilter('checkCSRF');

		$sex = Input::get('sex', 'm');
		if ($sex != 'm' && $sex != 'w')
		{
			$sex = 'm';
		}

		$member           = new Member();
		$member->vorname  = Input::get('vorname');
		$member->nachname = Input::get('nachname');
		$member->email    = Input::get('email');
		$member->sex      = $sex;
		$member->gebdatum = Input::get('gebdatum');
		$member->adresse1 = Input::get('adresse1');
		$member->adresse2 = Input::get('adresse2');
		$member->plz      = Input::get('plz');
		$member->ort      = Input::get('ort');
		$member->land     = Input::get('land');
		$member->mobile   = Input::get('mobile');
		$member->tel      = Input::get('tel');
		$member->fax      = Input::get('fax');

		$member->firma          = Input::get('firma');
		$member->position       = Input::get('position');
		$member->beruf          = Input::get('beruf');
		$member->firma_adresse1 = Input::get('firma_adresse1');
		$member->firma_adresse2 = Input::get('firma_adresse2');
		$member->firma_plz      = Input::get('firma_plz');
		$member->firma_ort      = Input::get('firma_ort');
		$member->firma_land     = Input::get('firma_land');
		$member->firma_tel      = Input::get('firma_tel');
		$member->firma_fax      = Input::get('firma_fax');

		$member->login_username = Input::get('login_username');

		$pwd = Input::get('login_password');
		if (strlen($pwd) > 0)
		{
			$member->login_password = $pwd;
		}

		if ($member->save())
		{
			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_CREATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Member::getTableName(),
				'foreign_key'   => $member->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/members/message.success.create'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/members/message.error.create'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}


	/**
	 * @return mixed
	 */
	public function deleteMember()
	{
		$member = Member::find(Input::get('id'));

		if ($member->delete())
		{
			$memberName = '<strong>"' . $member->vorname . ' ' . $member->nachname . '"</strong>';
			$successMsg = Lang::get('admin/members/message.success.delete', compact('memberName'));

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_DELETE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => Member::getTableName(),
				'foreign_key'   => $member->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/members/message.error.delete'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 * Upload/Save Portrait
	 *
	 * @return mixed
	 */
	public
	function uploadPortrait()
	{
		if ($_FILES['file']['type'] != 'image/jpeg')
		{
			$this->httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('general.error'), "Nur JPG Bilder erlaubt!");

			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		if ($_FILES['file']['size'] > 4194300)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('general.error'), "Das Bild darf nicht grösser als 4 MB sein!");

			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		$member = Member::find(Input::get('id'));

		$destination = base_path() . '/' . Config::get('app.dir_image_member') . $member->id . ".jpg";
		move_uploaded_file($_FILES['file']['tmp_name'], $destination);

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::MEMBER_UPDATEPORTRAIT,
			'user_id'       => $this->getUserId(),
			'foreign_table' => Member::getTableName(),
			'foreign_key'   => $member->id
		));

		Event::fire(core\EventType::USER, array($userEvent));

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Ok, Portrait gespeichert");
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * Delete Portrait
	 *
	 * @return mixed
	 */
	public function deletePortrait()
	{
		$member = Member::find(Input::get('id'));

		try
		{
			unlink(Config::get('app.dir_image_member') . $member->id . '.jpg');
		}
		catch (Exception $e)
		{
		}

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::MEMBER_DELETEPORTRAIT,
			'user_id'       => $this->getUserId(),
			'foreign_table' => Member::getTableName(),
			'foreign_key'   => $member->id
		));

		Event::fire(core\EventType::USER, array($userEvent));

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Ok, Portrait gelöscht");
		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	/**
	 * @return mixed
	 */
	public function getDevices()
	{
		$member  = Member::find(Input::get('id'));
		$devices = new MemberDevices();

		if ($member)
		{
			$devices = $member->devices;

			$devices->each(function ($row)
			{
				// fnc
				$btnSend    = '<button type="button" class="btn btn-xs btn-success" ng-click="grid.appScope.onSend(row)" tooltips="" tooltip-title="PUSH-NACHRICHT SENDEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-send"></span></button>';
				$btnSentLog = '<button type="button" class="btn btn-xs btn-primary" ng-click="grid.appScope.onSentLog(row)" tooltips="" tooltip-title="SENDEPROTOKOLL" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-transfer"></span></button> ';

				$row->fnc = $btnSend;

				if ($row->notifications->count() > 0)
				{
					$row->fnc .= ' ' . $btnSentLog;
				}
			});
		}

		return Response::json($devices, 200);
	}

	/**
	 * Send Push Notification to Mobile App to all filtered Members
	 *
	 * @return mixed
	 */
	public function sendNotificationToSelectedMembers()
	{
		Log::error("sendNotificationToSelectedMembers");

		foreach (Input::get('recipientIds') as $recipientId)
		{
			$member = Member::find($recipientId);
			$devices = $member->devices;

			if ($devices)
			{
				foreach ($devices as $device)
				{
					$notification              = new MemberNotifications();
					$notification->message     = Input::get('message');
					$notification->certificate = Input::get('certificate');
					$notification->badgeicon   = Input::get('badgeicon');

					if ($this->sendMessage($device->devicetoken, $notification->message, $notification->certificate, $notification->badgeicon, $member->vorname, $member->nachname))
					{
						$notification = $device->notifications()->save($notification);

						// Event auslösen
						$userEvent = new UserEvent(array(
							'event'         => core\BaseEvent::MEMBER_SENDNOTIFICATION,
							'user_id'       => $this->getUserId(),
							'foreign_table' => MemberNotifications::getTableName(),
							'foreign_key'   => $notification->id
						));

						Event::fire(core\EventType::USER, array($userEvent));
					}
					else
					{
						// Event auslösen
						$userEvent = new UserEvent(array(
							'event'         => core\BaseEvent::MEMBER_SENDNOTIFICATION_FAILED,
							'user_id'       => $this->getUserId(),
							'foreign_table' => MemberNotifications::getTableName(),
							'foreign_key'   => $notification->id
						));

						Event::fire(core\EventType::USER, array($userEvent));
					}
				}
			}

		}

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/members/message.success.send_notification'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * Send Push Notification to Mobile App
	 *
	 * @return mixed
	 */
	public function sendNotification()
	{
		$member = Member::find(Input::get('id'));
		$device = MemberDevices::find(Input::get('deviceId'));

		$notification              = new MemberNotifications();
		$notification->message     = Input::get('message');
		$notification->certificate = Input::get('certificate');
		$notification->badgeicon   = Input::get('badgeicon');

		$msg = new Messages();

		if ($msg->sendMessage($device->devicetoken, $notification->message, $notification->certificate, $notification->badgeicon, $member->vorname, $member->nachname))
		{
			$notification = $device->notifications()->save($notification);

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::MEMBER_SENDNOTIFICATION,
				'user_id'       => $this->getUserId(),
				'foreign_table' => MemberNotifications::getTableName(),
				'foreign_key'   => $notification->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/members/message.success.send_notification'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		else
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/members/message.error.send_notification'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 *
	 */
	public function getSentLog()
	{
		$device = MemberDevices::find(Input::get('deviceId'));
		$sents  = $device->notifications; //->sortByDesc('sent_at');
		//		$sents->each(function ($row)
		//		{
		//
		//		});

		return Response::json($sents, 200);
	}

	/**
	 * ....
	 */
//	private function sendMessage($devicetoken, $message, $env, $badgeicon, $vorname, $nachname)
//	{
//
//		// Put your device token here (without spaces):
//		//$deviceToken = 'afff48d45a1760a0eb362bd298b6e7e0dd519460cc4818bb27540ce60c935015';
//		$deviceToken = $devicetoken;
//
//		// Put your private key's passphrase here:
//		$passphrase = 'safran4ever';
//
//		$ctx = stream_context_create();
//		if ($env == "prod")
//		{
//			$url = 'ssl://gateway.push.apple.com:2195';
//			stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/config/certificates/pushcert_prod.pem');
//		}
//		else
//		{
//			$url = 'ssl://gateway.sandbox.push.apple.com:2195';
//			stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/config/certificates/pushcert_dev.pem');
//		}
//		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
//
//		// Open a connection to the APNS server
//		$fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
//
//
//		if (!$fp)
//		{
//			Log::error('Push Notification Fehler: Failed to connect. Error: ' . $err . ' ErrStr: ' . $errstr);
//			return FALSE;
//		}
//
//
//		// TODO: erweitern: message für hinweis auf news (für badge) und message ohne hinweis auf news
//		// Create the payload body
//		$body['aps']       = array(
//			'alert' => $message,
//			'sound' => 'default',
//			'badge' => $badgeicon
//		);
//		$body['badgeicon'] = $badgeicon;
//
//
//		// Encode the payload as JSON
//		$payload = json_encode($body);
//
//		// Build the binary notification
//		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
//
//		// Send it to the server
//		$result = fwrite($fp, $msg, strlen($msg));
//
//		// Close the connection to the server
//		fclose($fp);
//
//
//		if (!$result)
//		{
//			return FALSE;
//		}
//
//		return TRUE;
//	}
}
