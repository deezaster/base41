<?php

use Carbon\Carbon;

class TemplatesController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}


	/**
	 *
	 */
	public function getEmailCompiled()
	{
		$template = TemplatesEmail::where("name", "=", Input::get('name'))->first();


		if (Input::get('name') == "Mitglied.Zugangsdaten")
		{
			$member = Member::find(Input::get('id'));

			$compiled = $template->content;
			$compiled = str_replace("{{member.vorname}}", $member->vorname, $compiled);
			$compiled = str_replace("{{member.nachname}}", $member->nachname, $compiled);
			$compiled = str_replace("{{member.login_username}}", $member->login_username, $compiled);
			$compiled = str_replace("{{member.login_password}}", $member->login_password, $compiled);

			$template->content = $compiled;
		}

		return Response::json($template, 200);
	}
}
