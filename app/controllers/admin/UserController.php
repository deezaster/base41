<?php

use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Groups\GroupNotFoundException;


class UserController extends AdminController {


	public function __construct()
	{
		// Call parent
		parent::__construct();
	}

	/**
	 * @return mixed
	 */
	public function getUsers()
	{

		//$rows = User::withTrashed()->whereRaw($whereActivated)->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->get();
		//$rows = User::whereRaw($whereActivated)->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->get();

		$rows = User::orderBy('last_name', 'asc')->orderBy('first_name', 'asc')->get();

		$rows->each(function ($row)
		{
			try
			{
				$user       = Sentry::getUserProvider()->findById($row->id);
				$userGroups = $user->groups()->lists('name');

				// user with no group?!
				if (count($userGroups) == 0)
				{
					$row->usergroup = "";
				}

				// get all groups
				$groups = "";
				foreach ($userGroups as $group)
				{
					$groups .= $group . ", ";
				}

				$row->usergroup = substr($groups, 0, -2);

				// Portrait vorhanden?
				if ($user->hasPortrait(base_path() . '/' . Config::get('app.dir_image_user')))
				{
					$row->portrait = Config::get('app.dir_image_user') . $user->id . '.jpg';
				}
				else
				{
					if ($user->sex == 'm')
					{
						$row->portrait = Config::get('app.dir_image_user') . 'male.jpg';
					}
					else
					{
						$row->portrait = Config::get('app.dir_image_user') . 'female.jpg';
					}
				}

				$random = rand(1, 9999);
				$row->portrait .= "?v=" . $random;

				// banned?
				$throttle = Sentry::findThrottlerByUserId($row->id);
				$isBanned = $throttle->isBanned();

				$row->banned = $isBanned;

				// fnc
				$btnEdit     = '<button class="btn btn-xs btn-default" ng-click="grid.appScope.onClick(row)" tooltips="" tooltip-title="EDITIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-pencil"></span></button>';
				$btnDelete   = '<button class="btn btn-xs btn-danger" ng-click="grid.appScope.onDelete(row)" tooltips="" tooltip-title="LÖSCHEN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-trash"></span></button>';
				$btnBan      = '<button class="btn btn-xs btn-warning" ng-click="grid.appScope.onBan(row)" tooltips="" tooltip-title="SPERREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-lock"></span></button>';
				$btnUnban    = '<button class="btn btn-xs btn-success" ng-click="grid.appScope.onUnban(row)" tooltips="" tooltip-title="ENTSPERREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="fa fa-unlock-alt"></span></button>';
				$btnActivate = '<button class="btn btn-xs btn-primary" ng-click="grid.appScope.onActivate(row)" tooltips="" tooltip-title="AKTIVIEREN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><span class="glyphicon glyphicon-flash"></span></button>';

				$userMe = Sentry::getUser();
				if ($userMe->id == $user->id)
				{
					$row->fnc = $btnEdit;
				}
				else
				{
					$row->fnc = $btnEdit;


					if (!$row->isSystemUser())
					{
						if ($isBanned == TRUE)
						{
							$row->fnc .= " " . $btnUnban;
						}
						else
						{
							$row->fnc .= " " . $btnBan;
						}

						$row->fnc .= " " . $btnDelete;
					}

				}
				if (!$row->activated)
				{
					$row->fnc .= " " . $btnActivate;
				}
			}
			catch (UserNotFoundException $e)
			{
				// user not found (deleted!)

			}
		});


		return Response::json($rows, 200);
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		try
		{
			$user = User::withTrashed()->find(Input::get('id'));

			$user->created_by_user = $user->getCreatedByUser();
			$user->updated_by_user = $user->getUpdatedByUser();
			$user->deleted_by_user = $user->getDeletedByUser();

			$userGroups = $user->groups()->lists('name', 'group_id');

			if (!$user->deleted_at)
			{
				$throttle = Sentry::findThrottlerByUserId(Input::get('id'));
				$user->attempts            = $throttle->attempts;
				$user->suspended_at        = $throttle->suspended_at;
				$banned_at                 = $throttle->banned_at;
				$user->suspendet_time      = $throttle->getSuspensionTime();
				$user->suspendet_remaining = $throttle->getRemainingSuspensionTime();
			}
			else
			{
				$user->attempts            = 0;
				$user->suspended_at        = NULL;
				$banned_at                 = NULL;
				$user->suspendet_time      = NULL;
				$user->suspendet_remaining = NULL;
			}


			if ($banned_at)
			{
				$user->banned_at = $banned_at->toDateTimeString();
			}
			else
			{
				$user->banned_at = "";
			}

			$user->act_code     = $user->readActivationCode();
			$user->res_pwd_code = $user->readPasswordResetCode();



			foreach ($userGroups as $key => $value)
			{
				$user->group = $key;
				break;  // Benutzer ist nur einer Gruppe zugeteilt (obwohl das System mehrere Gruppen erlaubt!)
			}


			// Portrait vorhanden?
			if ($user->hasPortrait(base_path() . '/' . Config::get('app.dir_image_user')))
			{
				$user->portrait          = Config::get('app.dir_image_user') . $user->id . '.jpg';
				$user->isDefaultPortrait = FALSE;
			}
			else
			{
				if ($user->sex == 'm')
				{
					$user->portrait = Config::get('app.dir_image_user') . 'male.jpg';
				}
				else
				{
					$user->portrait = Config::get('app.dir_image_user') . 'female.jpg';
				}
				$user->isDefaultPortrait = TRUE;
			}

			$random = rand(1, 9999);
			$user->portrait .= "?v=" . $random;

			$user->isSystemUser = $user->isSystemUser();

			return Response::json($user, 200);
		}
		catch (UserNotFoundException $e)
		{
			// user not found (deleted!?)
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));
			return Response::json($this->httpResponse->getAsArray(), 400);
		}
	}

	/**
	 * @return mixed
	 */
	public function saveUser()
	{
		$this->beforeFilter('checkCSRF');

		try
		{
			// Get the user
			$user = User::withTrashed()->find(Input::get('id'));

			if ($user->deleted_at != NULL)
			{
				$user->restore();

				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::USER_RESTORE,
					'user_id'       => $this->getUserId(),
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id,
				));

				Event::fire(core\EventType::USER, array($userEvent));
			}
		}
		catch (UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));

			return Response::json($this->httpResponse->getAsArray(), 200);
		}


		$sex = Input::get('sex', 'm');
		if ($sex != 'm' && $sex != 'w')
		{
			$sex = 'm';
		}

		$user->username   = Input::get('username');
		$user->first_name = Input::get('first_name');
		$user->last_name  = Input::get('last_name');
		$user->sex        = $sex;
		$user->email      = Input::get('email');

		// Was the user updated?
		if ($user->save())
		{
			try
			{
				$group = Sentry::getGroupProvider()->findById(Input::get('group'));
			}
			catch (GroupNotFoundException $e)
			{

				$group = Sentry::getGroupProvider()->findByName("User");
			}

			$user->addGroup($group);

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::USER_UPDATE,
				'user_id'       => $this->getUserId(),
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/users/message.success.update'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.error.update'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * @return mixed
	 */
	public function insertUser()
	{
		$this->beforeFilter('checkCSRF');

		$sex = Input::get('sex', 'm');
		if ($sex != 'm' && $sex != 'w')
		{
			$sex = 'm';
		}

		// Register the user
		$userData = array(
			'username'   => Input::get('username'),
			'first_name' => Input::get('first_name'),
			'last_name'  => Input::get('last_name'),
			'email'      => Input::get('email'),
			'sex'        => $sex,
			'password'   => Input::get('password'),
			'activated'  => TRUE
		);
		try
		{
			// Was the user created?
			if ($user = Sentry::getUserProvider()->create($userData))
			{
				try
				{
					$group = Sentry::getGroupProvider()->findById(Input::get('group'));
				}
				catch (GroupNotFoundException $e)
				{

					$group = Sentry::getGroupProvider()->findByName("User");
				}
				$user->addGroup($group);

				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::USER_CREATE,
					'user_id'       => $this->getUserId(),
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), Lang::get('admin/users/message.success.create'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}
		}
		catch (PasswordRequiredException $e)
		{
			// user not found (deleted!?)
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.error.create'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}

		$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.error.create'));
		return Response::json($this->httpResponse->getAsArray(), 200);
	}


	/**
	 * @return mixed
	 */
	public function deleteUser()
	{
		try
		{
			// Get the user
			$user = Sentry::getUserProvider()->findById(Input::get('id'));

			// Check if we are not trying to delete ourselves
			if ($user->id === Sentry::getId())
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.error.delete_myself'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}

			// Do we have permission to delete this user?
			if (!Sentry::getUser()->hasAccess('admin'))
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('auth/insufficient_permission_title'), Lang::get('auth/insufficient_permission'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}


			// Delete the user
			if ($user->delete())
			{
				$userName   = '<strong>"' . $user->first_name . ' ' . $user->last_name . '"</strong>';
				$successMsg = Lang::get('admin/users/message.success.delete', compact('userName'));

				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::USER_DELETE,
					'user_id'       => $this->getUserId(),
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);
				return Response::json($this->httpResponse->getAsArray(), 200);
			}
			else
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.error.delete'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}
		}
		catch (UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 * @return mixed
	 */
	public function banUser()
	{
		try
		{
			// Get the user
			$user = Sentry::getUserProvider()->findById(Input::get('id'));

			// Check if we are not trying to delete ourselves
			if ($user->id === Sentry::getId())
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.error.ban_myself'));

				return Response::json($this->httpResponse->getAsArray(), 200);
			}

			// Do we have permission?
			if (!Sentry::getUser()->hasAccess('admin'))
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('auth/insufficient_permission_title'), Lang::get('auth/insufficient_permission'));

				return Response::json($this->httpResponse->getAsArray(), 200);
			}


			// Ban the user
			$throttle = Sentry::findThrottlerByUserId(Input::get('id'));
			$throttle->ban();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::USER_BAN,
				'user_id'       => $this->getUserId(),
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));


			$userName   = '<strong>"' . $user->first_name . ' ' . $user->last_name . '"</strong>';
			$successMsg = Lang::get('admin/users/message.success.ban', compact('userName'));
			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);

			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		catch (UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 * @return mixed
	 */
	public function unbanUser()
	{
		try
		{
			// Get the user
			$user = Sentry::getUserProvider()->findById(Input::get('id'));

			// Do we have permission?
			if (!Sentry::getUser()->hasAccess('admin'))
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('auth/insufficient_permission_title'), Lang::get('auth/insufficient_permission'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}

			// Ban the user
			$throttle = Sentry::findThrottlerByUserId(Input::get('id'));
			$throttle->unban();

			// Event auslösen
			$userEvent = new UserEvent(array(
				'event'         => core\BaseEvent::USER_UNBAN,
				'user_id'       => $this->getUserId(),
				'foreign_table' => User::getTableName(),
				'foreign_key'   => $user->id
			));

			Event::fire(core\EventType::USER, array($userEvent));

			$userName   = '<strong>"' . $user->first_name . ' ' . $user->last_name . '"</strong>';
			$successMsg = Lang::get('admin/users/message.success.unban', compact('userName'));
			$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);

			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		catch (UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}

	/**
	 * @return mixed
	 */
	public function activateUser()
	{
		try
		{
			// Get the user
			$user = Sentry::getUserProvider()->findById(Input::get('id'));

			// Do we have permission?
			if (!Sentry::getUser()->hasAccess('admin'))
			{
				$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('auth/insufficient_permission_title'), Lang::get('auth/insufficient_permission'));
				return Response::json($this->httpResponse->getAsArray(), 200);
			}

			$user->activated    = TRUE;
			$user->activated_at = new DateTime;

			if ($user->save())
			{
				// Event auslösen
				$userEvent = new UserEvent(array(
					'event'         => core\BaseEvent::USER_ACTIVATE,
					'user_id'       => $this->getUserId(),
					'foreign_table' => User::getTableName(),
					'foreign_key'   => $user->id
				));

				Event::fire(core\EventType::USER, array($userEvent));

				$userName   = '<strong>"' . $user->first_name . ' ' . $user->last_name . '"</strong>';
				$successMsg = Lang::get('admin/users/message.success.activate', compact('userName'));
				$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), $successMsg);

				return Response::json($this->httpResponse->getAsArray(), 200);
			}

			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
		catch (UserNotFoundException $e)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('admin/users/message.user_not_found'));
			return Response::json($this->httpResponse->getAsArray(), 200);
		}
	}


	/**
	 * Upload/Save Portrait
	 *
	 * @return mixed
	 */
	public function uploadPortrait()
	{
		if ($_FILES['file']['type'] != 'image/jpeg')
		{
			$this->httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('general.error'), "Nur JPG Bilder erlaubt!");
			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		if ($_FILES['file']['size'] > 4194300)
		{
			$this->httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('general.error'), "Das Bild darf nicht grösser als 4 MB sein!");
			return Response::json($this->httpResponse->getAsArray(), 400);
		}

		// Get the user
		$user = Sentry::getUserProvider()->findById(Input::get('id'));

		$destination = base_path() . '/' . Config::get('app.dir_image_user') . $user->id . ".jpg";
		move_uploaded_file($_FILES['file']['tmp_name'], $destination);

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::USER_UPDATEPORTRAIT,
			'user_id'       => $this->getUserId(),
			'foreign_table' => User::getTableName(),
			'foreign_key'   => $user->id
		));

		Event::fire(core\EventType::USER, array($userEvent));

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Ok, Portrait gespeichert");
		return Response::json($this->httpResponse->getAsArray(), 200);
	}

	/**
	 * Delete Portrait
	 *
	 * @return mixed
	 */
	public function deletePortrait()
	{
		$user = Sentry::getUserProvider()->findById(Input::get('id'));

		try
		{
			unlink(Config::get('app.dir_image_user') . $user->id . '.jpg');
		}
		catch (Exception $e)
		{
		}

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::USER_DELETEPORTRAIT,
			'user_id'       => $this->getUserId(),
			'foreign_table' => User::getTableName(),
			'foreign_key'   => $user->id
		));

		Event::fire(core\EventType::USER, array($userEvent));

		$this->httpResponse->setData(HttpResponse::TYPE_SUCCESS, Lang::get('general.ok'), "Ok, Portrait gelöscht");
		return Response::json($this->httpResponse->getAsArray(), 200);
	}
}

