<?php



class AdminController extends AuthorizedController {

	public function __construct()
	{
		// Call parent
		parent::__construct();

		// Apply the admin auth filter
		$this->beforeFilter('checkAuthAdmin');
	}

	public function getIndex()
	{
		return View::make('admin');
	}

}
