<?php



class AuthorizedController extends BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('checkAuth');
	}

	public function getIndex()
	{
		return View::make('loggedin');
	}

	/**
	 * return the id of the loggedin user
	 *
	 * @return mixed
	 */
	protected function getUserId()
	{
		$me = Sentry::getUser();
		return $me->id;
	}
}
