<?php

use Illuminate\Routing\Controller;
use Cartalyst\Sentry\Hashing\Sha256Hasher;
use Carbon\Carbon;
use core\Messages;

/**
 * Class ServiceController
 *
 * Der ServiceController verarbeitet alle GET/POST Requests mit dem Prefix: 'api/v1/service'
 *
 * Folgende Requests sind implementiert:
 *
 * GET  api/v1/service/listConfigMobileApp: liefert alle Mobile App Konfigurationen
 * GET  api/v1/service/listUsers: liefert alle Benutzer
 * GET  api/v1/service/listMembers: liefert alle Mitglieder
 * GET  api/v1/service/listMembersFilteredByUpdatedAt: liefert alle Mitglieder mit 'updated_at' >= (heute - gewünschte Tage)
 * GET  api/v1/service/listAppointments: liefert alle Termine
 * GET  api/v1/service/listCurrentAppointments: liefert alle aktuellen Termine (ab heute)
 * GET  api/v1/service/listArticles: liefert alle Artikel
 * GET  api/v1/service/listCurrentArticles: liefert alle aktuellen Artikel (ab heute)
 * GET  api/v1/service/listFiles: liefert alle Files von einem bestimmten Verzeichnis
 * POST api/v1/service/saveDeviceToken: speichert das Device Token für ein Mitglied
 * POST api/v1/service/sendMessageToMembers: Nachrichten (Notification Message) an Mitglieder senden
 * POST api/v1/service/checkInLocation: aktuelle GPS Koordinaten des Mitglieds speichern
 *
 */
class ServiceController extends Controller {

	/**
	 * The response class
	 *
	 * @var ServiceResponse
	 */
	protected $serviceResponse;

	/**
	 * Create a new service controller instance.
	 */
	public function __construct()
	{
		$this->serviceResponse = new ServiceResponse();
		$this->loginAsSystemUser();
	}


	/**
	 * Return all mobile app configs
	 *
	 * @return ServiceResponse
	 */
	public function getConfigMobileApp()
	{
		$rows = Configmobileapp::get();
		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $rows);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all users
	 *
	 * @return ServiceResponse
	 */
	public function getUsers()
	{
		$sql = "SELECT u.id, u.email, u.password, u.username, u.first_name, u.last_name, g.name groupname, u.sex, u.activated, t.suspended, t.banned
      FROM users u
      JOIN users_groups ug ON ug.user_id = u.id
      JOIN groups g ON g.id = ug.group_id
      LEFT OUTER JOIN throttle t ON t.user_id = u.id";

		$rows = DB::select(DB::raw($sql));

		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $rows);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all members
	 *
	 * @return ServiceResponse
	 */
	public function getMembers()
	{
		// TEST:
		//		$salt = 'g8dZrY6XI3vEf3qc';
		//		$apikey = 'ds6cXA4qw7Ep7xOaiE23ViRbTs4KHH25'; // muss immer als parameter mitgeliefert werden
		//		$hashedkey = 'g8dZrY6XI3vEf3qcce8d9fee15d8b95e6b7135b1ee4d9f6f02c6cc2cef41fadc9ef8ac402f4bb906'; // speichern je mobile-benutzer

		//		$hasher = new Sha256Hasher();
		//		$check  = $hasher->checkhash(Input::get('apikey'), 'g8dZrY6XI3vEf3qcce8d9fee15d8b95e6b7135b1ee4d9f6f02c6cc2cef41fadc9ef8ac402f4bb906');
		//		Log::error("checked=" . $check);

		$rows = Member::orderBy('nachname', 'asc')->orderBy('vorname', 'asc')->get();

		// get current location (if exist)
		$rows->each(function ($row)
		{
			$row->latitude = "";
			$row->longitude =  "";
			$row->commentlocation = "";

			$location = $row->currentLocation();

			if ($location) {
				$row->latitude        = $location->latitude;
				$row->longitude       = $location->longitude;
				$row->commentlocation = $location->comment;
			}

		});

		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $this->preparingMembers($rows));
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return only members with updated_at >= ($now - days)
	 *
	 * @param String GET-Parameter 'days'
	 *
	 * @return ServiceResponse
	 */
	public function getMembersFilteredByUpdatedAt()
	{
		$limitDays       = Input::get('days', 90); // default 90 days
		$today           = Carbon::today();
		$updated_at_from = $today->subDays($limitDays);

		$rows = Member::where('updated_at', '>=', $updated_at_from)->orderBy('nachname', 'asc')->orderBy('vorname', 'asc')->get();

		// get current location (if exist)
		$rows->each(function ($row)
		{
			$row->latitude = "";
			$row->longitude =  "";
			$row->commentlocation = "";

			$location = $row->currentLocation();

			if ($location) {
				$row->latitude        = $location->latitude;
				$row->longitude       = $location->longitude;
				$row->commentlocation = $location->comment;
			}

		});

		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $this->preparingMembers($rows));
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return the newest location for all members
	 *
	 * @return ServiceResponse
	 */
	public function getMemberLocations()
	{
		$limitLocations    = Input::get('limit', 1);
		$locations = array();

		if ($limitLocations == 1)
		{
			$rows = Member::all();
			foreach ($rows as $row) {
				$location = $row->currentLocation();
				if ($location)
				{
					array_push($locations, $location);
				}
			}
		}
		else {
			$rows = Member::all();

			foreach ($rows as $row) {

				$limitedLocations = $row->locationsLimited($limitLocations);
				foreach ($limitedLocations as $location) {
					array_push($locations, $locs);
				}
			}
		}

		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $locations);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all appointments
	 *
	 * @return ServiceResponse
	 */
	public function getAppointments()
	{
		$rows = Appointment::orderBy('startdate', 'asc')->orderBy('starttime', 'asc')->get();
		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $rows);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all current appointments
	 *
	 * @return ServiceResponse
	 */
	public function getCurrentAppointments()
	{
		$today = Carbon::today();

		$rows = Appointment::where('startdate', '>=', $today)->orderBy('startdate', 'asc')->orderBy('starttime', 'asc')->get();
		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $rows);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all articles
	 *
	 * @return ServiceResponse
	 */
	public function getArticles()
	{
		$rows = Article::orderBy('articledate', 'desc')->get();
		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $rows);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all current articles
	 *
	 * @return ServiceResponse
	 */
	public function getCurrentArticles()
	{
		$today = Carbon::today();

		$rows = Article::where('articledate', '>=', $today->toDateString())->orderBy('articledate', 'desc')->get();
		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $rows);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Return all downloadable Files
	 *
	 * @param String GET-Parameter 'directoryId'
	 *
	 * @return ServiceResponse
	 */
	public function getFiles()
	{
		$filestructures = FileStructure::where('id', '=', Input::get('directoryId'))->get();

		$docs     = array();
		$node_ids = array();

		while (count($filestructures) > 0)
		{

			foreach ($filestructures as $filestructure)
			{

				array_push($node_ids, $filestructure->node_id);

				foreach ($filestructure->documents as $doc)
				{
					$doc->directory = $filestructure->name;
					array_push($docs, $doc);
				}
			}

			$curent_node_id = array_shift($node_ids);
			$filestructures = FileStructure::where('node_id_parent', '=', $curent_node_id)->orderBy('name', 'asc')->get();
		}

		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", $docs);
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Save device token for a member
	 *
	 * @param String POST-Parameter 'id' (member id)
	 * @param String POST-Parameter 'devicename'
	 * @param String POST-Parameter 'devicetoken'
	 *
	 * @return ServiceResponse
	 */
	public function saveDeviceToken()
	{
		$row = Member::where('id', '=', [Input::get('id')])->first();

		if ($row)
		{
			if (Input::get('devicetoken') != NULL && Input::get('devicetoken') != "(null)")
			{
				$found = FALSE;

				foreach ($row->devices as $device)
				{
					if ($device->name == Input::get('devicename'))
					{
						$found = TRUE;

						$device->devicetoken = Input::get('devicetoken');
						$device->save();

						// Event auslösen
						$userEvent = new UserEvent(array(
							'event'         => core\BaseEvent::SERVICE_MEMBER_SAVE_DEVICETOKEN,
							'user_id'       => $this->getUserId(),
							'foreign_table' => Member::getTableName(),
							'foreign_key'   => Input::get('id'),
							'eventtext1'    => $device->name,
							'eventtext2'    => $device->devicetoken
						));

						Event::fire(core\EventType::USER, array($userEvent));

						break;
					}

					$row->devicetoken = Input::get('devicetoken');
				}

				if (!$found)
				{
					$device              = new MemberDevices();
					$device->name        = Input::get('devicename');
					$device->devicetoken = Input::get('devicetoken');
					$row->devices()->save($device);

					// Event auslösen
					$userEvent = new UserEvent(array(
						'event'         => core\BaseEvent::SERVICE_MEMBER_SAVE_DEVICETOKEN,
						'user_id'       => $this->getUserId(),
						'foreign_table' => Member::getTableName(),
						'foreign_key'   => Input::get('id'),
						'eventtext1'    => $device->name,
						'eventtext2'    => $device->devicetoken
					));

					Event::fire(core\EventType::USER, array($userEvent));
				}
			}

			$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", NULL);
			$this->serviceResponse->setId(Input::get('id'));
			return Response::json($this->serviceResponse->getAsArray(), 200);
		}

		$this->serviceResponse->fill(ServiceResponse::TYPE_ERROR, Lang::get('service/message.member_not_found'), NULL);
		$this->serviceResponse->setId(Input::get('id'));
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Send Notification to member(s)
	 *
	 * @param String POST-Parameter 'id' (member id)
	 * @param String POST-Parameter 'recipients' (a comma separated list of member id's)
	 * @param String POST-Parameter 'message'
	 *
	 * @return ServiceResponse
	 */
	public function sendMessageToMembers()
	{
		$member = Member::where('id', '=', [Input::get('id')])->first();
		if (!$member)
		{
			$this->serviceResponse->fill(ServiceResponse::TYPE_ERROR, Lang::get('service/message.member_not_found'), NULL);
			$this->serviceResponse->setId(Input::get('id'));
			return Response::json($this->serviceResponse->getAsArray(), 200);
		}

		$recipients = explode(",", Input::get('recipients'));

		foreach ($recipients as $id)
		{
			$recipient = Member::find($id);

			if ($recipient)
			{
				foreach ($recipient->devices as $device)
				{
					$notification              = new MemberNotifications();
					$notification->message     = Input::get('message');
					$notification->certificate = 'prod';
					$notification->badgeicon   = '0';

					$msg = new Messages();

					if ($msg->sendMessage($device->devicetoken, $notification->message, $notification->certificate, $notification->badgeicon, $member->vorname, $member->nachname))
					{
						$notification = $device->notifications()->save($notification);

						// Event auslösen
						$userEvent = new UserEvent(array(
							'event'         => core\BaseEvent::SERVICE_MEMBER_SENDNOTIFICATION,
							'user_id'       => $this->getUserId(),
							'foreign_table' => MemberNotifications::getTableName(),
							'foreign_key'   => $notification->id,
							'eventtext1'    => $member->id
						));

						Event::fire(core\EventType::USER, array($userEvent));
					}
					else
					{
						// Event auslösen
						$userEvent = new UserEvent(array(
							'event'         => core\BaseEvent::SERVICE_MEMBER_SENDNOTIFICATION_FAILED,
							'user_id'       => $this->getUserId(),
							'foreign_table' => MemberNotifications::getTableName(),
							'foreign_key'   => $notification->id,
							'eventtext1'    => $member->id
						));

						Event::fire(core\EventType::USER, array($userEvent));
					}
				}
			}
		}

		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", array());
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Save current Location of the Member
	 *
	 * @param String POST-Parameter 'id' (member id)
	 * @param String POST-Parameter 'long' (longitude coordinate)
	 * @param String POST-Parameter 'lat' (latitude coordinte)
	 * @param String POST-Parameter 'accuracy' (10.0 precise, 65.0 imprecise)
	 * @param String POST-Parameter 'comment' (a optional commentary)
	 *
	 * @return ServiceResponse
	 */
	public function checkInLocation()
	{
		$member = Member::where('id', '=', [Input::get('id')])->first();
		if (!$member)
		{
			$this->serviceResponse->fill(ServiceResponse::TYPE_ERROR, Lang::get('service/message.member_not_found'), NULL);
			$this->serviceResponse->setId(Input::get('id'));
			return Response::json($this->serviceResponse->getAsArray(), 200);
		}

		$loc            = new MemberLocations();
		$loc->longitude = Input::get('long');
		$loc->latitude  = Input::get('lat');
		$loc->accuracy  = Input::get('accuracy');
		$loc->comment   = Input::get('comment');
		$member->locations()->save($loc);

		// Event auslösen
		$userEvent = new UserEvent(array(
			'event'         => core\BaseEvent::SERVICE_MEMBER_CHECKIN_LOCATION,
			'user_id'       => $this->getUserId(),
			'foreign_table' => MemberLocations::getTableName(),
			'foreign_key'   => Input::get('id'),
			'eventtext1'    => 'Long: ' . $loc->longitude . ' Lat: ' . $loc->latitude,
			'eventtext2'    => $loc->comment
		));

		Event::fire(core\EventType::USER, array($userEvent));


		$this->serviceResponse->fill(ServiceResponse::TYPE_SUCCESS, "OK", array());
		return Response::json($this->serviceResponse->getAsArray(), 200);
	}

	/**
	 * Preparing Members
	 *
	 * @param lluminate\Support\Collection Collection of Members
	 *
	 * @return mixed
	 */
	private function preparingMembers($rows)
	{
		$rows->each(function ($row)
		{
			// Passwort hashen!
			$hasher              = new Sha256Hasher();
			$row->login_password = $hasher->hash($row->login_password);

			// Portrait vorhanden?
			if ($row->hasPortrait(base_path() . '/' . Config::get('app.dir_image_member')))
			{
				$row->portrait = Config::get('app.dir_image_member') . $row->id . '.jpg';
			}
			else
			{
				if ($row->sex == 'm')
				{
					$row->portrait = Config::get('app.dir_image_member') . 'male.jpg';
				}
				else
				{
					$row->portrait = Config::get('app.dir_image_member') . 'female.jpg';
				}
			}
		});

		return $rows;
	}

	/**
	 * Log in as system user
	 */
	private function loginAsSystemUser()
	{
		$credentials = array(
			'username' => 'system',
			'password' => 'xxx',
		);

		try
		{
			Sentry::authenticate($credentials, TRUE);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			$this->serviceResponse->fill(ServiceResponse::TYPE_ERROR, "Required system user not found!", NULL);
			return Response::json($this->serviceResponse->getAsArray(), 401);
		}
	}

	/**
	 * return the id of the loggedin user
	 *
	 * @return mixed
	 */
	private function getUserId()
	{
		$me = Sentry::getUser();
		return $me->id;
	}
}

