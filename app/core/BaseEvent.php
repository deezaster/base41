<?php namespace core;

class BaseEvent {

	const AUTH_LOGIN = 'auth.login';
	const AUTH_LOGOFF = 'auth.logoff';
	const AUTH_REGISTER = 'auth.register';
	const AUTH_ACTIVATEACCOUNT = 'auth.activateaccount';
	const AUTH_ACTIVATEACCOUNT_FAILED = 'auth.activateaccount_failed';
	const AUTH_REQUESTPASSWORD = 'auth.requestpassword';
	const AUTH_RESETPASSWORD = 'auth.resetpassword';

	const ACCOUNT_UPDATEPROFILE = 'account.updateprofile';
	const ACCOUNT_UPDATEPORTRAIT = 'account.updateportrait';
	const ACCOUNT_DELETEPORTRAIT = 'account.deleteportrait';
	const ACCOUNT_UPDATEPASSWORD = 'account.updatepassword';

	const FILEMANAGER_UPLOADFILE = 'filemanager.uploadfile';
	const FILEMANAGER_DELETEFILE = 'filemanager.deletefile';
	const FILEMANAGER_UPDATEDESCRIPTION = 'filemanager.updatedescription';
	const FILEMANAGER_CREATEDIR = 'filemanager.createdir';
	const FILEMANAGER_DELETEDIR = 'filemanager.deletedir';
	const FILEMANAGER_RENAMEDIR = 'filemanager.renamedir';
	const FILEMANAGER_MOVEDIR = 'filemanager.movedir';
	const FILEMANAGER_SETDOWNLOADDIR = 'filemanager.setdownloaddir';

	const MEMBER_CREATE = 'member.create';
	const MEMBER_UPDATE = 'member.update';
	const MEMBER_DELETE = 'member.delete';
	const MEMBER_RESTORE = 'member.restore';
	const MEMBER_UPDATEPORTRAIT = 'member.updateportrait';
	const MEMBER_DELETEPORTRAIT = 'member.deleteportrait';
	const MEMBER_SENDNOTIFICATION = 'member.sendnotification';
	const MEMBER_SENDNOTIFICATION_FAILED = 'member.sendnotification_failed';
	const MEMBER_SENDLOGINCREDENTIALS = 'member.sendlogincredentials';

	const USER_CREATE = 'user.create';
	const USER_UPDATE = 'user.update';
	const USER_DELETE = 'user.delete';
	const USER_RESTORE = 'user.restore';
	const USER_BAN = 'user.ban';
	const USER_UNBAN = 'user.unban';
	const USER_ACTIVATE = 'user.activate';
	const USER_UPDATEPORTRAIT = 'user.updateportrait';
	const USER_DELETEPORTRAIT = 'user.deleteportrait';

	const EMAIL_SEND = 'email.send';
	const EMAIL_DELETE = 'email.delete';
	const EMAIL_RESTORE = 'email.restore';
	const EMAIL_ARCHIVE = 'email.archive';
	const EMAIL_UNARCHIVE = 'email.unarchive';

	const MASSEMAIL_CREATE = 'massemail.create';
	const MASSEMAIL_UPDATE = 'massemail.update';
	const MASSEMAIL_DELETE = 'massemail.delete';
	const MASSEMAIL_RESTORE = 'massemail.restore';
	const MASSEMAIL_SEND = 'massemail.send';
	const MASSEMAIL_ARCHIVE = 'massemail.archive';
	const MASSEMAIL_UNARCHIVE = 'massemail.unarchive';

	const EMAILTEMPLATE_UPDATE = 'emailtemplate.update';

	const ARTICLE_CREATE = 'article.create';
	const ARTICLE_UPDATE = 'article.update';
	const ARTICLE_DELETE = 'article.delete';
	const ARTICLE_RESTORE = 'article.restore';

	const APPOINTMENT_CREATE = 'appointment.create';
	const APPOINTMENT_UPDATE = 'appointment.update';
	const APPOINTMENT_DELETE = 'appointment.delete';
	const APPOINTMENT_RESTORE = 'appointment.restore';

	const CONFIGMOBILEAPP_UPDATE = 'configmobileapp.update';

	const SERVICE_MEMBER_SAVE_DEVICETOKEN = 'service_member.savedevicetoken';
	const SERVICE_MEMBER_SENDNOTIFICATION = 'service_member.sendnotification';
	const SERVICE_MEMBER_SENDNOTIFICATION_FAILED = 'service_member.sendnotification_failed';
	const SERVICE_MEMBER_CHECKIN_LOCATION = 'service_member.checkinlocation';

}