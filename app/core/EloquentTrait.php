<?php namespace core;


trait EloquentTrait {

	public static function getTableName()
	{
		return ((new self)->getTable());
	}
}