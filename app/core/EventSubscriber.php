<?php namespace core;

use Illuminate\Events;
use DateTime;
use Log;

class EventSubscriber {

	/**
	 * Outline the events this class will be listening for.
	 *
	 * @param  Illuminate\Events\Dispatcher $events
	 *
	 * @return void
	 */
	public function subscribe($events)
	{
		$events->listen(EventType::USER, 'core\EventSubscriber@processUserEvent');
	}

	/**
	 * Handle the USER event
	 *
	 * @param EventData $userEvent
	 *
	 * @return bool
	 */
	public function processUserEvent($userEvent)
	{
		$userEvent->event_at = (new DateTime)->format('Y-m-d H:i:s');
		$userEvent->save();
	}

}