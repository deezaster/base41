<?php namespace core;

use DateTime;

class Messages {

	/**
	 * ....
	 */
	function sendMessage($devicetoken, $message, $env, $badgeicon, $vorname, $nachname)
	{

		// Put your device token here (without spaces):
		//$deviceToken = 'afff48d45a1760a0eb362bd298b6e7e0dd519460cc4818bb27540ce60c935015';
		$deviceToken = $devicetoken;

		// Put your private key's passphrase here:
		$passphrase = 'cobra84ever';

		$ctx = stream_context_create();
		if ($env == "prod")
		{
			$url = 'ssl://gateway.push.apple.com:2195';
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/config/certificates/pushcert_prod.pem');
		}
		else
		{
			$url = 'ssl://gateway.sandbox.push.apple.com:2195';
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/config/certificates/pushcert_dev.pem');
		}
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);


		if (!$fp)
		{
			Log::error('Push Notification Fehler: Failed to connect. Error: ' . $err . ' ErrStr: ' . $errstr);
			return FALSE;
		}


		// TODO: erweitern: message für hinweis auf news (für badge) und message ohne hinweis auf news
		// Create the payload body
		$body['aps']       = array(
			'alert' => $message,
			'sound' => 'default',
			'badge' => $badgeicon
		);
		$body['badgeicon'] = $badgeicon;


		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		// Close the connection to the server
		fclose($fp);


		if (!$result)
		{
			return FALSE;
		}

		return TRUE;
	}

}