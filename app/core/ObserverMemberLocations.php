<?php namespace core;

use DB;
use Log;
use Config;
use MemberLocations;

class ObserverMemberLocations {

	public function saving($model)
	{
	}

	public function saved($model)
	{
		// Einträge löschen, falls die maximal erlaubt Anzahl überschritten ist
		$limit = Config::get('app.limit_saved_member_locations');
		$table = MemberLocations::getTableName();

		$rows = DB::table($table)
			->where('member_id', '=', $model->member_id)
			->orderBy('updated_at', 'desc')
			->take($limit)
			->get();

		$lastValidId = 0;
		foreach ($rows as $row) {
			$lastValidId = $row->id;
		}

		DB::table($table)
			->where('member_id', '=', $model->member_id)
			->where('id', '<', $lastValidId)
			->delete();
	}

}