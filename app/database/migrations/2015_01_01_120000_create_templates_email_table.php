<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesEmailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('templates_email', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('subject');
			$table->text('content')->nullable();

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('templates_email');
	}

}
