<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('users_event', function($table)
		{
			$table->increments('id')->unsigned();

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			$table->string('event');
			$table->timestamp('event_at');
			$table->text('foreign_table')->nullable();
			$table->text('foreign_key')->nullable();
			$table->text('eventtext1')->nullable();
			$table->text('eventtext2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('users_event');
	}

}
