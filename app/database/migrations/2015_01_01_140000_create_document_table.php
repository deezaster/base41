<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('document', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->integer('filestructure_id')->unsigned()->index();
			$table->foreign('filestructure_id')->references('id')->on('filestructure')->onDelete('cascade');
			$table->string('filename');
			$table->string('description');
			$table->string('type');
			$table->string('filesize');

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('document');
	}
}
