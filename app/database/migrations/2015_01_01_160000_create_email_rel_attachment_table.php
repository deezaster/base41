<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailRelAttachmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_rel_attachment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('email_id')->unsigned()->index();
			$table->foreign('email_id')->references('id')->on('email')->onDelete('cascade');
			$table->integer('document_id')->unsigned()->index();
			$table->foreign('document_id')->references('id')->on('document')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_rel_attachment');
	}

}
