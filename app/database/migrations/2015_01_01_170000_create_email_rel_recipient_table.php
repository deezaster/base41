<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailRelRecipientTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_rel_recipient', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('email_id')->unsigned()->index();
			$table->foreign('email_id')->references('id')->on('email')->onDelete('cascade');

			$table->integer('email_recipient_id')->unsigned();
			$table->string('email_recipient_type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_rel_recipient');
	}

}
