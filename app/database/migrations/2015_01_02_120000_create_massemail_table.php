<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMassemailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('massemail', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->string('subject');
			$table->string('sender_email');
			$table->string('sender_name');
			$table->text('content')->nullable();
			$table->timestamp('archived_at')->nullable();

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('massemail');
	}

}
