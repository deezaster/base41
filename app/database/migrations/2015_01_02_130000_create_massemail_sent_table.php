<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMassemailSentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('massemail_sent', function ($table)
		{
			$table->increments('id')->unsigned();
			$table->timestamp('sent_at');

			$table->integer('massemail_id')->unsigned()->index();
			$table->foreign('massemail_id')->references('id')->on('massemail')->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('massemail_sent');
	}

}
