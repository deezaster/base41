<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMassemailRelAttachmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('massemail_rel_attachment', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('massemail_id')->unsigned()->index();
			$table->foreign('massemail_id')->references('id')->on('massemail')->onDelete('cascade');
			$table->integer('document_id')->unsigned()->index();
			$table->foreign('document_id')->references('id')->on('document')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('massemail_rel_attachment');
	}

}
