<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMassemailRelRecipientTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('massemail_rel_recipient', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('massemail_sent_id')->unsigned()->index();
			$table->foreign('massemail_sent_id')->references('id')->on('massemail_sent')->onDelete('cascade');

			$table->integer('massemail_recipient_id')->unsigned();
			$table->string('massemail_recipient_type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('massemail_rel_recipient');
	}

}
