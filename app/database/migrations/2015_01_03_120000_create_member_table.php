<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('member', function ($table)
		{
			$table->increments('id')->unsigned();

			$table->string('vorname');
			$table->string('nachname');
			$table->string('adresse1')->nullable();
			$table->string('adresse2')->nullable();
			$table->string('plz')->nullable();
			$table->string('ort')->nullable();
			$table->string('land')->nullable();
			$table->string('sex');
			$table->string('email')->nullable();
			$table->string('mobile')->nullable();
			$table->string('tel')->nullable();
			$table->string('fax')->nullable();

			$table->date('gebdatum')->nullable();
			$table->string('beruf')->nullable();

			$table->string('firma')->nullable();
			$table->string('position')->nullable();
			$table->string('firma_adresse1')->nullable();
			$table->string('firma_adresse2')->nullable();
			$table->string('firma_plz')->nullable();
			$table->string('firma_ort')->nullable();
			$table->string('firma_land')->nullable();
			$table->string('firma_tel')->nullable();
			$table->string('firma_fax')->nullable();

			$table->string('login_username')->nullable();
			$table->string('login_password')->nullable();

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('member');
	}
}
