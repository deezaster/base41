<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('member_devices', function ($table)
		{
			$table->increments('id')->unsigned();

			$table->integer('member_id')->unsigned()->index();
			$table->foreign('member_id')->references('id')->on('member')->onDelete('cascade');

			$table->string('name'); // iPhone; iOS 8.2; Scale/2.00
			$table->string('devicetoken')->nullable(); // e2ebf04d7bb222d647f4c41887eeabaeeefbcf33a5313c2981bce684a3669b6a

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('member_devices');
	}
}
