<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('member_notifications', function ($table)
		{
			$table->increments('id')->unsigned();

			$table->integer('member_device_id')->unsigned()->index();
			$table->foreign('member_device_id')->references('id')->on('member_devices')->onDelete('cascade');

			$table->string('message');
			$table->string('certificate'); // prod, dev
			$table->integer('badgeicon'); // 0,1,2,3,4,5

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('member_notifications');
	}
}
