<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('member_locations', function ($table)
		{
			$table->increments('id')->unsigned();

			$table->integer('member_id')->unsigned()->index();
			$table->foreign('member_id')->references('id')->on('member')->onDelete('cascade');

			$table->decimal('longitude', 11, 8); // -180 bis 180
			$table->decimal('latitude', 10, 8); // -90  bis 90
			$table->decimal('accuracy', 4,1); // 10.0 = precise, 65.0 imprecise

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('member_locations');
	}
}
