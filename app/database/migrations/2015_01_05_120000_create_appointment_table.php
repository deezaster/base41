<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the table
		Schema::create('appointment', function ($table)
		{
			$table->increments('id')->unsigned();

			$table->string('title');
			$table->string('description')->nullable();

			$table->date('startdate')->nullable();
			$table->time('starttime')->nullable();

			$table->date('enddate')->nullable();
			$table->time('endtime')->nullable();

			$table->boolean('is_important')->default(0);

			$table->timestamps();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('updated_by')->unsigned()->nullable();

			$table->softDeletes();
			$table->integer('deleted_by')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the table
		Schema::drop('appointment');
	}
}
