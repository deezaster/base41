<?php

use Carbon\Carbon;

class AppointmentSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('appointment')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		$startdate = Carbon::createFromDate(2015, 8, 1)->toDateString();
		$enddate   = Carbon::createFromDate(2015, 8, 1)->toDateString();
		$starttime = Carbon::createFromTime(12, 0, 0)->toTimeString();
		$endtime   = Carbon::createFromTime(23, 0, 0)->toTimeString();

		Appointment::create(['title'      => '1. August Feier', 'description' => 'BBQ begleitet von einem fantastischen Feuerwerk',
									'startdate'  => $startdate, 'starttime' => $starttime, 'enddate' => $enddate, 'endtime' => $endtime,
									'created_by' => $adminUser->id]);

		$startdate = Carbon::createFromDate(2015, 12, 24)->toDateString();
		$enddate   = Carbon::createFromDate(2015, 12, 24)->toDateString();
		$starttime = Carbon::createFromTime(18, 0, 0)->toTimeString();
		$endtime   = Carbon::createFromTime(24, 0, 0)->toTimeString();

		Appointment::create(['title'      => 'Heiliger Abend', 'description' => 'Stille mich du Fröhliche...',
									'startdate'  => $startdate, 'starttime' => $starttime, 'enddate' => $enddate, 'endtime' => $endtime,
									'created_by' => $adminUser->id]);

		$startdate = Carbon::createFromDate(2015, 12, 31)->toDateString();
		$enddate   = Carbon::createFromDate(2016, 01, 01)->toDateString();
		$starttime = Carbon::createFromTime(18, 0, 0)->toTimeString();
		$endtime   = Carbon::createFromTime(04, 0, 0)->toTimeString();

		Appointment::create(['title'      => 'Silvesterparty', 'description' => 'Feuchtfröhlicher Rutsch ins neue Jahr',
									'startdate'  => $startdate, 'starttime' => $starttime, 'enddate' => $enddate, 'endtime' => $endtime,
									'created_by' => $adminUser->id]);
	}
}