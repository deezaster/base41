<?php

use Carbon\Carbon;

class ArticleSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('article')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		$publishdate = Carbon::createFromDate(2016, 1, 1)->toDateString();
		$articledate = Carbon::createFromDate(2015, 6, 30)->toDateString();
		Article::create(['title'       => 'Umfrage', 'content' => '<p>Bis am 1.1.2016 ist die Teilnahme möglich.</p><a href="http://www.xxx.com">zum Fragebogen</a>',
							  'articledate' => $articledate,
							  'publishdate' => $publishdate,
							  'created_by'  => $adminUser->id]);

		$articledate = Carbon::createFromDate(2015, 7, 1)->toDateString();
		Article::create(['title'       => 'Neuste Neuigkeit', 'content' => '<p>Pssst, hast Du das schon gehört?</p>',
							  'articledate' => $articledate,
							  'created_by'  => $adminUser->id]);
	}
}