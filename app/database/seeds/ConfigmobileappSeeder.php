<?php

use Carbon\Carbon;

class ConfigmobileappSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('configmobileapp')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		Configmobileapp::create(['key' => 'MemberUpdatePeriodDays', 'value' => '20', 'description' => 'Anzahl Tage...', 'is_editable' => 1, 'created_by' => $adminUser->id]);
		Configmobileapp::create(['key' => 'DocumentsDirectoryId', 'value' => '3', 'description' => 'Im Filemanager kann das Download-Verzeichnis definiert werden', 'is_editable' => 0, 'created_by' => $adminUser->id]);
		Configmobileapp::create(['key' => 'DocumentsDirectory', 'value' => 'upload', 'description' => 'Verzeichnis der Dokumente für den Download', 'is_editable' => 1, 'created_by' => $adminUser->id]);

	}
}