<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserSeeder');
		$this->call('GroupSeeder');
		$this->call('UserGroupSeeder');
		$this->call('ThrottleSeeder');

		$this->call('FileStructureSeeder');
		$this->call('DocumentSeeder');

		$this->call('TemplatesSeeder');
		$this->call('MassemailSeeder');

		$this->call('MemberSeeder');
		$this->call('AppointmentSeeder');
		$this->call('ArticleSeeder');
		$this->call('ConfigmobileappSeeder');


	}

}
