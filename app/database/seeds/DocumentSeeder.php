<?php

class DocumentSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('document')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		$node1 = FileStructure::where('name', '=', 'Verzeichnis 1')->first();
		$node3 = FileStructure::where('name', '=', 'Subdirectory ABC')->first();

		Document::create(['filestructure_id' => $node1->id, 'filename' => 'drink caipirinha.pdf', 'description' => 'Cocktail Caipirinha', 'type' => 'pdf', 'filesize' => '35433', 'created_by' => $adminUser->id]);
		Document::create(['filestructure_id' => $node1->id, 'filename' => 'drink cosmopolitan.pdf', 'description' => 'Cocktail Cosmopolitan', 'type' => 'pdf', 'filesize' => '35625', 'created_by' => $adminUser->id]);
		Document::create(['filestructure_id' => $node3->id, 'filename' => 'drink whiterussian.pdf', 'description' => 'Cocktail White Russian', 'type' => 'pdf', 'filesize' => '31199', 'created_by' => $adminUser->id]);
		Document::create(['filestructure_id' => $node3->id, 'filename' => 'drink tequilasunrise.pdf', 'description' => 'Cocktail Tequila Sunrise', 'type' => 'pdf', 'filesize' => '36746', 'created_by' => $adminUser->id]);


	}
}