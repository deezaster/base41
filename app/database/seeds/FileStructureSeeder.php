<?php



class FileStructureSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('filestructure')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		$node1 = FileStructure::create(['name' => 'Verzeichnis 1', 'type' => 'dir', 'node_id' => '01', 'created_by' => $adminUser->id]);
		$node2 = FileStructure::create(['name' => 'Verzeichnis Zwei', 'type' => 'dir', 'node_id' => '02', 'created_by' => $adminUser->id]);
		$node3 = FileStructure::create(['name' => 'Verzeichnis Three', 'type' => 'dir', 'node_id' => '03', 'created_by' => $adminUser->id]);

		$node1_1 = FileStructure::create(['node_id' => '011', 'node_id_parent' => $node1->node_id, 'name' => 'Unterverzeichnis 1-A', 'type' => 'dir', 'created_by' => $adminUser->id]);
		$node3_1 = FileStructure::create(['node_id' => '031', 'node_id_parent' => $node3->node_id, 'name' => 'Subdirectory ABC', 'type' => 'dir', 'created_by' => $adminUser->id]);

	}
}