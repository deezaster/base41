<?php



class GroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('groups')->delete();

		Sentry::getGroupProvider()->create(array(
			'name'        => 'Administrator',
			'permissions' => array(
				'admin' => 1,
				'users' => 1,
			)));

		Sentry::getGroupProvider()->create(array(
			'name'        => 'User',
			'permissions' => array(
				'admin' => 0,
				'users' => 1,
			)));


		Sentry::getGroupProvider()->create(array(
			'name'        => 'System',
			'permissions' => array(
			)));

	}
}