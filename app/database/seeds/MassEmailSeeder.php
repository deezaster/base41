<?php

use Carbon\Carbon;

class MassemailSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('massemail')->delete();
		DB::table('massemail_sent')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		$email1 = Massemail::create(['subject' => 'Testemail Eins', 'sender_email' => 'sen@der.com', 'sender_name' => 'Der Sender', 'created_by' => $adminUser->id]);
		$email2 = Massemail::create(['subject' => 'Testemail Zwei', 'sender_email' => 'sen@der.com', 'sender_name' => 'Der Sender', 'created_by' => $adminUser->id]);
		$email3 = Massemail::create(['subject' => 'Testemail Drei', 'sender_email' => 'sen@der.com', 'sender_name' => 'Der Sender', 'created_by' => $adminUser->id]);

		$doc1 = Document::where('filename', '=', 'drink caipirinha.pdf')->first();
		$doc2 = Document::where('filename', '=', 'drink whiterussian.pdf')->first();

		$email1->attachments()->attach($doc1->id);

		$email3->attachments()->attach($doc1->id);
		$email3->attachments()->attach($doc2->id);

		$sent1 = MassemailSent::create(['sent_at' => new Carbon(), 'massemail_id' => $email1->id]);
		$recipient1 = User::where ('username', '=', 'admin')->first();
		$sent1->users()->attach($recipient1->id);

		$sent2 = MassemailSent::create(['sent_at' => new Carbon(), 'massemail_id' => $email3->id]);
		$recipient2 = User::where ('username', '=', 'user1')->first();
		$sent2->users()->attach($recipient2->id);

	}
}