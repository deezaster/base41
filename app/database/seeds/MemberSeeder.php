<?php

use Carbon\Carbon;

class MemberSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('member_devices')->delete();
		DB::table('member')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		$gebdat = Carbon::createFromDate(1971, 4, 9)->toDateString();

		$member1 = Member::create(['vorname' => 'Andy', 'nachname' => 'Theiler', 'adresse1' => 'Strasse 1', 'adresse2' => 'Postfach',
											'plz'     => '6006', 'ort' => 'Luzern',
											'sex'     => 'm', 'gebdatum' => $gebdat, 'email' => 'andy@theilers.ch', 'beruf' => 'Informatiker',
											'firma'   => 'Xtreme Software GmbH', 'position' => 'Geschäftsführer',
											'mobile'  => '+41 79-249 00 01', 'created_by' => $adminUser->id, 'login_username' => 'andy', 'login_password' => 'admin']);

		MemberDevices::create(['member_id' => $member1->id, 'name' => 'iPhone; iOS 8.2; Scale/2.00', 'devicetoken' => 'afff48d45a1760a0eb362bd298b6e7e0dd519460cc4818bb27540ce60c935015', 'created_by' => $adminUser->id]);
		MemberDevices::create(['member_id' => $member1->id, 'name' => 'Android bla bla bla', 'devicetoken' => 'aabbf04d7bb222d647f4c41887eeabaeeefbcf33a5313c2981bce684a3669b6a', 'created_by' => $adminUser->id]);


		$gebdat = Carbon::createFromDate(1969, 1, 29)->toDateString();

		Member::create(['vorname' => 'Barbara', 'nachname' => 'Mehr Theiler', 'adresse1' => 'Ring 55',
							 'plz'     => '6312', 'ort' => 'Steinhausen',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'barbi@yabadoo.ch',
							 'mobile'  => '+41 79-392 00 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(2013, 1, 7)->toDateString();

		Member::create(['vorname'    => 'Lennox Flynn', 'nachname' => 'Theiler', 'adresse1' => 'Avenue 13',
							 'plz'        => '6312', 'ort' => 'Steinhausen',
							 'sex'        => 'm', 'gebdatum' => $gebdat, 'email' => 'lenny@noxx.com',
							 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1967, 11, 21)->toDateString();

		Member::create(['vorname' => 'David', 'nachname' => 'Bowie', 'adresse1' => 'Street 71',
							 'plz'     => '90211', 'ort' => 'London',
							 'sex'     => 'm', 'gebdatum' => $gebdat, 'email' => 'major@tom.com',
							 'mobile'  => '+41 76-987 00 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);
		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);
		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);

		$gebdat = Carbon::createFromDate(1935, 8, 30)->toDateString();

		Member::create(['vorname' => 'Audrey', 'nachname' => 'Hepburn', 'adresse1' => 'Rue 2b',
							 'plz'     => '78000', 'ort' => 'New York',
							 'sex'     => 'w', 'gebdatum' => $gebdat, 'email' => 'audrey@hollywood.com',
							 'mobile'  => '+41 79-222 33 01', 'created_by' => $adminUser->id]);
	}
}