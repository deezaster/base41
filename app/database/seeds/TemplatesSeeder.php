<?php

use Carbon\Carbon;

class TemplatesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('templates_email')->delete();

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');

		TemplatesEmail::create(['name'       => 'Mitglied.Zugangsdaten', 'subject' => 'BASE41: Deine Zugangsdaten',
										'content'    => '<p>Hallo {{member.vorname}} {{member.nachname}}</p><p>Hier deine Zugangsdaten:</p><p>&nbsp;&nbsp;Benutzername: <strong>{{member.login_username}}</strong><br />&nbsp;&nbsp;Passwort: <strong>{{member.login_password}}</strong>',
										'created_by' => $adminUser->id]);
	}
}