<?php



class ThrottleSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('throttle')->delete();


		try
		{
			// Find the user using the user id
			$throttle = Sentry::findThrottlerByUserLogin("user5");

			// Ban the user
			$throttle->ban();
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			echo 'User was not found.';
		}


	}
}