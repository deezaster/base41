<?php



class UserGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users_groups')->delete();

		$userUser1 = Sentry::getUserProvider()->findByLogin('user1');
		$userUser2 = Sentry::getUserProvider()->findByLogin('user2');
		$userUser3 = Sentry::getUserProvider()->findByLogin('user3');
		$userUser4 = Sentry::getUserProvider()->findByLogin('user4');
		$userUser5 = Sentry::getUserProvider()->findByLogin('user5');

		$adminUser = Sentry::getUserProvider()->findByLogin('admin');
		$systemUser = Sentry::getUserProvider()->findByLogin('system');

		$userGroup  = Sentry::getGroupProvider()->findByName('User');
		$adminGroup = Sentry::getGroupProvider()->findByName('Administrator');
		$systemGroup = Sentry::getGroupProvider()->findByName('System');

		// Assign the groups to the users
		$userUser1->addGroup($userGroup);
		$userUser2->addGroup($userGroup);
		$userUser3->addGroup($userGroup);
		$userUser4->addGroup($userGroup);
		$userUser5->addGroup($userGroup);

		$adminUser->addGroup($adminGroup);
		$systemUser->addGroup($systemGroup);
	}
}
