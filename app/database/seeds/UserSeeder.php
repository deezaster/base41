<?php



class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();

		$admin = Sentry::getUserProvider()->create(array(
			'username'   => 'admin',
			'email'      => 'admin@admin.com',
			'first_name' => 'Andy',
			'last_name'  => 'Theiler',
			'password'   => 'admin',
			'sex'        => 'm',
			'activated'  => 1
		));

		$admin->created_by = $admin->id;
		$admin->save();

		Sentry::getUserProvider()->create(array(
			'username'   => 'system',
			'email'      => 'system@x3m.ch',
			'first_name' => 'System',
			'last_name'  => '',
			'password'   => 'xxx',
			'sex'        => 'm',
			'activated'  => 1,
			'created_by' => $admin->id
		));

		Sentry::getUserProvider()->create(array(
			'username'   => 'user1',
			'email'      => 'user1@user.com',
			'first_name' => 'Barbara',
			'last_name'  => 'Mehr Theiler',
			'password'   => 'user',
			'sex'        => 'w',
			'activated'  => 1,
			'created_by' => $admin->id
		));
		Sentry::getUserProvider()->create(array(
			'username'   => 'user2',
			'email'      => 'user2@user.com',
			'first_name' => 'Lennox Flynn',
			'last_name'  => 'Theiler',
			'password'   => 'user',
			'sex'        => 'm',
			'activated'  => 0,
			'created_by' => $admin->id
		));
		Sentry::getUserProvider()->create(array(
			'username'   => 'user3',
			'email'      => 'user3@user.com',
			'first_name' => 'Leonardo',
			'last_name'  => 'Da Vinci',
			'password'   => 'user',
			'sex'        => 'm',
			'activated'  => 1,
			'created_by' => $admin->id
		));
		Sentry::getUserProvider()->create(array(
			'username'   => 'user4',
			'email'      => 'user4@user.com',
			'first_name' => 'Diego Armando',
			'last_name'  => 'Maradona',
			'password'   => 'user',
			'sex'        => 'm',
			'activated'  => 1,
			'created_by' => $admin->id
		));
		Sentry::getUserProvider()->create(array(
			'username'   => 'user5',
			'email'      => 'user5@user.com',
			'first_name' => 'Liv',
			'last_name'  => 'Angel',
			'password'   => 'user',
			'sex'        => 'w',
			'activated'  => 1,
			'created_by' => $admin->id
		));
	}
}