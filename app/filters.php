<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function ($request, $response)
{
	//
});

App::after(function ($request, $response)
{
	//
});


Route::filter('checkCSRF', function ()
{
	if (Session::token() != Request::header('csrf_token'))
	{
		$httpResponse = new HttpResponse();
		$httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('general.error'), Lang::get('auth/message.stupid_hacker'));
		return Response::json($httpResponse->getAsArray(), 418);
	}
});

// Check if the user is logged in
Route::filter('checkAuth', function ()
{
	if (!Sentry::check())
	{
		$httpResponse = new HttpResponse();
		$httpResponse->setData(HttpResponse::TYPE_WARNING, Lang::get('auth/message.timeout_session_title'), Lang::get('auth/message.timeout_session'));
		return Response::json($httpResponse->getAsArray(), 403);

	}
});

// Check if the user has access to the admin page
Route::filter('checkAuthAdmin', function ()
{
	if (!Sentry::getUser()->hasAccess('admin'))
	{
		$httpResponse = new HttpResponse();
		$httpResponse->setData(HttpResponse::TYPE_ERROR, Lang::get('auth/message.access_forbidden_title'), Lang::get('auth/message.access_forbidden'));

		return Response::json($httpResponse->getAsArray(), 403);

	}
});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/
//Route::filter('csrf', function ()
//{
//	$token = Request::ajax() ? Request::header('x-csrf-token') : Input::get('_token');
//
//	if (Session::token() != $token)
//	{
//		throw new Illuminate\Session\TokenMismatchException;
//	}
//});

