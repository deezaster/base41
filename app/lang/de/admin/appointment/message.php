<?php

return array(

	'appointment_not_found' => 'Dieser Termin existiert nicht (mehr)',

	'success'         => array(
		'create'          => 'Termin erfolgreich erstellt.',
		'update'          => 'Termin erfolgreich mutiert.',
		'restore'         => 'Termin erfolgreich wiederhergestellt.',
		'delete'          => 'Der Termin :appointmentName wurde gelöscht',
	),

	'error'           => array(
		'create'          => 'Der Termin konnte nicht gespeichert werden. Versuche es später nochmals.',
		'update'          => 'Der Termin konnte nicht gespeichert werden. Versuche es später nochmals.',
		'delete'          => 'Der Termin konnte nicht gelöscht werden. Versuche es später nochmals.',
	),

);
