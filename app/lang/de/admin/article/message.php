<?php

return array(

	'article_not_found' => 'Dieser Artikel existiert nicht (mehr)',

	'success'         => array(
		'create'          => 'Artikel erfolgreich erstellt.',
		'update'          => 'Artikel erfolgreich mutiert.',
		'restore'         => 'Artikel erfolgreich wiederhergestellt.',
		'delete'          => 'Der Artikel :articleName wurde gelöscht',
	),

	'error'           => array(
		'create'          => 'Der Artikel konnte nicht gespeichert werden. Versuche es später nochmals.',
		'update'          => 'Der Artikel konnte nicht gespeichert werden. Versuche es später nochmals.',
		'delete'          => 'Der Artikel konnte nicht gelöscht werden. Versuche es später nochmals.',
	),

);
