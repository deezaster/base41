<?php

return array(

	'success'         => array(
		'update'          => 'Konfiguration erfolgreich mutiert.',
	),

	'error'           => array(
		'update'          => 'Die Konfiguration konnte nicht gespeichert werden. Versuche es später nochmals.',
	),

);
