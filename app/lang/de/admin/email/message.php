<?php

return array(

	'email_not_found' => 'Dieses E-Mail existiert nicht (mehr)',

	'success'         => array(
		'create'          => 'E-Mail erfolgreich erstellt.',
		'createRecipient' => 'E-Mail Empfänger erfolgreich erstellt.',
		'update'          => 'E-Mail erfolgreich mutiert.',
		'restore'         => 'E-Mail erfolgreich wiederhergestellt.',
		'send'            => 'E-Mail erfolgreich versendet.',
		'delete'          => 'Das E-Mail :emailName wurde gelöscht',
		'archive'         => 'Das E-Mail :emailName wurde archiviert',
		'unarchive'       => 'Das E-Mail :emailName wurde wieder aktiviert',
	),

	'error'           => array(
		'create'          => 'Das E-Mail konnte nicht gespeichert werden. Versuche es später nochmals.',
		'createRecipient' => 'Der E-Mail Empfänger konnte nicht gespeichert werden. Versuche es später nochmals.',
		'update'          => 'Das E-Mail konnte nicht gespeichert werden. Versuche es später nochmals.',
		'delete'          => 'Das E-Mail konnte nicht gelöscht werden. Versuche es später nochmals.',
		'send'            => 'Das E-Mail konnte nicht versendet werden. Versuche es später nochmals oder überpüfe die Konfiguration.',
		'archive'         => 'Das E-Mail konnte nicht archiviert werden. Versuche es später nochmals.',
		'unarchive'       => 'Das E-Mail konnte nicht aktiviert werden. Versuche es später nochmals.',
	),

);
