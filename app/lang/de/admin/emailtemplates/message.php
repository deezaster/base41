<?php

return array(

	'template_not_found' => 'Dieses E-Mail Template existiert nicht (mehr)',

	'success'         => array(
		'update'          => 'E-Mail Template erfolgreich mutiert.',
	),

	'error'           => array(
		'update'          => 'Das E-Mail Template konnte nicht gespeichert werden. Versuche es später nochmals.',
	),

);
