<?php

use core\BaseEvent;

return array(

   BaseEvent::AUTH_LOGIN => 'Benutzer.Login',
	BaseEvent::AUTH_LOGOFF => 'Benutzer.Logoff',
	BaseEvent::AUTH_REGISTER => 'Benutzer.Registrierung',
	BaseEvent::AUTH_ACTIVATEACCOUNT => 'Benutzer.Aktivierung',
	BaseEvent::AUTH_ACTIVATEACCOUNT_FAILED => 'Benutzer.Aktivierung fehlgeschlagen',
	BaseEvent::AUTH_REQUESTPASSWORD => 'Benutzer.Passwort anfragen',
	BaseEvent::AUTH_RESETPASSWORD => 'Benutzer.Passwort zurücksetzen',

	BaseEvent::ACCOUNT_UPDATEPROFILE => 'Benutzer.Profil mutiert',
	BaseEvent::ACCOUNT_UPDATEPORTRAIT => 'Benutzer.Portrait mutiert',
	BaseEvent::ACCOUNT_DELETEPORTRAIT => 'Benutzer.Portrait gelöscht',
	BaseEvent::ACCOUNT_UPDATEPASSWORD => 'Benutzer.Passwort mutiert',

	BaseEvent::FILEMANAGER_UPLOADFILE => 'Filemanager.Upload Datei',
	BaseEvent::FILEMANAGER_DELETEFILE => 'Filemanager.Datei gelöscht',
	BaseEvent::FILEMANAGER_UPDATEDESCRIPTION => 'Filemanager.Dateibeschreibung mutiert',
	BaseEvent::FILEMANAGER_CREATEDIR => 'Filemanager.Verzeichnis erstellt',
	BaseEvent::FILEMANAGER_DELETEDIR => 'Filemanager.Verzeichnis gelöscht',
	BaseEvent::FILEMANAGER_RENAMEDIR => 'Filemanager.Verzeichnis umbenennt',
	BaseEvent::FILEMANAGER_MOVEDIR => 'Filemanager.Verzeichnis verschoben',
	BaseEvent::FILEMANAGER_SETDOWNLOADDIR => 'Filemanager.Downloadverzeichnis definiert',

	BaseEvent::MEMBER_CREATE => 'Mitglied.Erfasst',
	BaseEvent::MEMBER_UPDATE => 'Mitglied.Mutiert',
	BaseEvent::MEMBER_DELETE => 'Mitglied.Gelöscht',
	BaseEvent::MEMBER_RESTORE => 'Mitglied.Wiederhergestellt',
	BaseEvent::MEMBER_UPDATEPORTRAIT => 'Mitglied.Portrait mutiert',
	BaseEvent::MEMBER_DELETEPORTRAIT => 'Mitglied.Portrait gelöscht',
	BaseEvent::MEMBER_SENDNOTIFICATION => 'Mitglied.Nachricht an App gesendet',
	BaseEvent::MEMBER_SENDNOTIFICATION_FAILED => 'Mitglied.Nachricht an App fehlgeschlagen!',
	BaseEvent::MEMBER_SENDLOGINCREDENTIALS => 'Mitglied.Zugangsdaten gesendet',

	BaseEvent::USER_CREATE => 'Benutzer.Erfasst',
	BaseEvent::USER_UPDATE => 'Benutzer.Mutiert',
	BaseEvent::USER_DELETE => 'Benutzer.Gelöscht',
	BaseEvent::USER_RESTORE => 'Benutzer.Wiederhergestellt',
	BaseEvent::USER_BAN => 'Benutzer.Gesperrt',
	BaseEvent::USER_UNBAN => 'Benutzer.Entsperrt',
	BaseEvent::USER_ACTIVATE => 'Benutzer.Aktiviert',
	BaseEvent::USER_UPDATEPORTRAIT => 'Benutzer.Portrait mutiert',
	BaseEvent::USER_DELETEPORTRAIT => 'Benutzer.Portrait gelöscht',

	BaseEvent::EMAIL_DELETE => 'E-Mail.Gelöscht',
	BaseEvent::EMAIL_RESTORE => 'E-Mail.Wiederhergestellt',
	BaseEvent::EMAIL_SEND => 'E-Mail.Verschickt',
	BaseEvent::EMAIL_ARCHIVE => 'E-Mail.Archiviert',
	BaseEvent::EMAIL_UNARCHIVE => 'E-Mail.Aktiviert',

	BaseEvent::MASSEMAIL_CREATE => 'Massen E-Mail.Erfasst',
	BaseEvent::MASSEMAIL_UPDATE => 'Massen E-Mail.Mutiert',
	BaseEvent::MASSEMAIL_DELETE => 'Massen E-Mail.Gelöscht',
	BaseEvent::MASSEMAIL_RESTORE => 'Massen E-Mail.Wiederhergestellt',
	BaseEvent::MASSEMAIL_SEND => 'Massen E-Mail.Verschickt',
	BaseEvent::MASSEMAIL_ARCHIVE => 'Massen E-Mail.Archiviert',
	BaseEvent::MASSEMAIL_UNARCHIVE => 'Massen E-Mail.Aktiviert',

	BaseEvent::EMAILTEMPLATE_UPDATE => 'E-Mail Template.Mutiert',

	BaseEvent::ARTICLE_CREATE => 'Artikel.Erfasst',
	BaseEvent::ARTICLE_UPDATE => 'Artikel.Mutiert',
	BaseEvent::ARTICLE_DELETE => 'Artikel.Gelöscht',
	BaseEvent::ARTICLE_RESTORE => 'Artikel.Wiederhergestellt',
	
	BaseEvent::APPOINTMENT_CREATE => 'Termin.Erfasst',
	BaseEvent::APPOINTMENT_UPDATE => 'Termin.Mutiert',
	BaseEvent::APPOINTMENT_DELETE => 'Termin.Gelöscht',
	BaseEvent::APPOINTMENT_RESTORE => 'Termin.Wiederhergestellt',

	BaseEvent::CONFIGMOBILEAPP_UPDATE => 'Konfiguration MobileApp.Mutiert',

	BaseEvent::SERVICE_MEMBER_SAVE_DEVICETOKEN => 'Mobile App.Device Token gesendet',
	BaseEvent::SERVICE_MEMBER_SENDNOTIFICATION => 'Mobile App.Nachricht an Mitglied gesendet',
	BaseEvent::SERVICE_MEMBER_SENDNOTIFICATION_FAILED => 'Mobile App.Nachricht an Mitglied fehlgeschlagen!',
	BaseEvent::SERVICE_MEMBER_CHECKIN_LOCATION => 'Mobile App.Check-In aktueller Standort',
);
