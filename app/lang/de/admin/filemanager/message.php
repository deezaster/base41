<?php

return array(

	'success' => array(
		'createDir'         => 'Das Verzeichnis wurde erfolgreich erstellt.',
		'renameDir'         => 'Das Verzeichnis wurde erfolgreich umbenannt.',
		'moveDir'           => 'Das Verzeichnis wurde erfolgreich verschoben.',
		'deleteDir'         => 'Das Verzeichnis wurde erfolgreich gelöscht.',
		'uploadFile'        => 'Das Dokument wurde erfolgreich gespeichert.',
		'updateDescription' => 'Die Beschreibung wurde erfolgreich gespeichert',
		'setDownloadDir'    => 'Das Verzeichnis wurde markiert',
	),

	'error'   => array(
		'deleteDirWithChilfs' => 'Das Verzeichnis darf nicht gelöscht werden, solange noch ein Unterverzeichnis dran hängt!',
		'uploadFile'          => 'Das Dokument konnte nicht gespeichert werden!',
		'setDownloadDir'      => 'Das Verzeichnis konnte nicht markiert werden',

	),

);
