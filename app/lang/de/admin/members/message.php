<?php

return array(

	'member_not_found' => 'Dieses Mitglied existiert nicht (mehr)',

	'success'          => array(
		'create'            => 'Mitglied erfolgreich erstellt.',
		'update'            => 'Mitglied erfolgreich mutiert.',
		'delete'            => 'Das Mitglied :memberName wurde gelöscht',
		'send_notification' => 'Nachricht erfolgreich versendet.',
	),

	'error'            => array(
		'create'            => 'Das Mitglied konnte nicht gespeichert werden. Versuche es nochmals.',
		'update'            => 'Das Mitglied konnte nicht gespeichert werden. Versuche es nochmals.',
		'delete'            => 'Das Mitglied konnte nicht gelöscht werden. Versuche es nochmals.',
		'send_notification' => 'Nachricht konnte nicht versendet werden.',

	),

);
