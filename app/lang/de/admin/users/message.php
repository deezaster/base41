<?php

return array(

	'user_not_found' => 'Dieser Benutzer existiert nicht (mehr)',

	'success'        => array(
		'create'              => 'Benutzer erfolgreich erstellt.',
		'update'              => 'Benutzer erfolgreich mutiert.',
		'delete'              => 'Der Benutzer :userName wurde gelöscht',
		'activate'            => 'Benutzer :userName erfolgreich aktiviert.',
		'ban'                 => 'Benutzer :userName erfolgreich gesperrt.',
		'unban'               => 'Benutzer :userName erfolgreich wieder aktiviert.',
		'suspend'             => 'Benutzer erfolgreich temporär gesperrt.',
		'unsuspend'           => 'Benutzer erfolgreich entsperrt.',
		'sendActivationEmail' => 'Email mit dem Aktivierungslink verschickt.'
	),

	'error'          => array(
		'create'        => 'Der Benutzer konnte nicht gespeichert werden. Versuche es nochmals.',
		'update'        => 'Der Benutzer konnte nicht gespeichert werden. Versuche es nochmals.',
		'delete'        => 'Der Benutzer konnte nicht gelöscht werden. Versuche es nochmals.',
		'delete_myself' => 'Sich selber zu löschen ist nicht erlaubt.',
		'ban_myself'    => 'Sich selber zu sperren ist nicht wirklich clever.',
	),

);
