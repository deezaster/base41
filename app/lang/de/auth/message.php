<?php

return array(

	'account_already_exists'         => 'Dieses Konto existiert bereits!',
	'account_not_activated'          => 'Dieses Konto ist noch nicht aktiviert! Du hast von uns ein E-Mail mit dem Aktivierungslink erhalten.',
	'account_suspended'              => 'Zu viele Loginversuche! Dein Login ist für die nächsten 5 Minuten gesperrt.',
	'account_banned'                 => 'Dieses Konto ist gesperrt! Ein Login ist nicht mehr möglich. Kontaktiere den Support.',

	'login_success'                  => 'Das Login war erfolgreich.',
	'login_failed'                   => 'Die Zugangsdaten sind nicht korrekt! Überprüfe die Eingaben und versuches es nochmals.',
	'register_success'               => 'Die Registrierung muss noch bestätigt werden. Das E-Mail mit dem Aktivierungslink ist unterwegs...',
	'register_success_email_subject' => 'Bestätigung Registrierung',

	'forgot_password_email_subject'  => 'Neues Passwort anfordern',
	'forgot_password_success'        => 'Das E-Mail mit den Instruktionen für ein neues Passwort wurde soeben verschickt.',
	'forgot_password_reset_error'    => 'Es gab ein Problem beim Zurücksetzen des Passwortes. Bitte fordere nochmals das Passwort an.',
	'forgot_password_renew_error'    => 'Es gab ein Problem beim Zurücksetzen des Passwortes. Bitte versuche es nochmal.',
	'forgot_password_renew_success'  => 'Das Passwort wurde erfolgreich geändert. Du kannst dich jetzt einloggen.',

	'activate_error'                 => 'Aktivierung nicht möglich! Bitte überprüfen den Aktivierungscode und versuche es nochmal.',
	'activate_success'               => 'Das Konto wurde erfolgreich aktiviert. Du kannst dich jetzt einloggen.',

	'profile_update_success'         => 'Änderungen am Benutzerkonto sind gespeichert.',
	'password_update_success'        => 'Passwort erfolgreich geändert.',
	'email_update_success'           => 'E-Mailadresse erfolgreich geändert.',
	'current_password_incorrect'     => 'Das aktuelle Passwort ist falsch',

	'logoff_success_title'           => 'Bis zum nächsten Mal',
	'logoff_success'                 => 'Du hast dich erfolgreich abgemeldet',
	'timeout_session_title'          => 'Bitte einloggen',
	'timeout_session'                => 'Die Sitzungszeit ist abgelaufen. Bitte nochmals anmelden.',
	'access_forbidden_title'         => 'Keine Berechtigung',
	'access_forbidden'               => 'Du bist nicht berechtigt diese Seite anzusehen!',
	'insufficient_permission_title'  => 'Keine Berechtigung',
	'insufficient_permission'        => 'Dir fehlt die Berechtigung um die gewünschte Aktion durchzuführen',

	'stupid_hacker'                  => 'I’m a teapot !!! you stupid hacker :D',


);
