<?php

return array(

	'never'         => 'Nie',
	'no'            => 'Nein',
	'yes'           => 'Ja',

	'attention'     => 'Achtung',
	'error'         => 'Fehler',
	'ok'            => 'OK',

	'general_error' => 'Ein bisher unbekannter Fehler ist aufgetreten. Versuche es nochmals oder wende dich direkt an den Support.',
);
