<?php

return array(

	'member_not_found'    => 'Dieses Mitglied existiert nicht (mehr)',

	'success'             => array(
		'send_notification' => 'Nachricht erfolgreich versendet.',
	),

	'error'               => array(
		'send_notification' => 'Nachricht konnte nicht versendet werden.',

	),

);
