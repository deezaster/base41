<?php



/**
 * Class BaseModel
 */
class BaseModel extends Eloquent {

	public static function boot()
	{
		parent::boot();

		// set the created by user
		static::creating(function ($model)
		{
			if (Sentry::check())
			{
				$user              = Sentry::getUser();
				$model->created_by = $user->id;
			}
		});

		// set the updated by user
		static::updating(function ($model)
		{
			if (Sentry::check())
			{
				$user              = Sentry::getUser();
				$model->updated_by = $user->id;
			}
		});

	}

	/**
	 * Get the created by user
	 *
	 * @return  User
	 */
	public function getCreatedByUser()
	{
		try
		{
			return Sentry::findUserById($this->created_by);
		}
		catch (UserNotFoundException $e)
		{
			return NULL;
		}
	}

	/**
	 * Get the updated by user
	 *
	 * @return  User
	 */
	public function getUpdatedByUser()
	{
		if ($this->updated_by == NULL)
		{
			return NULL;
		}

		try
		{
			return Sentry::findUserById($this->updated_by);
		}
		catch (UserNotFoundException $e)
		{
			return NULL;
		}
	}


}