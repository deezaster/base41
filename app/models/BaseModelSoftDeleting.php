<?php



/**
 * Class BaseModel
 */
class BaseModelSoftDeleting extends BaseModel {

	public static function boot()
	{
		parent::boot();


		// set the deleted by user
		static::deleted(function ($model)
		{
			if (Sentry::check())
			{
				// Manueller Update von 'deleted_by', weil Laravel bei delete() nur den Timestamp 'delete_at' anpasst!
				$user = Sentry::getUser();
				DB::update('UPDATE ' . $model::getTableName() . ' SET deleted_by = ? WHERE id = ?', array($user->id, $model->id));
			}
		});


		// set the updated by user
		static::restoring(function ($model)
		{
			if (Sentry::check())
			{
				$user              = Sentry::getUser();
				$model->updated_by = $user->id;
				$model->deleted_by = NULL;
			}
		});
	}

	/**
	 * Get the deleted by user
	 *
	 * @return  User
	 */
	public function getDeletedByUser()
	{
		if ($this->deleted_by == NULL)
		{
			return NULL;
		}

		try
		{
			return Sentry::findUserById($this->deleted_by);
		}
		catch (UserNotFoundException $e)
		{
			return NULL;
		}
	}
}