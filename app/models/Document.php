<?php



class Document extends BaseModelSoftDeleting{

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'document';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	public function directory()
	{
		return $this->belongsTo('Filestructure', 'filestructure_id', 'id');
	}

	public function massemails()
	{
		return $this->belongsToMany('Massemail', 'massemail_rel_attachment', 'document_id', 'massemail_id');
	}

	public function emails()
	{
		return $this->belongsToMany('Email', 'email_rel_attachment', 'document_id', 'email_id');
	}

}