<?php



class Email extends BaseModelSoftDeleting {

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'email';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;


	public function attachments()
	{
		return $this->belongsToMany('Document', 'email_rel_attachment', 'email_id', 'document_id');
	}

	public function users()
	{
		return $this->morphedByMany('User', 'email_recipient', 'email_rel_recipient', 'email_id');
	}

	public function members()
	{
		return $this->morphedByMany('Member', 'email_recipient', 'email_rel_recipient', 'email_id');
	}
}