<?php



class FileStructure extends BaseModelSoftDeleting{

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'filestructure';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	public function documents()
	{
		return $this->hasMany('Document', 'filestructure_id');
	}
}