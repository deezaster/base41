<?php



/**
 * The HTTP Response class
 *
 * Response for the frontend (e.g. angular) to show a notification message (alert)
 */
class HttpResponse {

	const TYPE_SUCCESS = 'success';
	const TYPE_INFO = 'info';
	const TYPE_WARNING = 'warning';
	const TYPE_ERROR = 'error';

	const ALERT_DURATION = 5000; // milliseconds (5000 = 5 seconds)
	const ALERT_CONTAINER = "body";
	const ALERT_PLACEMENT = "toast-top-right";

	protected $title;
	protected $content;
	protected $type;
	protected $alertDuration;
	protected $alertPlacement;
	protected $alertContainer;

	protected $id;



	public function __construct()
	{
	}

	/**
	 * Creates a Message
	 *
	 * @param string $type           The message type: One of the TYPE_* constants
	 * @param string $title          The title of the message
	 * @param string $content        The messsage text
	 * @param int    $alertDuration  The duration in seconds the alert is showing
	 * @param string $alertPlacement The placement of the alertmessage (default: top-left)
	 * @param string $alertContainer The CSS container (default: body
	 */
	public function setData($type, $title, $content, $alertDuration = self::ALERT_DURATION, $alertPlacement = self::ALERT_PLACEMENT, $alertContainer = self::ALERT_CONTAINER)
	{
		$this->setType($type);
		$this->setTitle($title);
		$this->setContent($content);
		$this->setAlertDuration($alertDuration);
		$this->setAlertPlacement($alertPlacement);
		$this->setAlertContainer($alertContainer);
		$this->setId(null);
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		if (NULL === $type)
		{
			$type = self::TYPE_SUCCESS;
		}
		elseif ($type != self::TYPE_SUCCESS && $type != self::TYPE_INFO && $type != self::TYPE_WARNING && $type != self::TYPE_ERROR)
		{
			$type = self::TYPE_SUCCESS;
		}

		$this->type = $type;
	}

	/**
	 * @return int
	 */
	public function getAlertDuration()
	{
		return $this->alertDuration;
	}

	/**
	 * @param int $alertDuration
	 */
	public function setAlertDuration($alertDuration)
	{
		$this->alertDuration = $alertDuration;
	}

	/**
	 * @return mixed
	 */
	public function getAlertPlacement()
	{
		return $this->alertPlacement;
	}

	/**
	 * @param mixed $alertPlacement
	 */
	public function setAlertPlacement($alertPlacement)
	{
		$this->alertPlacement = $alertPlacement;
	}

	/**
	 * @return mixed
	 */
	public function getAlertContainer()
	{
		return $this->alertContainer;
	}

	/**
	 * @param mixed $alertContainer
	 */
	public function setAlertContainer($alertContainer)
	{
		$this->alertContainer = $alertContainer;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}


	/**
	 * @return array
	 */
	public function getAsArray()
	{
		$arr = array("response" => array("id" => $this->getId(), "title" => $this->getTitle(), "msg" => $this->getContent(), "type" => $this->getType(), "duration" => $this->getAlertDuration(), "placement" => $this->getAlertPlacement(), "container" => $this->getAlertContainer()));

		return $arr;
	}
}