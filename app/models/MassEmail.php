<?php



class Massemail extends BaseModelSoftDeleting {

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'massemail';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;
	
	public function sent()
	{
		return $this->hasMany('MassemailSent', 'massemail_id');
	}


	public function attachments()
	{
		return $this->belongsToMany('Document', 'massemail_rel_attachment', 'massemail_id', 'document_id');
	}
} 