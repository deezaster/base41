<?php



class MassemailSent extends BaseModelSoftDeleting {

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'massemail_sent';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	public function users()
	{
		return $this->morphedByMany('User', 'massemail_recipient', 'massemail_rel_recipient', 'massemail_sent_id');
	}

	public function members()
	{
		return $this->morphedByMany('Member', 'massemail_recipient', 'massemail_rel_recipient', 'massemail_sent_id');
	}

	public function massemail()
	{
		return $this->belongsTo('Massemail', 'massemail_id', 'id');
	}


}