<?php

use Cartalyst\Sentry\Hashing\Sha256Hasher;

class Member extends BaseModelSoftDeleting {

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'member';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	/**
	 * Attributes that should be hashed.
	 *
	 * @var array
	 */
	protected $hashableAttributes = array(//'login_password',
	);

	/**
	 * Returns the polymorphic Many-to-Many relation
	 *
	 * [Member] -- n:n --> [Email]
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function emails()
	{
		return $this->morphToMany('Email', 'email_recipient', 'email_rel_recipient', 'email_recipient_id');
	}

	/**
	 * Returns the polymorphic Many-to-Many relation
	 *
	 * [Member] -- n:n --> [MassemailSent]
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function massemails()
	{
		return $this->morphToMany('MassemailSent', 'massemail_recipient', 'massemail_rel_recipient', 'massemail_recipient_id');
	}

	/**
	 * Returns the One-to-Many relationship
	 *
	 * [Member] -- 1:n --> [MemberDevices]
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function devices()
	{
		return $this->hasMany('MemberDevices', 'member_id');
	}


	/**
	 * Returns the One-to-Many relationship
	 *
	 * [Member] -- 1:n --> [MemberLocations]
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function locations()
	{
		return $this->hasMany('MemberLocations', 'member_id');
	}

	/**
	 * Returns the newest $limitRows locations
	 *
	 * @param  $limitRows
	 * @return Illuminate\Support\Collection
	 */
	public function locationsLimited($limitRows)
	{
		$rows = $this->hasMany('MemberLocations', 'member_id')->orderBy('updated_at', 'desc')->get();

		$countRows = 0;
		$filteredRows = $rows->filter(function ($row) use (&$countRows, $limitRows)
		{
			$countRows++;
			if ($countRows > $limitRows) {
				return false;
			}
			return true;

		});

		return $filteredRows;
	}

	/**
	 * Returns the current (newest) location of this member
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function currentLocation()
	{
		$hasLocations = $this->hasMany('MemberLocations', 'member_id');

		if ($hasLocations->count() > 0)
		{
			$locId = MemberLocations::where('member_id', '=', $this->id)->max('id');
			$location = MemberLocations::find($locId);

			return $location;
		}
		return null;
	}

	/**
	 * Has the member a portrait?
	 *
	 * @param $dirImage
	 * @return bool
	 */
	public function hasPortrait($dirImage)
	{
		if (File::exists($dirImage . $this->id . '.jpg'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Set a given attribute on the model
	 *
	 * @param  string $key
	 * @param  mixed  $value
	 * @return void
	 */
	public function setAttribute($key, $value)
	{
		// Hash required fields when necessary
		if (in_array($key, $this->hashableAttributes) and !empty($value))
		{
			$value = $this->hash($value);
		}

		return parent::setAttribute($key, $value);
	}

	/**
	 * Check string against hashed string
	 *
	 * @param  string $string
	 * @param  string $hashedString
	 * @return bool
	 */
	public function checkHash($string, $hashedString)
	{
		$hasher = new Sha256Hasher();
		return $hasher->checkHash($string, $hashedString);
	}

	/**
	 * Hash a string
	 *
	 * @param  string $string
	 * @return string
	 */
	public function hash($string)
	{
		$hasher = new Sha256Hasher();
		return $hasher->hash($string);
	}
}