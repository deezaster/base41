<?php



class MemberDevices extends BaseModelSoftDeleting {

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'member_devices';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	public function member()
	{
		return $this->belongsTo('Member', 'member_id', 'id');
	}

	public function notifications()
	{
		return $this->hasMany('MemberNotifications', 'member_device_id');
	}
}