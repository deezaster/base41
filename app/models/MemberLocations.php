<?php



class MemberLocations extends BaseModel {


	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'member_locations';


	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	public function member()
	{
		return $this->belongsTo('Member', 'member_id', 'id');
	}


}