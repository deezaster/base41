<?php

/**
 * The Service Response class
 *
 * Response for the Service caller (e.g. Mobile App)
 */
class ServiceResponse {

	const TYPE_SUCCESS = 'success';
	const TYPE_INFO = 'info';
	const TYPE_WARNING = 'warning';
	const TYPE_ERROR = 'error';

	protected $message;
	protected $data;
	protected $type;

	protected $id;



	public function __construct()
	{
	}

	/**
	 * Fill the class with data
	 *
	 * @param string $type           The message type: One of the TYPE_* constants
	 * @param string $message        The message
	 * @param string $data           The messsage text
	 */
	public function fill($type, $message, $data)
	{
		$this->setType($type);
		$this->setMessage($message);
		$this->setData($data);
		$this->setId(null);
	}

	/**
	 * @return mixed
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param mixed $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param mixed $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}



	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		if (NULL === $type)
		{
			$type = self::TYPE_SUCCESS;
		}
		elseif ($type != self::TYPE_SUCCESS && $type != self::TYPE_INFO && $type != self::TYPE_WARNING && $type != self::TYPE_ERROR)
		{
			$type = self::TYPE_SUCCESS;
		}

		$this->type = $type;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return array
	 */
	public function getAsArray()
	{
		$arr = array("response" => array("type" => $this->getType(), "message" => $this->getMessage(), "id" => $this->getId()), "data" => $this->getData());

		// {"response":  {"type":"success","message":"OK","id":null},
		//      "data": [{"id": 45,"name": "Theiler"...},
		//			        {"id": 99,"name": "Mehr"...}]
		return $arr;
	}
}