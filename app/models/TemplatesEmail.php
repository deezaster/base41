<?php

use Cartalyst\Sentry\Hashing\Sha256Hasher;

class TemplatesEmail extends BaseModelSoftDeleting {

	/**
	 * Datebase tablename
	 *
	 * @var string
	 */
	protected $table = 'templates_email';

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;


}