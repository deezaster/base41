<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryModel;

class User extends SentryModel {

	/**
	 * Soft Deleting
	 */
	use SoftDeletingTrait;
	protected $dates = ['deleted_at'];

	/**
	 * Returns database table name
	 *
	 * $tableName = Model::getTableName();
	 *
	 */
	use core\EloquentTrait;

	public static function boot()
	{
		parent::boot();

		// set the created by user
		static::creating(function ($model)
		{
			if (Sentry::check())
			{
				$user              = Sentry::getUser();
				$model->created_by = $user->id;
			}
		});

		// set the updated by user
		static::updating(function ($model)
		{
			if (Sentry::check())
			{
				$user              = Sentry::getUser();
				$model->updated_by = $user->id;
			}
		});

		// set the deleted by user
		static::deleted(function ($model)
		{
			if (Sentry::check())
			{
				// Manueller Update von 'deleted_by', weil Laravel bei delete() nur den Timestamp 'delete_at' anpasst!
				$user = Sentry::getUser();
				DB::update('UPDATE ' . $model::getTableName() . ' SET deleted_by = ? WHERE id = ?', array($user->id, $model->id));
			}
		});


		// set the updated by user
		static::restoring(function ($model)
		{
			if (Sentry::check())
			{
				$user              = Sentry::getUser();
				$model->updated_by = $user->id;
				$model->deleted_by = NULL;
			}
		});
	}


	public function emails()
	{
		return $this->morphToMany('Email', 'email_recipient', 'email_rel_recipient', 'email_recipient_id');
	}

	public function massemails()
	{
		return $this->morphToMany('MassemailSent', 'massemail_recipient', 'massemail_rel_recipient', 'massemail_recipient_id');
	}

	public function hasPortrait($dirImage)
	{
		if (File::exists($dirImage . $this->id . '.jpg'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// get hidden attribute
	public function readActivationCode() {

		return $this->activation_code;
	}

	// get hidden attribute
	public function readPasswordResetCode() {

		return $this->password_reset_code;
	}

	/**
	 * Is this a system user?
	 *
	 * @return bool
	 */
	public function isSystemUser()
	{
		$userGroups = $this->groups()->lists('name', 'group_id');
		return in_array("System", $userGroups);
	}

	/**
	 * Get the created by user
	 *
	 * @return  User
	 */
	public function getCreatedByUser()
	{
		try
		{
			return User::withTrashed()->find($this->created_by);
		}
		catch (UserNotFoundException $e)
		{
			return NULL;
		}
	}

	/**
	 * Get the updated by user
	 *
	 * @return  User
	 */
	public function getUpdatedByUser()
	{
		if ($this->updated_by == NULL)
		{
			return NULL;
		}

		try
		{
			return User::withTrashed()->find($this->updated_by);
		}
		catch (UserNotFoundException $e)
		{
			return NULL;
		}
	}

	/**
	 * Get the deleted by user
	 *
	 * @return  User
	 */
	public function getDeletedByUser()
	{
		if ($this->deleted_by == NULL)
		{
			return NULL;
		}

		try
		{
			return User::withTrashed()->find($this->deleted_by);
		}
		catch (UserNotFoundException $e)
		{
			return NULL;
		}
	}
}
