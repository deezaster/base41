<?php

use core\BaseEvent;

class UserEvent extends Eloquent {

	protected $table = 'users_event';

	public $timestamps = false;

	protected $fillable = ['user_id', 'event', 'foreign_table', 'foreign_key', 'eventtext1', 'eventtext2'];

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

}