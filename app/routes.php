<?php


// get's von js/lib/angular/angular.*****.map ignorieren
Route::group(array('prefix' => 'js/lib'), function ()
{
});

/*
|--------------------------------------------------------------------------
| Common Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'api/v1'), function ()
{
	Route::get('auth/isLoggedin', 'AuthController@isLoggedin');
	Route::post('auth/login', 'AuthController@login');
	Route::post('auth/register', 'AuthController@register');

	Route::get('auth/getUser', 'AuthController@getUser');

	Route::post('auth/register/isUsernameUnique', 'AuthController@isUsernameUnique');
	Route::post('auth/register/isEmailUnique', 'AuthController@isEmailUnique');

	Route::post('auth/activateaccount', 'AuthController@activateAccount');
	Route::post('auth/requestpwd', 'AuthController@requestPassword');
	Route::post('auth/resetpwd', 'AuthController@resetPassword');
	Route::get('auth/groups', 'AuthController@getGroups');

	Route::get('auth/logoff', 'AuthController@logoff');

	Route::get('useraccount/get', 'UserAccountController@get');
	Route::post('useraccount/save', 'UserAccountController@save');
	Route::post('useraccount/save_pwd', 'UserAccountController@savePassword');
	Route::get('useraccount/deletePortrait', 'UserAccountController@deletePortrait');
	Route::post('useraccount/uploadPortrait', 'UserAccountController@uploadPortrait');
});




/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'api/v1/admin'), function ()
{
	Route::get('init', 'DashboardController@init');
	Route::get('userevents', 'DashboardController@getUserEvents');
	Route::get('emails', 'DashboardController@getEmails');

	Route::get('users', 'UserController@getUsers');
	Route::post('users/edit', 'UserController@getUser');
	Route::post('users/save', 'UserController@saveUser');
	Route::post('users/insert', 'UserController@insertUser');
	Route::post('users/delete', 'UserController@deleteUser');
	Route::post('users/ban', 'UserController@banUser');
	Route::post('users/unban', 'UserController@unbanUser');
	Route::post('users/activate', 'UserController@activateUser');
	Route::post('users/deletePortrait', 'UserController@deletePortrait');
	Route::post('users/uploadPortrait', 'UserController@uploadPortrait');

	Route::get('emails', 'EmailController@getEmails');
	Route::post('email/edit', 'EmailController@getEmail');
	Route::post('email/restore', 'EmailController@restoreEmail');
	Route::post('email/send', 'EmailController@sendEmail');
	Route::post('email/delete', 'EmailController@deleteEmail');
	Route::post('email/archive', 'EmailController@archiveEmail');
	Route::post('email/unarchive', 'EmailController@unarchiveEmail');
	Route::get('email/getRecipients', 'EmailController@getRecipients');
	Route::post('email/getMassemailBySentId', 'EmailController@getMassemailBySentId');
	Route::post('email/getEmail', 'EmailController@getEmail');
	Route::post('email/getEmailsByUserId', 'EmailController@getEmailsByUserId');
	Route::post('email/getEmailsByMemberId', 'EmailController@getEmailsByMemberId');
	Route::post('email/sendEmailLoginCredentials', 'EmailController@sendEmailLoginCredentials');

	Route::get('massemail', 'MassemailController@getEmails');
	Route::post('massemail/edit', 'MassemailController@getEmail');
	Route::post('massemail/save', 'MassemailController@saveEmail');
	Route::post('massemail/insert', 'MassemailController@insertEmail');
	Route::post('massemail/delete', 'MassemailController@deleteEmail');
	Route::post('massemail/archive', 'MassemailController@archiveEmail');
	Route::post('massemail/unarchive', 'MassemailController@unarchiveEmail');
	Route::post('massemail/sendToUsers', 'MassemailController@sendEmailToUsers');
	Route::post('massemail/sendToMembers', 'MassemailController@sendEmailToMembers');
	Route::post('massemail/getRecipients', 'MassemailController@getRecipients');
	Route::post('massemail/insertRecipient', 'MassemailController@insertRecipient');
	Route::post('massemail/setTo', 'MassemailController@setTo');
	Route::get('massemail/getEmailsForDropdown', 'MassemailController@getEmailsForDropdown');
	Route::post('massemail/getSentLog', 'MassemailController@getSentLog');
	Route::post('massemail/getSentLogRecipients', 'MassemailController@getSentLogRecipients');

	Route::get('filemanager', 'FilemanagerController@getStructure');
	Route::post('filemanager/getFilesPerNode', 'FilemanagerController@getFilesPerNode');
	Route::post('filemanager/getFiles', 'FilemanagerController@getFiles');
	Route::post('filemanager/uploadFile', 'FilemanagerController@uploadFile');
	Route::post('filemanager/deleteFile', 'FilemanagerController@deleteFile');
	Route::post('filemanager/deleteDir', 'FilemanagerController@deleteDir');
	Route::post('filemanager/renameDir', 'FilemanagerController@renameDir');
	Route::post('filemanager/createDir', 'FilemanagerController@createDir');
	Route::post('filemanager/moveDir', 'FilemanagerController@moveDir');
	Route::post('filemanager/updateDescription', 'FilemanagerController@updateDescription');
	Route::post('filemanager/setDownloadDir', 'FilemanagerController@setDownloadDir');

	Route::get('members', 'MemberController@getMembers');
	Route::post('members/edit', 'MemberController@getMember');
	Route::post('members/save', 'MemberController@saveMember');
	Route::post('members/insert', 'MemberController@insertMember');
	Route::post('members/delete', 'MemberController@deleteMember');
	Route::post('members/deletePortrait', 'MemberController@deletePortrait');
	Route::post('members/uploadPortrait', 'MemberController@uploadPortrait');
	Route::post('members/getDevices', 'MemberController@getDevices');
	Route::post('members/sendNotification', 'MemberController@sendNotification');
	Route::post('members/sendNotificationToSelectedMembers', 'MemberController@sendNotificationToSelectedMembers');
	Route::post('members/getSentLog', 'MemberController@getSentLog');

	Route::get('article', 'ArticleController@getArticles');
	Route::post('article/edit', 'ArticleController@getArticle');
	Route::post('article/save', 'ArticleController@saveArticle');
	Route::post('article/insert', 'ArticleController@insertArticle');
	Route::post('article/delete', 'ArticleController@deleteArticle');

	Route::get('appointment', 'AppointmentController@getAppointments');
	Route::post('appointment/edit', 'AppointmentController@getAppointment');
	Route::post('appointment/save', 'AppointmentController@saveAppointment');
	Route::post('appointment/insert', 'AppointmentController@insertAppointment');
	Route::post('appointment/delete', 'AppointmentController@deleteAppointment');


	Route::get('configmobileapp', 'ConfigmobileappController@getConfigmobileapps');
	Route::post('configmobileapp/edit', 'ConfigmobileappController@getConfigmobileapp');
	Route::post('configmobileapp/save', 'ConfigmobileappController@saveConfigmobileapp');

	Route::get('emailtemplates', 'EmailController@getEmailTemplates');
	Route::post('emailtemplate/edit', 'EmailController@getEmailTemplate');
	Route::post('emailtemplate/save', 'EmailController@saveEmailTemplate');

	Route::post('templates/getEmailCompiled', 'TemplatesController@getEmailCompiled');
});


/*
|--------------------------------------------------------------------------
| Webservice Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'api/v1/service'), function ()
{
	Route::get('listConfigMobileApp', 'ServiceController@getConfigMobileApp');

	Route::get('listUsers', 'ServiceController@getUsers');
	Route::get('listMembers', 'ServiceController@getMembers');
	Route::get('listMembersFilteredByUpdatedAt', 'ServiceController@getMembersFilteredByUpdatedAt');
	Route::get('listMemberLocations', 'ServiceController@getMemberLocations');

	Route::post('saveDeviceToken', 'ServiceController@saveDeviceToken');
	Route::post('sendMessageToMembers', 'ServiceController@sendMessageToMembers');

	Route::get('listAppointments', 'ServiceController@getAppointments');
	Route::get('listCurrentAppointments', 'ServiceController@getCurrentAppointments');

	Route::get('listArticles', 'ServiceController@getArticles');
	Route::get('listCurrentArticles', 'ServiceController@getCurrentArticles');

	Route::get('listFiles', 'ServiceController@getFiles');

	Route::post('checkInLocation', 'ServiceController@checkInLocation');

});

/*
|--------------------------------------------------------------------------
| Website Routes
|--------------------------------------------------------------------------
*/
Route::get('/', array('as' => 'home', 'uses' => 'WebsiteController@getIndex')); // Website Home










