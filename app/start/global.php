<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path() . '/commands',
	app_path() . '/controllers',
	app_path() . '/core',
	app_path() . '/models',
	app_path() . '/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a rotating log file setup which creates a new file each day.
|
*/

$logFile = 'log-' . php_sapi_name() . '.txt';

Log::useDailyFiles(storage_path() . '/logs/' . $logFile);

// TODO: wieder rausnehmen und in der local.php aktivieren
// eigentlich nur für localhost. will es auf dem webserver einige zeit laufen lassen (s. local.php)
$dateNow     = (new DateTime)->format('Y-m-d');
$pathLogFile = storage_path() . '/logs/trace-' . $dateNow . '.log';

// log request to file
App::before(function ($request) use ($pathLogFile)
{
	$time_now = (new DateTime)->format('Y-m-d H:i:s');
	$start    = PHP_EOL . '==REQUEST==| ' . $request->method() . ' ' . $request->path() . ' @ ' . $time_now . PHP_EOL;

	File::append($pathLogFile, $start);
});


// log route to file
Route::matched(function ($route, $request) use ($pathLogFile)
{
	$actions    = $route->getAction();
	$controller = (array_key_exists('controller', $actions)) ? '> ' . $actions['controller'] : '';
	$start      = '== ROUTE ==| ' . $route->uri() . ' ' . $controller . PHP_EOL;
	File::append($pathLogFile, $start);
});

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/
App::error(function (Exception $exception, $code)
{
	//Log::error($exception);

	if (!Config::get('app.debug'))
	{
		if ($code == 404)
		{
			return Response::make(View::make('error/404'), 404);
		}
		//		switch ($code)
		//		{
		//			case 403:
		//				return Response::make(View::make('error/403'), 403);
		//
		//			case 500:
		//				return Response::make(View::make('error/500'), 500);
		//
		//			default:
		//				return Response::make(View::make('error/404'), 404);
		//		}
	}
});




/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenace mode is in effect for this application.
|
*/

App::down(function ()
{
	return Response::make(View::make('error/503'), 503);
});


/*
|--------------------------------------------------------------------------
| Require The Observables File
|--------------------------------------------------------------------------
*/
require app_path() . '/observables.php';

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/
require app_path() . '/filters.php';



