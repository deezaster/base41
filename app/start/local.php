<?php
/*
|--------------------------------------------------------------------------
| ONLY WHEN RUNNING ON LOCALHOST !!!!
|--------------------------------------------------------------------------
*/

/*$dateNow     = (new DateTime)->format('Y-m-d');
$pathLogFile = storage_path() . '/logs/trace-' . $dateNow . '.log';*/

// FIXME: wieder aktivieren für den betrieb auf dem webserver!
// siehe global.php

/*// log request to file
App::before(function ($request) use ($pathLogFile)
{
	$time_now = (new DateTime)->format('Y-m-d H:i:s');
	$start    = PHP_EOL . '==REQUEST==| ' . $request->method() . ' ' . $request->path() . ' @ ' . $time_now . PHP_EOL;

	File::append($pathLogFile, $start);
});


// log route to file
Route::matched(function ($route, $request) use ($pathLogFile)
{
	$actions    = $route->getAction();
	$controller = (array_key_exists('controller', $actions)) ? '> ' . $actions['controller'] : '';
	$start      = '== ROUTE ==| ' . $route->uri() . ' ' . $controller . PHP_EOL;
	File::append($pathLogFile, $start);
});*/


// log sql to file
Event::listen('illuminate.query', function ($sql, $bindings, $time) use ($pathLogFile)
{
	$sql = str_replace(array('%', '?'), array('%%', '%s'), $sql);
	$sql = vsprintf($sql, $bindings);

	$time_now = (new DateTime)->format('Y-m-d H:i:s');
	$log      = '==  SQL  ==| ' . $time_now . ' | ' . $sql . ' | ' . $time . 'ms' . PHP_EOL;
	File::append($pathLogFile, $log);
});




