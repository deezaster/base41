<?php

use core\EventSubscriber;
use core\UserLogFile;
use core\UserLogDB;
use Way\Tests\Assert;
use Mockery as M;


class UserLogTest extends TestCase {

	public function setup()
	{
		parent::setup();
	}

	protected function tearDown()
	{
		M::close();
	}


	public function testEventOnLogin()
	{
		$user = User::find(1);
		$eventData = new EventData($user->id);
		Event::fire('auth.login', array($eventData));
	}
	public function testEventOnLoginWithoutUser()
	{
		$user = User::find(1);
		Event::fire('auth.login', null);
	}
}