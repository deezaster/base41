@extends('emails/layouts/default')

@section('content')
    <p>Hallo {{ $user->first_name }} {{ $user->last_name }}</p>

    <p>Bitte auf den untenstehen Link klicken um ein neues Passwort zu erfassen:</p>

    <p><a href="{{ $forgotPasswordUrl }}">{{ $forgotPasswordUrl }}</a></p>

@stop
