<!DOCTYPE HTML>
<html lang="de-CH">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	@yield('content')

	<p>Solltest Du Fragen zu Deinem Konto haben, kontaktiere bitte unseren Support: support@x3m.ch</p>

	<p>
	Xtreme Software GmbH, Schweiz<br />
	<a href="mailto:info@x3m.ch">info@x3m.ch</a>
	</p>

</body>
</html>
