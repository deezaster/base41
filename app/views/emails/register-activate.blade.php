@extends('emails/layouts/default')

@section('content')

    <p>Hallo {{ $user->first_name }} {{ $user->last_name }}</p>

    <p>
        Wir freuen uns, dass Du dabei bist.<br />
        Bevor es losgehen kann, musst Du noch Deine E-Mail Adresse bestätigen. Klicke dazu auf den folgenden Link:
    </p>

    <p><a href="{{ $activationUrl }}">{{ $activationUrl }}</a></p>


@stop

