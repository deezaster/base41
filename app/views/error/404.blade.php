<!DOCTYPE html>
<html lang="en" ng-app="theApp">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <title>{{ Config::get('app.name') }}</title>

    <meta name="description" content="{{ Config::get('app.meta_description') }}">
    <meta name="keywords" content="{{ Config::get('app.meta_keywords') }}" />
    <meta name="author" content="{{ Config::get('app.meta_author') }}" />
    <link rel="author" href="{{ Config::get('app.meta_author_link') }}">



    {{-- Styles --}}
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/app.css') }}">


    {{-- Web Fonts --}}
    {{--<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">--}}
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,400,500,700' rel='stylesheet' type='text/css'>


</head>
<body>
<div class="container-fluid">
    <div class="row">

        <div class="col-md-12">

            <h1 class="page-header">SORRY, THIS PAGE DOESN'T EXISTS</h1>


            <h3>What does this mean?</h3>

            <p>
                We couldn't find the page you requested on our servers. We're really sorry
                about that. It's our fault, not yours. We'll work hard to get this page
                back online as soon as possible.
            </p>

            <p>
                Perhaps you would like to go to our <a href="{{ URL::route('home'); }}">home page</a>?
            </p>
        </div>

    </div>
</div>

</body>
</html>
