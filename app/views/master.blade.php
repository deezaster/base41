<!DOCTYPE html>
<html lang="en" ng-app="theApp">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <title>{{ Config::get('app.name') }}</title>

    <meta name="description" content="{{ Config::get('app.meta_description') }}">
    <meta name="keywords" content="{{ Config::get('app.meta_keywords') }}" />
    <meta name="author" content="{{ Config::get('app.meta_author') }}" />
    <link rel="author" href="{{ Config::get('app.meta_author_link') }}">



    {{-- Styles --}}
    {{--<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/bootstrap/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/bootstrap/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/fontawesome/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/ionicons/ionicons.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/angularmotion/angular-motion.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/loadingbar/loading-bar.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/uiselect/select.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/uiselect/selectize.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/uiselect/select2.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/uigrid/ui-grid-unstable.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/angularstrap/angular-strap.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/angulartoastr/angular-toastr.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/angulartooltips/angular-tooltips.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/textangular/textAngular.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/ngjstree/style.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/app.css') }}">


    {{-- Web Fonts --}}
    {{--<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">--}}
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,400,500,700' rel='stylesheet' type='text/css'>

    {{--JQuery--}}
    <script src="{{ asset('/js/lib/jquery/jquery-2.1.3.min.js') }}"></script>
    <script src="{{ asset('/js/lib/jquery/jquery.ui.widget.js') }}"></script>


    {{-- AngularJS --}}
    <script src="{{ asset('/js/lib/angular/angular.min.js') }}"></script>
    <script src="{{ asset('/js/lib/angular/angular-touch.min.js') }}"></script>
    <script src="{{ asset('/js/lib/angular/angular-route.min.js') }}"></script>
    <script src="{{ asset('/js/lib/angular/angular-resource.min.js') }}"></script>
    <script src="{{ asset('/js/lib/angular/angular-sanitize.min.js') }}"></script>
    <script src="{{ asset('/js/lib/angular/angular-animate.min.js') }}"></script>


    {{-- Bootstrap--}}
    <script src="{{ asset('/js/lib/bootstrap/bootstrap.min.js') }}"></script>

    {{-- Moment JS Date Library--}}
    <script src="{{ asset('/js/lib/moment/moment-with-locales.min.js') }}"></script>

    {{-- Angular Modal Service https://github.com/dwmkerr/angular-modal-service --}}
    <script src="{{ asset('/js/lib/angularmodalservice/angular-modal-service.js') }}"></script>

    {{-- AngularStrap --}}
    <script src="{{ asset('/js/lib/angularstrap/angular-strap.js') }}"></script>
    <script src="{{ asset('/js/lib/angularstrap/angular-strap.tpl.js') }}"></script>

    {{-- AngularToastr --}}
    <script src="{{ asset('/js/lib/angulartoastr/angular-toastr.tpls.js') }}"></script>

    {{-- Angular 720kb Tooltip --}}
    <script src="{{ asset('/js/lib/angulartooltips/angular-tooltips.js') }}"></script>

    {{-- Loading Bar https://github.com/chieffancypants/angular-loading-bar--}}
    <script src="{{ asset('/js/lib/loadingbar/loading-bar.min.js') }}"></script>

    {{-- Angular File Upload https://github.com/danialfarid/angular-file-upload--}}
    <script src="{{ asset('/js/lib/angularfileupload/angular-file-upload.js') }}"></script>

    {{-- Angular Local Storage https://github.com/grevory/angular-local-storage --}}
    <script src="{{ asset('/js/lib/angularlocalstorage/angular-local-storage.min.js') }}"></script>

    {{-- textAngular https://github.com/fraywing/textAngular --}}
    <script src="{{ asset('/js/lib/textangular/textAngular.min.js') }}"></script>
    <script src="{{ asset('/js/lib/textangular/textAngular-rangy.min.js') }}"></script>
    <script src="{{ asset('/js/lib/textangular/textAngular-sanitize.min.js') }}"></script>

    {{-- ngJsTree https://github.com/ezraroi/ngJsTree --}}
    <script src="{{ asset('/js/lib/ngjstree/jstree.js') }}"></script>
    <script src="{{ asset('/js/lib/ngjstree/ngJsTree.js') }}"></script>

    {{-- AngularJS --}}
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{ asset('/js/shared/auth.ctrl.js') }}"></script>
    <script src="{{ asset('/js/shared/navbar.ctrl.js') }}"></script>
    <script src="{{ asset('/js/shared/footer.ctrl.js') }}"></script>
    <script src="{{ asset('/js/shared/auth.service.js') }}"></script>
    <script src="{{ asset('/js/shared/dataprovider.service.js') }}"></script>
    <script src="{{ asset('/js/shared/http.service.js') }}"></script>
    <script src="{{ asset('/js/shared/modal.ctrl.js') }}"></script>
    <script src="{{ asset('/js/shared/formvalidation.directive.js') }}"></script>
    <script src="{{ asset('/js/shared/ui-grid.directive.js') }}"></script>
    <script src="{{ asset('/js/shared/ui-tabs.directive.js') }}"></script>
    <script src="{{ asset('/js/shared/modal.massemailrecipient.ctrl.js') }}"></script>

    {{-- *** WEBSITE *** --}}
    <script src="{{ asset('/js/components/website/home.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/website/contact.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/website/useraccount/useraccount.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/website/useraccount/useraccount_pwd.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/website/useraccount/useraccount_portrait.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/website/useraccount/useraccount.service.js') }}"></script>
    <script src="{{ asset('/js/components/website/useraccount/useraccount.directive.js') }}"></script>

    {{-- *** ADMIN *** --}}
    <script src="{{ asset('/js/components/admin/admin.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/home.ctrl.js') }}"></script>

    <script src="{{ asset('/js/components/admin/users/user.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/users/user.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/users/user.directive.js') }}"></script>

    <script src="{{ asset('/js/components/admin/templates/templates.service.js') }}"></script>

    <script src="{{ asset('/js/components/admin/emails/email.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/emails/email.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/emails/email.directive.js') }}"></script>

    <script src="{{ asset('/js/components/admin/massemail/massemail.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/massemail/massemail.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/massemail/massemail.directive.js') }}"></script>

    <script src="{{ asset('/js/components/admin/filemanager/filemanager.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/filemanager/filemanager.service.js') }}"></script>

    <script src="{{ asset('/js/components/admin/members/member.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/members/member.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/members/member.directive.js') }}"></script>

    <script src="{{ asset('/js/components/admin/emailtemplates/emailtemplates.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/emailtemplates/emailtemplates.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/emailtemplates/emailtemplates.directive.js') }}"></script>

    <script src="{{ asset('/js/components/admin/articles/article.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/articles/article.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/articles/article.directive.js') }}"></script>

    <script src="{{ asset('/js/components/admin/appointments/appointment.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/appointments/appointment.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/appointments/appointment.directive.js') }}"></script>
    
    <script src="{{ asset('/js/components/admin/configmobileapp/configmobileapp.ctrl.js') }}"></script>
    <script src="{{ asset('/js/components/admin/configmobileapp/configmobileapp.service.js') }}"></script>
    <script src="{{ asset('/js/components/admin/configmobileapp/configmobileapp.directive.js') }}"></script>

    <script src="{{ asset('/js/filters.js') }}"></script>

    {{-- Angular UI-Grid --}}
    <script src="{{ asset('/js/lib/uigrid/ui-grid-unstable.js') }}"></script>

    {{-- Angular UI-Select https://github.com/angular-ui/ui-select--}}
    <script src="{{ asset('/js/lib/uiselect/select.js') }}"></script>

    <script>
        angular.module("theApp").constant("APP_VERSION", '{{ Config::get('app.version') }}');
        angular.module("theApp").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
        angular.module("theApp").constant("URL_HOME", '<?php echo route('home');  ?>');
        angular.module("theApp").constant("DIR_IMG_USER", '{{ Config::get('app.dir_image_user') }}');
        angular.module("theApp").constant("DIR_IMG_MEMBER", '{{ Config::get('app.dir_image_member') }}');
    </script>


</head>

<body>

<div class="container" ng-view></div>

</body>
</html>