'use strict';

angular.module('theApp', ['mgcrea.ngStrap', 'angularModalService', 'LocalStorageModule', 'toastr', 'angularFileUpload', '720kb.tooltips', 'textAngular', 'ngJsTree', 'ui.select', 'ui.grid', 'ui.grid.pagination', 'ui.grid.resizeColumns', 'ui.grid.pinning', 'ui.grid.moveColumns', 'ui.grid.selection', 'ui.grid.edit', 'angular-loading-bar', 'ngRoute', 'ngResource', 'ngSanitize', 'ngAnimate'])

   .config(configRoute)
   .config(configInterceptor)
   .config(configDatepicker)
   .config(configToastr)

   .run(function ($http, CSRF_TOKEN) {
      $http.defaults.headers.common['csrf_token'] = CSRF_TOKEN;
   })

   // Route
   .run([
      'URL_HOME', '$rootScope', '$location', '$window', 'AuthService', 'toastr', 'localStorageService',
      function (URL_HOME, $rootScope, $location, $window, AuthService, toastr, localStorageService) {

         $rootScope.$on('$routeChangeStart', function (event, next, current) {

            console.log('Next Route: %s', next.originalPath);


            // Cache cleared and still logged in?
            if (!localStorageService.get("isLoggedin") && !localStorageService.get("hasLoggedout"))
            {
               AuthService.logoff()
                  .success(function (data) {

                     localStorageService.clearAll();
                     //$location.path('/auth/login');
                  })
                  .error(function (data) {
                  });
            }

            if (!$rootScope.UserPortrait)
            {
               if (localStorageService.get("isLoggedin") == 'y')
               {
                  $rootScope.UserPortrait = {imageSrc: localStorageService.get("userPortrait")};
               }
            }

         });
      }
   ]);

configInterceptor.$inject = ['$httpProvider'];


function configInterceptor($httpProvider) {

   $httpProvider.interceptors.push('httpInterceptor');
}


function configToastr(toastrConfig) {
   angular.extend(toastrConfig, {
      allowHtml: true,
      autoDismiss: false,
      closeButton: true,
      closeHtml: '<button>&times;</button>',
      containerId: 'toast-container',
      extendedTimeOut: 1000,
      iconClasses: {
         error: 'toast-error',
         info: 'toast-info',
         success: 'toast-success',
         warning: 'toast-warning'
      },
      maxOpened: 0,
      messageClass: 'toast-message',
      newestOnTop: true,
      onHidden: null,
      onShown: null,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
      progressBar: true,
      tapToDismiss: true,
      target: 'body',
      templates: {
         toast: 'directives/toast/toast.html',
         progressbar: 'directives/progressbar/progressbar.html'
      },
      timeOut: 5000,
      titleClass: 'toast-title',
      toastClass: 'toast'
   });
}


function configDatepicker($datepickerProvider) {
   angular.extend($datepickerProvider.defaults, {
      dateFormat: 'dd.MM.yyyy',
      timeFormat: 'HH.mm',
      autoclose: true,
      startWeek: 1
   });
}


function configRoute($routeProvider) {

   $routeProvider.

      // -- Website ---
      when('/', {
         controller:  'WebsiteHomeController',
         templateUrl: 'views/website/home.html'
      }).
      when('/website', {
         controller:  'WebsiteHomeController',
         templateUrl: 'views/website/home.html'
      }).
      when('/contact', {
         controller:  'WebsiteContactController',
         templateUrl: 'views/website/contact.html'
      }).

      // -- Logged in ---
      when('/useraccount', {
         controller:   'WebsiteUserAccountController',
         templateUrl:  'views/website/useraccount/useraccount.html',
         controllerAs: 'accountCtrl'
      }).
      when('/useraccount_portrait', {
         controller:   'WebsiteUserAccountPortraitController',
         templateUrl:  'views/website/useraccount/useraccount_portrait.html',
         controllerAs: 'accountCtrl'
      }).
      when('/useraccount_pwd', {
         controller:   'WebsiteUserAccountPwdController',
         templateUrl:  'views/website/useraccount/useraccount_pwd.html',
         controllerAs: 'accountCtrl'
      }).

      // -- Auth --
      when('/auth/login', {
         controller:   'WebsiteLoginController',
         templateUrl:  'views/website/auth/login.html',
         controllerAs: 'loginCtrl'
      }).
      when('/auth/register', {
         controller:   'WebsiteRegisterController',
         templateUrl:  'views/website/auth/register.html',
         controllerAs: 'registerCtrl'
      }).
      when('/auth/activateaccount/:activationcode', {
         controller:   'WebsiteActivateAccountController',
         templateUrl:  'views/website/auth/activateaccount.html',
         controllerAs: 'activateAccountCtrl'
      }).
      when('/auth/activateaccount', {
         controller:   'WebsiteActivateAccountController',
         templateUrl:  'views/website/auth/activateaccount.html',
         controllerAs: 'activateAccountCtrl'
      }).
      when('/auth/requestpwd', {
         controller:   'WebsiteRequestPwdController',
         templateUrl:  'views/website/auth/requestpwd.html',
         controllerAs: 'reqPwdCtrl'
      }).
      when('/auth/resetpwd/:resetcode', {
         controller:   'WebsiteResetPwdController',
         templateUrl:  'views/website/auth/resetpwd.html',
         controllerAs: 'resetPwdCtrl'
      }).
      when('/auth/resetpwd', {
         controller:   'WebsiteResetPwdController',
         templateUrl:  'views/website/auth/resetpwd.html',
         controllerAs: 'resetPwdCtrl'
      }).

      // -- Administration --
      when('/admin', {
         controller:   'AdminHomeController',
         templateUrl:  'views/admin/home.html',
         controllerAs: 'adminCtrl'
      }).

      when('/admin/members', {
         controller:   'AdminMembersController',
         templateUrl:  'views/admin/members/list.html',
         controllerAs: 'adminMemberCtrl'
      }).
      when('/admin/member/edit/:memberId', {
         controller:   'AdminMembersEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/member/edit', {
         controller:   'AdminMembersEditController',
         templateUrl:  'views/admin/members/edit.html',
         controllerAs: 'adminMemberEditCtrl'
      }).

      when('/admin/articles', {
         controller:   'AdminArticleController',
         templateUrl:  'views/admin/articles/list.html',
         controllerAs: 'adminArticleCtrl'
      }).
      when('/admin/article/edit/:articleId', {
         controller:   'AdminArticleEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/article/edit', {
         controller:   'AdminArticleEditController',
         templateUrl:  'views/admin/articles/edit.html',
         controllerAs: 'adminArticleEditCtrl'
      }).

      when('/admin/appointments', {
         controller:   'AdminAppointmentController',
         templateUrl:  'views/admin/appointments/list.html',
         controllerAs: 'adminAppointmentCtrl'
      }).
      when('/admin/appointment/edit/:appointmentId', {
         controller:   'AdminAppointmentEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/appointment/edit', {
         controller:   'AdminAppointmentEditController',
         templateUrl:  'views/admin/appointments/edit.html',
         controllerAs: 'adminAppointmentEditCtrl'
      }).
      
      when('/admin/configmobileapp', {
         controller:   'AdminConfigmobileappController',
         templateUrl:  'views/admin/configmobileapp/list.html',
         controllerAs: 'adminConfigmobileappCtrl'
      }).
      when('/admin/configmobileapp/edit/:configId', {
         controller:   'AdminConfigmobileappEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/configmobileapp/edit', {
         controller:   'AdminConfigmobileappEditController',
         templateUrl:  'views/admin/configmobileapp/edit.html',
         controllerAs: 'adminConfigmobileappEditCtrl'
      }).

      when('/admin/users', {
         controller:   'AdminUsersController',
         templateUrl:  'views/admin/users/list.html',
         controllerAs: 'adminUserCtrl'
      }).
      when('/admin/user/edit/:userId', {
         controller:   'AdminUsersEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/user/edit', {
         controller:   'AdminUsersEditController',
         templateUrl:  'views/admin/users/edit.html',
         controllerAs: 'adminUserEditCtrl'
      }).

      when('/admin/massemails', {
         controller:   'AdminMassEmailController',
         templateUrl:  'views/admin/massemail/list.html',
         controllerAs: 'adminMassEmailCtrl'
      }).
      when('/admin/massemail/edit/:massemailId', {
         controller:   'AdminMassEmailEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/massemail/edit', {
         controller:   'AdminMassEmailEditController',
         templateUrl:  'views/admin/massemail/edit.html',
         controllerAs: 'adminMassEmailEditCtrl'
      }).
      when('/admin/massemail_log', {
         controller:   'AdminMassEmailLogController',
         templateUrl:  'views/admin/massemail/log.html',
         controllerAs: 'adminMassEmailLogCtrl'
      }).
      when('/admin/massemail_send', {
         controller:   'AdminMassEmailSendController',
         templateUrl:  'views/admin/massemail/send.html',
         controllerAs: 'adminMassEmailSendCtrl'
      }).

      when('/admin/filemanager', {
         controller:   'AdminFilemanagerController',
         templateUrl:  'views/admin/filemanager/edit.html',
         controllerAs: 'adminFilemanagerCtrl'
      }).
      
      when('/admin/emails', {
         controller:   'AdminEmailController',
         templateUrl:  'views/admin/emails/list.html',
         controllerAs: 'adminEmailCtrl'
      }).
      when('/admin/email/edit', {
         controller:   'AdminEmailEditController',
         templateUrl:  'views/admin/emails/edit.html',
         controllerAs: 'adminEmailEditCtrl'
      }).
      
      when('/admin/emailtemplates', {
         controller:   'AdminEmailTemplatesController',
         templateUrl:  'views/admin/emailtemplates/list.html',
         controllerAs: 'adminEmailTemplateCtrl'
      }).
      when('/admin/emailtemplate/edit/:templateId', {
         controller:   'AdminEmailTemplatesEditByIdController',
         templateUrl:  'views/blank.html'
      }).
      when('/admin/emailtemplate/edit', {
         controller:   'AdminEmailTemplatesEditController',
         templateUrl:  'views/admin/emailtemplates/edit.html',
         controllerAs: 'adminEmailTemplateEditCtrl'
      }).
      otherwise({
         redirectTo: '/'
      })

}







