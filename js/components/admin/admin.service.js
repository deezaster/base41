'use strict';

angular
   .module('theApp')
   .factory('AdminService', AdminService);


AdminService.$inject = ['$http'];


function AdminService($http) {

   return {

      init: function () {
         return $http({method: 'GET', url: 'api/v1/admin/init'});
      },

      getUserEvents: function () {
         return $http({method: 'GET', url: 'api/v1/admin/userevents'});
      },

      getEmails: function () {
         return $http({method: 'GET', url: 'api/v1/admin/emails'});
      }
   };
}
