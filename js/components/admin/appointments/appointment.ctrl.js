'use strict';

angular
   .module('theApp')
   .controller('AdminAppointmentController', AdminAppointmentController)
   .controller('AdminAppointmentEditByIdController', AdminAppointmentEditByIdController)
   .controller('AdminAppointmentEditController', AdminAppointmentEditController)

AdminAppointmentController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminAppointmentService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminAppointmentEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminAppointmentService'];
AdminAppointmentEditController.$inject = ['$location', 'AdminAppointmentService', 'DataProviderService', 'toastr', '$sanitize', 'ModalService'];

function AdminAppointmentEditByIdController($location, DataProviderService, $routeParams, AdminAppointmentService) {

   var id = $routeParams.appointmentId;

   AdminAppointmentService.getAppointment(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("appointment", data);
         $location.path('/admin/appointment/edit');
      })
      .error(function (data) {
      });
}


function AdminAppointmentController(uiGridConstants, $location, $scope, AdminAppointmentService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            minWidth:         160,
            width:            160,
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'id', displayName: 'ID', minWidth: 50, width: 60, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'title', displayName: 'Titel', minWidth: 200, width: 300, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'startdate', displayName: 'Startdatum', minWidth: 100, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'starttime', displayName: 'Startzeit', minWidth: 100, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'enddate', displayName: 'Endedatum', minWidth: 100, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'endtime', displayName: 'Endezeit', minWidth: 100, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'updated_at', displayName: 'Version', minWidth: 100, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         //gridApi.core.on.filterChanged($scope, function () {
         //   var grid = this.grid;
         //   angular.forEach(grid.columns, function (value, key) {
         //      if (value.filters[0])
         //      {
         //         console.log('FILTER TERM FOR ' + value.colDef.name + ' = ' + value.filters[0].term);
         //      }
         //   });
         //});
      },
      appScopeProvider:    {


         // Editieren
         onClick: function (row) {
            $location.path('/admin/appointment/edit/' + row.entity.id);
         },

         onDelete:  function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "Termin löschen?",
                  msg1:  "Soll dieser Termin wirklich gelöscht werden?",
                  msg2:  row.entity.startdate + ": " + row.entity.title
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminAppointmentService.deleteAppointment(row.entity.id)
                        .success(function (data) {
                           DataProviderService.clear();

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target: data["response"]["container"],
                              timeOut: parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminAppointmentService.getAppointments()
                              .success(function (data) {

                                 $scope.gridOptions.data = data;
                              });
                        });

                  }

               });
            });


         }

      }
   };

   $scope.gridOptions.enableRowSelection = false;
   $scope.gridOptions.enableSelectAll = false;
   $scope.gridOptions.multiSelect = false;
   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.useExternalFiltering = false;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');


   AdminAppointmentService.getAppointments()
      .success(function (data) {

         $scope.gridOptions.data = data;
      });


   vm.newAppointment = function () {
      DataProviderService.clear();
      $location.path('/admin/appointment/edit');
   }

}

function AdminAppointmentEditController($location, AdminAppointmentService, DataProviderService, toastr, $sanitize, ModalService) {

   var vm = this;

   var row = DataProviderService.get("appointment");

   moment.locale('en');

   if (row)
   {
      // Mutation
      vm.id = row.id;
      vm.title = row.title;
      vm.description = row.description;
      vm.startdate = row.startdate;
      vm.starttime = row.starttime;
      vm.enddate = row.enddate;
      vm.endtime = row.endtime;

      var enddateMoment = moment(vm.enddate);
      vm.enddateForm = enddateMoment.valueOf();

      vm.created_at = row.created_at;
      vm.updated_at = row.updated_at;
      vm.deleted_at = row.deleted_at;
      vm.created_by_user = row.created_by_user;
      vm.updated_by_user = row.updated_by_user;
      vm.deleted_by_user = row.deleted_by_user;

   }
   else
   {
      // Neuerfassung
      vm.id = -1;

      vm.enddateForm = "";

   }

   vm.createData = function() {
      return vm.id == -1;
   };

   vm.editData = function() {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   var startdateMoment = moment(vm.startdate);
   vm.startdateForm = startdateMoment.valueOf();

   var starttimeMoment = moment(vm.startdate + "T" + vm.starttime);
   vm.starttimeForm = starttimeMoment.valueOf();

   var endtimeMoment = moment(vm.enddate + "T" + vm.endtime);
   vm.endtimeForm = endtimeMoment.valueOf();



   vm.submitForm = function (isValid) {

      var startdateMoment = moment(vm.startdateForm)
      vm.startdate = startdateMoment.format("YYYY-MM-DD");

      if (vm.starttimeForm)
      {
         var starttimeMoment = moment(vm.starttimeForm)
         vm.starttime = starttimeMoment.format("HH:mm");
      }
      else {
         vm.starttime = null
      }

      if (vm.enddateForm)
      {
         var enddateMoment = moment(vm.enddateForm)
         vm.enddate = enddateMoment.format("YYYY-MM-DD");
      }
      else {
         vm.enddate = null
      }

      if (vm.endtimeForm)
      {
         var endtimeMoment = moment(vm.endtimeForm)
         vm.endtime = endtimeMoment.format("HH:mm");
      }
      else {
         vm.endtime = null
      }

      if (isValid)
      {
         if (vm.id != -1)
         {
            console.log(vm.publishdate);

            AdminAppointmentService.saveAppointment(
               $sanitize(vm.id),
               $sanitize(vm.title),
               $sanitize(vm.description),
               $sanitize(vm.startdate),
               $sanitize(vm.starttime),
               $sanitize(vm.enddate),
               $sanitize(vm.endtime))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  $location.path('/admin/appointments');
               });
         }
         else
         {
            AdminAppointmentService.insertAppointment(
               $sanitize(vm.id),
               $sanitize(vm.title),
               $sanitize(vm.description),
               $sanitize(vm.startdate),
               $sanitize(vm.starttime),
               $sanitize(vm.enddate),
               $sanitize(vm.endtime))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  $location.path('/admin/appointments');
               });
         }


      }

   };


   vm.cancel = function () {
      $location.path('/admin/appointments');
   };


   vm.delete = function () {

      // Sicherheitsabfrage
      ModalService.showModal({
         templateUrl: "views/shared/modal_yesno.html",
         controller:  "ModalController",
         inputs:      {
            title: "Termin löschen?",
            msg1:  "Soll dieser Termin wirklich gelöscht werden?",
            msg2:  vm.startdate + ": " + vm.title
         }
      }).then(function (modal) {
         modal.element.modal();
         modal.close.then(function (result) {
            if (result)
            {
               AdminAppointmentService.deleteAppointment(vm.id)
                  .success(function (data) {
                     DataProviderService.clear();

                     toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                        target: data["response"]["container"],
                        timeOut: parseInt(data["response"]["duration"]),
                        positionClass: data["response"]["placement"]
                     });

                     $location.path('/admin/appointments');

                  });

            }

         });
      });

   };

}








