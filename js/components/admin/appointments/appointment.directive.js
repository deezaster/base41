'use strict';

angular
   .module('theApp')
   .directive('x3mAppointmentTitleNavbar', x3mAppointmentTitleNavbar)
   .directive('x3mAppointmentTitlePanel', x3mAppointmentTitlePanel)
   .directive('x3mAppointmentInfoPanel', x3mAppointmentInfoPanel)


function x3mAppointmentTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminAppointmentEditCtrl.id != -1)
         {
            this.display = $scope.adminAppointmentEditCtrl.title;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mAppointmentTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminAppointmentEditCtrl.id != -1)
         {
            this.display1 = $scope.adminAppointmentEditCtrl.title;
         }
         else
         {
            this.display1 = "NEUER TERMIN ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mAppointmentInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";

         this.show2 = false;
         this.show3 = false;

         this.show1 = true;
         this.display1 = $scope.adminAppointmentEditCtrl.created_at + " " + $scope.adminAppointmentEditCtrl.created_by_user.first_name + " " + $scope.adminAppointmentEditCtrl.created_by_user.last_name + " (" + $scope.adminAppointmentEditCtrl.created_by_user.username + ")";

         if ($scope.adminAppointmentEditCtrl.updated_at != $scope.adminAppointmentEditCtrl.created_at)
         {
            this.show2 = true;
            if ($scope.adminAppointmentEditCtrl.updated_by_user)
            {
               this.display2 = $scope.adminAppointmentEditCtrl.updated_at + " " + $scope.adminAppointmentEditCtrl.updated_by_user.first_name + " " + $scope.adminAppointmentEditCtrl.updated_by_user.last_name + " (" + $scope.adminAppointmentEditCtrl.updated_by_user.username + ")";
            }
            else
            {
               this.display2 = $scope.adminAppointmentEditCtrl.updated_at;
            }
         }

         if ($scope.adminAppointmentEditCtrl.deleted_at)
         {
            this.show3 = true;
            this.show2 = false;
            if ($scope.adminAppointmentEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminAppointmentEditCtrl.deleted_at + " " + $scope.adminAppointmentEditCtrl.deleted_by_user.first_name + " " + $scope.adminAppointmentEditCtrl.deleted_by_user.last_name + " (" + $scope.adminAppointmentEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminAppointmentEditCtrl.deleted_at;
            }
         }

      }
   };
   return directive;
}



