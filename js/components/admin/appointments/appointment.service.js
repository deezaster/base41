'use strict';

angular
   .module('theApp')
   .factory('AdminAppointmentService', AdminAppointmentService);


AdminAppointmentService.$inject = ['$http'];


function AdminAppointmentService($http) {

   return {

      getAppointments: function () {
         return $http({method: 'GET', url: 'api/v1/admin/appointment'});
      },

      getAppointment: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/appointment/edit', data: {'id': id}});
      },

      saveAppointment: function (id, title, description, startdate, starttime, enddate, endtime) {
         return $http({
            method: 'POST',
            url:    'api/v1/admin/appointment/save',
            data:   {'id': id, 'title': title,  'description': description, 'startdate': startdate, 'starttime': starttime, 'enddate': enddate, 'endtime': endtime}
         });
      },

      insertAppointment: function (id, title, description, startdate, starttime, enddate, endtime) {
         return $http({method: 'POST', url: 'api/v1/admin/appointment/insert', data: {'id': id, 'title': title,  'description': description, 'startdate': startdate, 'starttime': starttime, 'enddate': enddate, 'endtime': endtime}});
      },

      deleteAppointment: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/appointment/delete', data: {'id': id}});
      }


   };
}
