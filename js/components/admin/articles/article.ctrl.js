'use strict';

angular
   .module('theApp')
   .controller('AdminArticleController', AdminArticleController)
   .controller('AdminArticleEditByIdController', AdminArticleEditByIdController)
   .controller('AdminArticleEditController', AdminArticleEditController)

AdminArticleController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminArticleService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminArticleEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminArticleService'];
AdminArticleEditController.$inject = ['$location', 'AdminArticleService', 'DataProviderService', 'toastr', '$sanitize', 'ModalService'];

function AdminArticleEditByIdController($location, DataProviderService, $routeParams, AdminArticleService) {

   var id = $routeParams.articleId;

   AdminArticleService.getArticle(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("article", data);
         $location.path('/admin/article/edit');
      })
      .error(function (data) {
      });
}


function AdminArticleController(uiGridConstants, $location, $scope, AdminArticleService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            minWidth:         160,
            width:            160,
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'id', displayName: 'ID', minWidth: 50, width: 60, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'title', displayName: 'Titel', minWidth: 200, width: 200, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'articledate', displayName: 'Datum', minWidth: 200, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'publishdate', displayName: 'Publizieren am', minWidth: 200, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'updated_at', displayName: 'Version', minWidth: 100, width: 140, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         //gridApi.core.on.filterChanged($scope, function () {
         //   var grid = this.grid;
         //   angular.forEach(grid.columns, function (value, key) {
         //      if (value.filters[0])
         //      {
         //         console.log('FILTER TERM FOR ' + value.colDef.name + ' = ' + value.filters[0].term);
         //      }
         //   });
         //});
      },
      appScopeProvider:    {


         // Editieren
         onClick: function (row) {
            $location.path('/admin/article/edit/' + row.entity.id);
         },

         onDelete:  function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "Artikel löschen?",
                  msg1:  "Soll der folgende Artikel wirklich gelöscht werden?",
                  msg2:  row.entity.title
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminArticleService.deleteArticle(row.entity.id)
                        .success(function (data) {
                           DataProviderService.clear();

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target: data["response"]["container"],
                              timeOut: parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminArticleService.getArticles()
                              .success(function (data) {

                                 $scope.gridOptions.data = data;
                              });
                        });

                  }

               });
            });


         }

      }
   };

   $scope.gridOptions.enableRowSelection = false;
   $scope.gridOptions.enableSelectAll = false;
   $scope.gridOptions.multiSelect = false;
   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.useExternalFiltering = false;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');


   AdminArticleService.getArticles()
      .success(function (data) {

         $scope.gridOptions.data = data;
      });


   vm.newArticle = function () {
      DataProviderService.clear();
      $location.path('/admin/article/edit');
   }

}

function AdminArticleEditController($location, AdminArticleService, DataProviderService, toastr, $sanitize, ModalService) {

   var vm = this;

   var row = DataProviderService.get("article");

   moment.locale('en');

   if (row)
   {
      // Mutation
      vm.id = row.id;
      vm.title = row.title;
      vm.articledate = row.articledate;
      vm.publishdate = row.publishdate;

      var publishdateMoment = moment(vm.publishdate);
      vm.publishdateForm = publishdateMoment.valueOf();

      vm.created_at = row.created_at;
      vm.updated_at = row.updated_at;
      vm.deleted_at = row.deleted_at;
      vm.created_by_user = row.created_by_user;
      vm.updated_by_user = row.updated_by_user;
      vm.deleted_by_user = row.deleted_by_user;

      vm.orightml = row.content;
      vm.htmlcontent = vm.orightml;
      vm.disabled = false;
   }
   else
   {
      // Neuerfassung
      vm.id = -1;

      vm.orightml = "";
      vm.htmlcontent = vm.orightml;
      vm.disabled = false;
      vm.publishdateForm = "";
   }

   vm.createData = function() {
      return vm.id == -1;
   };

   vm.editData = function() {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   var articledateMoment = moment(vm.articledate);
   vm.articledateForm = articledateMoment.valueOf();



   vm.submitForm = function (isValid) {

      var articledateMoment = moment(vm.articledateForm)
      vm.articledate = articledateMoment.format("YYYY-MM-DD");

      if (vm.publishdateForm)
      {
         var publishdateMoment = moment(vm.publishdateForm)
         vm.publishdate = publishdateMoment.format("YYYY-MM-DD");
      }
      else {
         vm.publishdate = null
      }

      if (isValid)
      {
         if (vm.id != -1)
         {
            AdminArticleService.saveArticle(
               $sanitize(vm.id),
               $sanitize(vm.title),
               $sanitize(vm.articledate),
               $sanitize(vm.publishdate),
               vm.htmlcontent)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  $location.path('/admin/articles');
               });
         }
         else
         {
            AdminArticleService.insertArticle(
               $sanitize(vm.id),
               $sanitize(vm.title),
               $sanitize(vm.articledate),
               $sanitize(vm.publishdate),
               vm.htmlcontent)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  $location.path('/admin/articles');
               });
         }


      }

   };


   vm.cancel = function () {
      $location.path('/admin/articles');
   };


   vm.delete = function () {

      // Sicherheitsabfrage
      ModalService.showModal({
         templateUrl: "views/shared/modal_yesno.html",
         controller:  "ModalController",
         inputs:      {
            title: "Artikel löschen?",
            msg1:  "Soll dieser Artikel wirklich gelöscht werden?",
            msg2:  vm.subject
         }
      }).then(function (modal) {
         modal.element.modal();
         modal.close.then(function (result) {
            if (result)
            {
               AdminArticleService.deleteArticle(vm.id)
                  .success(function (data) {
                     DataProviderService.clear();

                     toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                        target: data["response"]["container"],
                        timeOut: parseInt(data["response"]["duration"]),
                        positionClass: data["response"]["placement"]
                     });

                     $location.path('/admin/articles');

                  });

            }

         });
      });

   };

}








