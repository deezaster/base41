'use strict';

angular
   .module('theApp')
   .directive('x3mArticleTitleNavbar', x3mArticleTitleNavbar)
   .directive('x3mArticleTitlePanel', x3mArticleTitlePanel)
   .directive('x3mArticleInfoPanel', x3mArticleInfoPanel)


function x3mArticleTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminArticleEditCtrl.id != -1)
         {
            this.display = $scope.adminArticleEditCtrl.title;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mArticleTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminArticleEditCtrl.id != -1)
         {
            this.display1 = $scope.adminArticleEditCtrl.title;
         }
         else
         {
            this.display1 = "NEUER ARTIKEL ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mArticleInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";

         this.show2 = false;
         this.show3 = false;

         this.show1 = true;
         this.display1 = $scope.adminArticleEditCtrl.created_at + " " + $scope.adminArticleEditCtrl.created_by_user.first_name + " " + $scope.adminArticleEditCtrl.created_by_user.last_name + " (" + $scope.adminArticleEditCtrl.created_by_user.username + ")";

         if ($scope.adminArticleEditCtrl.updated_at != $scope.adminArticleEditCtrl.created_at)
         {
            this.show2 = true;
            if ($scope.adminArticleEditCtrl.updated_by_user)
            {
               this.display2 = $scope.adminArticleEditCtrl.updated_at + " " + $scope.adminArticleEditCtrl.updated_by_user.first_name + " " + $scope.adminArticleEditCtrl.updated_by_user.last_name + " (" + $scope.adminArticleEditCtrl.updated_by_user.username + ")";
            }
            else
            {
               this.display2 = $scope.adminArticleEditCtrl.updated_at;
            }
         }

         if ($scope.adminArticleEditCtrl.deleted_at)
         {
            this.show3 = true;
            this.show2 = false;
            if ($scope.adminArticleEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminArticleEditCtrl.deleted_at + " " + $scope.adminArticleEditCtrl.deleted_by_user.first_name + " " + $scope.adminArticleEditCtrl.deleted_by_user.last_name + " (" + $scope.adminArticleEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminArticleEditCtrl.deleted_at;
            }
         }

      }
   };
   return directive;
}



