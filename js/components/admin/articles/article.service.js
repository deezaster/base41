'use strict';

angular
   .module('theApp')
   .factory('AdminArticleService', AdminArticleService);


AdminArticleService.$inject = ['$http'];


function AdminArticleService($http) {

   return {

      getArticles: function () {
         return $http({method: 'GET', url: 'api/v1/admin/article'});
      },


      getArticle: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/article/edit', data: {'id': id}});
      },

      saveArticle: function (id, title, articledate, publishdate, content) {
         return $http({
            method: 'POST',
            url:    'api/v1/admin/article/save',
            data:   {'id': id, 'title': title,  'content': content, 'articledate': articledate, 'publishdate': publishdate}
         });
      },

      insertArticle: function (id, title, articledate, publishdate, content) {
         return $http({method: 'POST', url: 'api/v1/admin/article/insert', data: {'id': id, 'title': title,  'content': content, 'articledate': articledate, 'publishdate': publishdate}});
      },

      deleteArticle: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/article/delete', data: {'id': id}});
      }


   };
}
