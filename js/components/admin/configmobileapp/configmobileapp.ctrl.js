'use strict';

angular
   .module('theApp')
   .controller('AdminConfigmobileappController', AdminConfigmobileappController)
   .controller('AdminConfigmobileappEditByIdController', AdminConfigmobileappEditByIdController)
   .controller('AdminConfigmobileappEditController', AdminConfigmobileappEditController)

AdminConfigmobileappController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminConfigmobileappService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminConfigmobileappEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminConfigmobileappService'];
AdminConfigmobileappEditController.$inject = ['$location', 'AdminConfigmobileappService', 'DataProviderService', 'toastr', '$sanitize', 'ModalService'];

function AdminConfigmobileappEditByIdController($location, DataProviderService, $routeParams, AdminConfigmobileappService) {

   var id = $routeParams.configId;

   AdminConfigmobileappService.getConfigmobileapp(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("configmobileapp", data);
         $location.path('/admin/configmobileapp/edit');
      })
      .error(function (data) {
      });
}


function AdminConfigmobileappController(uiGridConstants, $location, $scope, AdminConfigmobileappService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            minWidth:         160,
            width:            160,
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'id', displayName: 'ID', minWidth: 50, width: 60, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'key', displayName: 'Schlüssel', minWidth: 200, width: 200, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'value', displayName: 'Wert', minWidth: 200, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'description', displayName: 'Beschreibung', minWidth: 200, width: 400, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'updated_at', displayName: 'Version', minWidth: 100, width: 140, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         //gridApi.core.on.filterChanged($scope, function () {
         //   var grid = this.grid;
         //   angular.forEach(grid.columns, function (value, key) {
         //      if (value.filters[0])
         //      {
         //         console.log('FILTER TERM FOR ' + value.colDef.name + ' = ' + value.filters[0].term);
         //      }
         //   });
         //});
      },
      appScopeProvider:    {


         // Editieren
         onClick: function (row) {
            $location.path('/admin/configmobileapp/edit/' + row.entity.id);
         }
      }
   };

   $scope.gridOptions.enableRowSelection = false;
   $scope.gridOptions.enableSelectAll = false;
   $scope.gridOptions.multiSelect = false;
   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.useExternalFiltering = false;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');


   AdminConfigmobileappService.getConfigmobileapps()
      .success(function (data) {

         $scope.gridOptions.data = data;
      });


}

function AdminConfigmobileappEditController($location, AdminConfigmobileappService, DataProviderService, toastr, $sanitize, ModalService) {

   var vm = this;

   var row = DataProviderService.get("configmobileapp");

   moment.locale('en');

   if (row)
   {
      // Mutation
      vm.id = row.id;
      vm.keyform = row.key;
      vm.valueform = row.value;
      vm.description = row.description;

      vm.created_at = row.created_at;
      vm.updated_at = row.updated_at;
      vm.deleted_at = row.deleted_at;
      vm.created_by_user = row.created_by_user;
      vm.updated_by_user = row.updated_by_user;
      vm.deleted_by_user = row.deleted_by_user;
   }
   else
   {
      // Neuerfassung (zur Zeit nicht erlaubt!
      $location.path('/admin/configmobileapp');
   }

   vm.createData = function() {
      return vm.id == -1;
   };

   vm.editData = function() {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   
   vm.submitForm = function (isValid) {

      if (isValid)
      {
         if (vm.id != -1)
         {
            AdminConfigmobileappService.saveConfigmobileapp(
               $sanitize(vm.id),
               $sanitize(vm.keyform),
               $sanitize(vm.valueform))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  $location.path('/admin/configmobileapp');
               });
         }
         else
         {
            $location.path('/admin/configmobileapp');
         }


      }

   };


   vm.cancel = function () {
      $location.path('/admin/configmobileapp');
   };


}








