'use strict';

angular
   .module('theApp')
   .directive('x3mConfigmobileappTitleNavbar', x3mConfigmobileappTitleNavbar)
   .directive('x3mConfigmobileappTitlePanel', x3mConfigmobileappTitlePanel)
   .directive('x3mConfigmobileappInfoPanel', x3mConfigmobileappInfoPanel);


function x3mConfigmobileappTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminConfigmobileappEditCtrl.id != -1)
         {
            this.display = $scope.adminConfigmobileappEditCtrl.keyform;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mConfigmobileappTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminConfigmobileappEditCtrl.id != -1)
         {
            this.display1 = $scope.adminConfigmobileappEditCtrl.keyform;
         }
         else
         {
            this.display1 = "NEUE KONFIGURATION ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mConfigmobileappInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";

         this.show2 = false;
         this.show3 = false;

         this.show1 = true;
         this.display1 = $scope.adminConfigmobileappEditCtrl.created_at + " " + $scope.adminConfigmobileappEditCtrl.created_by_user.first_name + " " + $scope.adminConfigmobileappEditCtrl.created_by_user.last_name + " (" + $scope.adminConfigmobileappEditCtrl.created_by_user.username + ")";

         if ($scope.adminConfigmobileappEditCtrl.updated_at != $scope.adminConfigmobileappEditCtrl.created_at)
         {
            this.show2 = true;
            if ($scope.adminConfigmobileappEditCtrl.updated_by_user)
            {
               this.display2 = $scope.adminConfigmobileappEditCtrl.updated_at + " " + $scope.adminConfigmobileappEditCtrl.updated_by_user.first_name + " " + $scope.adminConfigmobileappEditCtrl.updated_by_user.last_name + " (" + $scope.adminConfigmobileappEditCtrl.updated_by_user.username + ")";
            }
            else
            {
               this.display2 = $scope.adminConfigmobileappEditCtrl.updated_at;
            }
         }

         if ($scope.adminConfigmobileappEditCtrl.deleted_at)
         {
            this.show3 = true;
            this.show2 = false;
            if ($scope.adminConfigmobileappEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminConfigmobileappEditCtrl.deleted_at + " " + $scope.adminConfigmobileappEditCtrl.deleted_by_user.first_name + " " + $scope.adminConfigmobileappEditCtrl.deleted_by_user.last_name + " (" + $scope.adminConfigmobileappEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminConfigmobileappEditCtrl.deleted_at;
            }
         }

      }
   };
   return directive;
}



