'use strict';

angular
   .module('theApp')
   .factory('AdminConfigmobileappService', AdminConfigmobileappService);


AdminConfigmobileappService.$inject = ['$http'];


function AdminConfigmobileappService($http) {

   return {

      getConfigmobileapps: function () {
         return $http({method: 'GET', url: 'api/v1/admin/configmobileapp'});
      },
      
      getConfigmobileapp: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/configmobileapp/edit', data: {'id': id}});
      },

      saveConfigmobileapp: function (id, key, value) {
         return $http({
            method: 'POST',
            url:    'api/v1/admin/configmobileapp/save',
            data:   {'id': id, 'key': key,  'value': value}
         });
      }

   };
}
