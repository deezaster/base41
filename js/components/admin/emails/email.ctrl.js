'use strict';

angular
   .module('theApp')
   .controller('AdminEmailController', AdminEmailController)
   .controller('AdminEmailEditController', AdminEmailEditController);

AdminEmailController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminEmailService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminEmailEditController.$inject = ['$location', 'AdminEmailService', 'toastr', '$sanitize'];


function AdminEmailController(uiGridConstants, $location, $scope, AdminEmailService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridEmails = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            minWidth:         160,
            width:            160,
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'sent_at', displayName: 'Gesendet', minWidth: 100, width: 140, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'subject', displayName: 'Titel', minWidth: 100, width: '20%', enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {
            name:             'attachmentsIcon',
            displayName:      'Anhang',
            minWidth:         65,
            width:            65,
            enableSorting:    false,
            enableFiltering:  false,
            enableColumnMenu: false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'recipient', displayName: 'Empfänger', minWidth: 100, width: '35%', enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {
            name:                 'archived_at',
            displayName:          " Archiviert",
            minWidth:             100, width: 140, enableColumnMenu: false,
            filterHeaderTemplate: "<div class=\"ui-grid-filter-container\" ng-repeat=\"colFilter in col.filters\" ng-class=\"{'ui-grid-filter-cancel-button-hidden' : colFilter.disableCancelFilterButton === true }\"><div ng-if=\"colFilter.type !== 'select'\"><input type=\"text\" class=\"ui-grid-filter-input\" ng-model=\"colFilter.term\" ng-attr-placeholder=\"{{colFilter.placeholder || ''}}\"><div class=\"ui-grid-filter-button\" ng-click=\"colFilter.term = null\" ng-if=\"!colFilter.disableCancelFilterButton\"><i class=\"ui-grid-icon-cancel\" ng-show=\"colFilter.term !== undefined && colFilter.term != null\">&nbsp;</i></div></div><div ng-if=\"colFilter.type === 'select'\"><select class=\"ui-grid-filter-select\" ng-model=\"colFilter.term\" ng-attr-placeholder=\"{{colFilter.placeholder || ''}}\" ng-options=\"option.value as option.label for option in colFilter.selectOptions\"></select><div class=\"ui-grid-filter-button-select\" ng-click=\"colFilter.term = null\" ng-if=\"!colFilter.disableCancelFilterButton\"><i class=\"ui-grid-icon-cancel\" ng-show=\"colFilter.term !== undefined && colFilter.term != null\">&nbsp;</i></div></div></div>",
            headerCellTemplate:   "<div ng-class=\"{ 'sortable': sortable }\"><!-- <div class=\"ui-grid-vertical-bar\">&nbsp;</div> --><div class=\"ui-grid-cell-contents\" col-index=\"renderIndex\"><span><span class=\"fa fa-database\"></span>&nbsp;{{ col.displayName CUSTOM_FILTERS }}</span> <span ui-grid-visible=\"col.sort.direction\" ng-class=\"{ 'ui-grid-icon-up-dir': col.sort.direction == asc, 'ui-grid-icon-down-dir': col.sort.direction == desc, 'ui-grid-icon-blank': !col.sort.direction }\">&nbsp;</span></div><div class=\"ui-grid-column-menu-button\" ng-if=\"grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false\" ng-click=\"toggleMenu($event)\" ng-class=\"{'ui-grid-column-menu-button-last-col': isLastCol}\"><i class=\"ui-grid-icon-angle-down\">&nbsp;</i></div><div ui-grid-filter></div></div>",
            filter:               {
               type:                      uiGridConstants.filter.SELECT,
               term:                      'n',
               selectOptions:             [{value: 'n', label: "Nein"}, {value: 'y', label: "Ja"}, {value: 'a', label: "Alle"}],
               disableCancelFilterButton: true,

               condition: function (searchTerm, cellValue) {

                  if (searchTerm == 'y')
                  {
                     if (cellValue == '')
                     {
                        return false;
                     }
                     else
                     {
                        return true;
                     }
                  }
                  if (searchTerm == 'n')
                  {
                     if (cellValue == '')
                     {
                        return true;
                     }
                     else
                     {
                        return false;
                     }
                  }
                  if (searchTerm == 'a')
                  {
                     return true;
                  }

                  return false;
               }
            }
         }
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         //gridApi.core.on.filterChanged($scope, function () {
         //   var grid = this.grid;
         //   angular.forEach(grid.columns, function (value, key) {
         //      if (value.filters[0])
         //      {
         //         console.log('FILTER TERM FOR ' + value.colDef.name + ' = ' + value.filters[0].term);
         //      }
         //   });
         //});
      },
      appScopeProvider:    {


         onShow: function (row) {


            AdminEmailService.getEmailToShow(row.entity.id)
               .success(function (data) {

                  ModalService.showModal({
                     templateUrl:  "views/shared/modal_showemail.html",
                     controller:   "ModalShowEmailController",
                     controllerAs: "showEmailController",
                     inputs:       {
                        data: data
                     }
                  }).then(function (modal) {
                     modal.element.modal();
                     modal.close.then(function (result) {

                     });
                  });
               });
         },

         //
         //onClick: function (row) {
         //   $location.path('/admin/email/edit/' + row.entity.id);
         //},

         onArchive: function (row) {
            AdminEmailService.archiveEmail(row.entity.id)
               .success(function (data) {
                  DataProviderService.clear();

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  AdminEmailService.getEmails()
                     .success(function (data) {

                        $scope.gridEmails.data = data;
                     });
               });
         },
         onDelete:  function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "E-Mail löschen?",
                  msg1:  "Soll das folgende E-Mail wirklich gelöscht werden?",
                  msg2:  row.entity.subject
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminEmailService.deleteEmail(row.entity.id)
                        .success(function (data) {
                           DataProviderService.clear();

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target:        data["response"]["container"],
                              timeOut:       parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminEmailService.getEmails()
                              .success(function (data) {

                                 $scope.gridEmails.data = data;
                              });
                        });

                  }

               });
            });


         }

      }
   };

   $scope.gridEmails.enableRowSelection = false;
   $scope.gridEmails.enableSelectAll = false;
   $scope.gridEmails.multiSelect = false;
   $scope.gridEmails.enableColumnResizing = true;
   $scope.gridEmails.enableFiltering = true;
   $scope.gridEmails.useExternalFiltering = false;
   $scope.gridEmails.enableGridMenu = false;
   $scope.gridEmails.showGridFooter = true;
   $scope.gridEmails.showColumnFooter = false;
   $scope.gridEmails.rowHeight = 36;

   i18nService.setCurrentLang('de');

   // 0=aktiven 1=archivierten 2=alle
   vm.button = {
      "filter1": 0
   };

   AdminEmailService.getEmails()
      .success(function (data) {

         $scope.gridEmails.data = data;
      });


   vm.newEmail = function () {
      DataProviderService.clear();
      $location.path('/admin/email/edit');
   }

}

function AdminEmailEditController($location, AdminEmailService, toastr, $sanitize) {

   var vm = this;

   var row = null;


   // Neuerfassung
   vm.id = -1;

   vm.orightml = "";
   vm.htmlcontent = vm.orightml;
   vm.disabled = false;
   vm.docs = [];
   vm.recipients = [];

   // Emailadresse und Name des eingeloggten Benutzers holen
   AdminEmailService.getUser()
      .success(function (data) {

         vm.sender_email = data["email"];
         vm.sender_name = data["first_name"] + " " + data["last_name"];
      });

   // Dokumente für die Selectbox
   AdminEmailService.getDocs()
      .success(function (data) {

         vm.docOptions = data;

      });

   // Empfänger Emails für die Selectbox
   AdminEmailService.getRecipients()
      .success(function (data) {

         vm.recipientsOptions = data;

      });


   vm.createData = function () {
      return vm.id == -1;
   };

   vm.editData = function () {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   vm.isArchived = function () {

      if (vm.archived_at != null && vm.archived_at != "0000-00-00 00:00:00")
      {
         return true;
      }
      else
      {
         return false;
      }

   };

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         //if (vm.id != -1)
         //{
         //   AdminEmailService.restoreEmail(
         //      $sanitize(vm.id))
         //
         //      .success(function (data) {
         //
         //         toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
         //            target:        data["response"]["container"],
         //            timeOut:       parseInt(data["response"]["duration"]),
         //            positionClass: data["response"]["placement"]
         //         });
         //
         //      });
         //}
         AdminEmailService.sendEmail(
            $sanitize(vm.subject),
            $sanitize(vm.sender_name),
            $sanitize(vm.sender_email),
            vm.htmlcontent,
            vm.docs,
            vm.recipients)

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target:        data["response"]["container"],
                  timeOut:       parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

            });

         $location.path('/admin/emails');

      }

   };


   vm.cancel = function () {
      $location.path('/admin/emails');
   };


}






