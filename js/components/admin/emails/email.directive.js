'use strict';

angular
   .module('theApp')
   .directive('x3mEmailTitleNavbar', x3mEmailTitleNavbar)
   .directive('x3mEmailTitlePanel', x3mEmailTitlePanel)
   .directive('x3mEmailInfoPanel', x3mEmailInfoPanel)


function x3mEmailTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminEmailEditCtrl.id != -1)
         {
            this.display = $scope.adminEmailEditCtrl.subject;
         }
         else
         {
            this.display = "NEU";
         }

      }
   };
   return directive;
}


function x3mEmailTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminEmailEditCtrl.id != -1)
         {
            this.display1 = $scope.adminEmailEditCtrl.subject;
         }
         else
         {
            this.display1 = "NEUES E-MAIL SCHREIBEN";
         }

      }
   };
   return directive;
}


function x3mEmailInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GESENDET" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-send text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show4"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ARCHIVIERT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-database text-warning"></i> {{ ctrlPanelInfo.display4 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display3 = "";
         this.display4 = "";

         this.show3 = false;
         this.show4 = false;

         this.show1 = true;
         this.display1 = $scope.adminEmailEditCtrl.created_at + " " + $scope.adminEmailEditCtrl.created_by_user.first_name + " " + $scope.adminEmailEditCtrl.created_by_user.last_name + " (" + $scope.adminEmailEditCtrl.created_by_user.username + ")";

         if ($scope.adminEmailEditCtrl.deleted_at)
         {
            this.show3 = true;
            if ($scope.adminEmailEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminEmailEditCtrl.deleted_at + " " + $scope.adminEmailEditCtrl.deleted_by_user.first_name + " " + $scope.adminEmailEditCtrl.deleted_by_user.last_name + " (" + $scope.adminEmailEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminEmailEditCtrl.deleted_at;
            }
         }

         if ($scope.adminEmailEditCtrl.isArchived())
         {
            this.show4 = true;
            this.display4 = $scope.adminEmailEditCtrl.archived_at;
         }
      }
   };
   return directive;
}



