'use strict';

angular
   .module('theApp')
   .factory('AdminEmailService', AdminEmailService);


AdminEmailService.$inject = ['$http'];


function AdminEmailService($http) {

   return {

      getEmails: function () {
         return $http({method: 'GET', url: 'api/v1/admin/emails'});
      },

      getEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/edit', data: {'id': id}});
      },

      getEmailToShow: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getEmail', data: {'id': id}});
      },

      sendEmail: function (subject, sender_name, sender_email, content, docs, recipients) {
         return $http({method: 'POST', url: 'api/v1/admin/email/send', data: {'subject': subject, 'sender_name': sender_name, 'sender_email': sender_email, 'content': content, 'docs': docs, 'recipients': recipients}});
      },

      deleteEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/delete', data: {'id': id}});
      },

      restoreEmail: function (id) {
         return $http({
            method: 'POST',
            url:    'api/v1/admin/email/restore',
            data:   {'id': id}
         });
      },

      archiveEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/archive', data: {'id': id}});
      },

      unarchiveEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/unarchive', data: {'id': id}});
      },

      getRecipients: function () {
         return $http({method: 'GET', url: 'api/v1/admin/email/getRecipients'});
      },

      getDocs: function () {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/getFiles'});
      },

      getUser: function () {
         return $http({method: 'GET', url: 'api/v1/auth/getUser'});
      }

   };
}
