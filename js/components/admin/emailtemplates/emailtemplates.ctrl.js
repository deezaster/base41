'use strict';

angular
   .module('theApp')
   .controller('AdminEmailTemplatesController', AdminEmailTemplatesController)
   .controller('AdminEmailTemplatesEditByIdController', AdminEmailTemplatesEditByIdController)
   .controller('AdminEmailTemplatesEditController', AdminEmailTemplatesEditController);

AdminEmailTemplatesController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminEmailTemplatesService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminEmailTemplatesEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminEmailTemplatesService'];
AdminEmailTemplatesEditController.$inject = ['$location', 'AdminEmailTemplatesService', 'DataProviderService', 'toastr', '$sanitize', 'ModalService'];


function AdminEmailTemplatesEditByIdController($location, DataProviderService, $routeParams, AdminEmailTemplatesService) {

   var id = $routeParams.templateId;

   AdminEmailTemplatesService.getEmailTemplate(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("emailtemplate", data);
         $location.path('/admin/emailtemplate/edit');
      })
      .error(function (data) {
      });
}


function AdminEmailTemplatesController(uiGridConstants, $location, $scope, AdminEmailTemplatesService, DataProviderService, i18nService, toastr, ModalService) {


   var vm = this;


   $scope.gridTemplates = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         130,
            width:            130,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'name', displayName: 'Name', minWidth: 100, width: 300, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'subject', displayName: 'Betreff', enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}

      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      },
      appScopeProvider:    {

         // Editieren
         onClick: function (row) {
            $location.path('/admin/emailtemplate/edit/' + row.entity.id);
         }
      }
   };

   $scope.gridTemplates.enableRowSelection = false;
   $scope.gridTemplates.enableSelectAll = false;
   $scope.gridTemplates.multiSelect = false;
   $scope.gridTemplates.enableColumnResizing = true;
   $scope.gridTemplates.enableFiltering = true;
   $scope.gridTemplates.useExternalFiltering = false;
   $scope.gridTemplates.enableGridMenu = false;
   $scope.gridTemplates.showGridFooter = false;
   $scope.gridTemplates.showColumnFooter = false;
   $scope.gridTemplates.rowHeight = 36;


   AdminEmailTemplatesService.getEmailTemplates()
      .success(function (data) {

         $scope.gridTemplates.data = data;
      });

}


function AdminEmailTemplatesEditController($location, AdminEmailTemplatesService, DataProviderService, toastr, $sanitize, ModalService) {

   var vm = this;

   var row = DataProviderService.get("emailtemplate");

   if (row)
   {
      // Mutation
      vm.id = row.id;
      vm.name = row.name;
      vm.subject = row.subject;

      vm.created_at = row.created_at;
      vm.updated_at = row.updated_at;
      vm.deleted_at = row.deleted_at;
      vm.created_by_user = row.created_by_user;
      vm.updated_by_user = row.updated_by_user;
      vm.deleted_by_user = row.deleted_by_user;

      vm.orightml = row.content;
      vm.htmlcontent = vm.orightml;
      vm.disabled = false;
   }
   else
   {
      // Neuerfassung (zur Zeit nicht erlaubt!
      $location.path('/admin/emailtemplates');
   }

   vm.createData = function () {
      return vm.id == -1;
   };

   vm.editData = function () {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };


   vm.submitForm = function (isValid) {

      if (isValid)
      {
         if (vm.id != -1)
         {
            AdminEmailTemplatesService.saveEmailTemplate(
               $sanitize(vm.id),
               $sanitize(vm.subject),
               vm.htmlcontent)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }
         else
         {
            // Create is not allowed yet!
            $location.path('/admin/emailtemplates');
         }

         $location.path('/admin/emailtemplates');

      }

   };


   vm.cancel = function () {
      $location.path('/admin/emailtemplates');
   };

}








