'use strict';

angular
   .module('theApp')
   .directive('x3mEmailtemplateTitleNavbar', x3mEmailtemplateTitleNavbar)
   .directive('x3mEmailtemplateTitlePanel', x3mEmailtemplateTitlePanel)
   .directive('x3mEmailtemplateInfoPanel', x3mEmailtemplateInfoPanel);


function x3mEmailtemplateTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminEmailTemplateEditCtrl.id != -1)
         {
            this.display = $scope.adminEmailTemplateEditCtrl.name;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mEmailtemplateTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminEmailTemplateEditCtrl.id != -1)
         {
            this.display1 = $scope.adminEmailTemplateEditCtrl.name;
         }
         else
         {
            this.display1 = "NEUES E-MAIL TEMPLATE ERFASSEN";
         }

      }
   };
   return directive;
}


function x3mEmailtemplateInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";

         this.show2 = false;
         this.show3 = false;

         this.show1 = true;
         this.display1 = $scope.adminEmailTemplateEditCtrl.created_at + " " + $scope.adminEmailTemplateEditCtrl.created_by_user.first_name + " " + $scope.adminEmailTemplateEditCtrl.created_by_user.last_name + " (" + $scope.adminEmailTemplateEditCtrl.created_by_user.username + ")";

         if ($scope.adminEmailTemplateEditCtrl.updated_at != $scope.adminEmailTemplateEditCtrl.created_at)
         {
            this.show2 = true;
            if ($scope.adminEmailTemplateEditCtrl.updated_by_user)
            {
               this.display2 = $scope.adminEmailTemplateEditCtrl.updated_at + " " + $scope.adminEmailTemplateEditCtrl.updated_by_user.first_name + " " + $scope.adminEmailTemplateEditCtrl.updated_by_user.last_name + " (" + $scope.adminEmailTemplateEditCtrl.updated_by_user.username + ")";
            }
            else
            {
               this.display2 = $scope.adminEmailTemplateEditCtrl.updated_at;
            }
         }

         if ($scope.adminEmailTemplateEditCtrl.deleted_at)
         {
            this.show3 = true;
            this.show2 = false;
            if ($scope.adminEmailTemplateEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminEmailTemplateEditCtrl.deleted_at + " " + $scope.adminEmailTemplateEditCtrl.deleted_by_user.first_name + " " + $scope.adminEmailTemplateEditCtrl.deleted_by_user.last_name + " (" + $scope.adminEmailTemplateEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminEmailTemplateEditCtrl.deleted_at;
            }
         }

      }
   };
   return directive;
}



