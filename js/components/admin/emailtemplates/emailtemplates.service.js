'use strict';

angular
   .module('theApp')
   .factory('AdminEmailTemplatesService', AdminEmailTemplatesService);


AdminEmailTemplatesService.$inject = ['$http'];


function AdminEmailTemplatesService($http) {

   return {

      getEmailTemplates: function () {
         return $http({method: 'GET', url: 'api/v1/admin/emailtemplates'});
      },

      getEmailTemplate: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/emailtemplate/edit', data: {'id': id}});
      },

      saveEmailTemplate: function (id, subject, content) {
         return $http({
            method: 'POST',
            url:    'api/v1/admin/emailtemplate/save',
            data:   {'id': id, 'subject': subject, 'content': content}
         });
      },

   };
}
