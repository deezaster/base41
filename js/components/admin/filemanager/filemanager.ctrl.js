'use strict';

angular
   .module('theApp')
   .controller('AdminFilemanagerController', AdminFilemanagerController)
   .controller('ModalUploadDocController', ModalUploadDocController);

AdminFilemanagerController.$inject = ['$location', 'AdminFilemanagerService', 'toastr',  'ModalService', '$scope', 'uiGridConstants', 'i18nService'];
ModalUploadDocController.$inject = ['nodeId', 'nodeName', '$upload', 'toastr', 'close'];


function AdminFilemanagerController($location, AdminFilemanagerService, toastr, ModalService, $scope, uiGridConstants, i18nService) {

   var vm = this;

   vm.selectedNodeId = null;
   vm.ignoreChanges = false;
   vm.newNode = {};
   vm.treeData = [];
   vm.treeConfig = {
      core:          {
         multiple:       false,
         animation:      true,
         error:          function (error) {
            $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
         },
         check_callback: true,
         worker:         true
      },
      types:         {
         default: {
            icon: 'glyphicon glyphicon-folder-close'
         },
         file:    {
            icon: 'fa fa-file-o'
         },
         dir:     {
            icon: 'glyphicon glyphicon-folder-close'
         },
         downloaddir:     {
            icon: 'fa fa-check-square-o text-danger'
         }
      },
      version:       1,
      plugins:       ['types', 'contextmenu', 'dnd'],
      "contextmenu": {
         "items": function ($node) {
            var tree = $("#tree").jstree(true);
            return {
               "upload": {
                  "separator_before": false,
                  "separator_after":  true,
                  "_disabled":        false, //(this.check("create_node", data.reference, {}, "last")),
                  "label":            "Dokument hochladen",
                  "icon":             "fa fa-upload text-primary",
                  "action":           function (data) {
                     var inst = $.jstree.reference(data.reference), obj = inst.get_node(data.reference);

                     ModalService.showModal({
                        templateUrl:  "views/shared/modal_uploaddoc.html",
                        controller:   "ModalUploadDocController",
                        controllerAs: "uploadDocController",
                        inputs:       {
                           nodeId:   obj.id,
                           nodeName: obj.text
                        }
                     }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {

                           AdminFilemanagerService.getFilesPerNode(vm.selectedNodeId)
                              .success(function (data) {

                                 $scope.gridOptions.data = data;

                              });

                        });
                     });
                  }
               },
               "create": {
                  "separator_before": false,
                  "separator_after":  false,
                  "_disabled":        false, //(this.check("create_node", data.reference, {}, "last")),
                  "label":            "Verzeichnis erstellen",
                  "icon":             "glyphicon glyphicon-folder-close",
                  "action":           function (data) {

                     var inst = $.jstree.reference(data.reference), obj = inst.get_node(data.reference);
                     inst.create_node(obj, {"text": "Verzeichnis Neu"}, "last", function (new_node) {

                        AdminFilemanagerService.createDir("Verzeichnis Neu", new_node.id, new_node.parent)
                           .success(function (data) {

                           });

                     });

                  }
               },
               "rename": {
                  "separator_before": false,
                  "separator_after":  false,
                  "_disabled":        false, //(this.check("rename_node", data.reference, this.get_parent(data.reference), "")),
                  "label":            "Verzeichnis umbenennen",
                  "icon":             "fa fa-font",
                  "action":           function (data) {
                     var inst = $.jstree.reference(data.reference), obj = inst.get_node(data.reference);
                     inst.edit(obj);
                  }
               },
               "remove": {
                  "separator_before": false,
                  "separator_after":  false,
                  "_disabled":        false, //(this.check("delete_node", data.reference, this.get_parent(data.reference), "")),
                  "label":            "Verzeichnis löschen",
                  "icon":             "glyphicon glyphicon-trash",
                  "action":           function (data) {
                     var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);

                     ModalService.showModal({
                        templateUrl: "views/shared/modal_yesno.html",
                        controller:  "ModalController",
                        inputs:      {
                           title: "Verzeichnis löschen?",
                           msg1:  "Soll das folgende Verzeichnis wirklich gelöscht werden?",
                           msg2:  obj.text
                        }
                     }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {

                           if (result)
                           {
                              AdminFilemanagerService.deleteDir(obj.id)
                                 .success(function (data) {

                                    if (inst.is_selected(obj))
                                    {
                                       inst.delete_node(inst.get_selected());
                                    }
                                    else
                                    {
                                       inst.delete_node(obj);
                                    }

                                 });
                           }

                        });
                     });


                  }
               },
               "downloaddir": {
                  "separator_before": true,
                  "separator_after":  false,
                  "_disabled":        false, //(this.check("create_node", data.reference, {}, "last")),
                  "label":            "Als Download-Verzeichnis markieren",
                  "icon":             "fa fa-check-square-o text-danger",
                  "action":           function (data) {

                     var inst = $.jstree.reference(data.reference), obj = inst.get_node(data.reference);

                     AdminFilemanagerService.setDownloadDir(obj.id)
                        .success(function (data) {

                           AdminFilemanagerService.getStructure()
                              .success(function (data) {

                                 vm.treeData = data;

                                 // If from some reason you would like to recreate the tree, the right way to do it is update the tree
                                 // configuration object. Once the directive will detect a change to the tree configuration
                                 // it will destory the tree and recreate it.
                                 vm.treeConfig.version++;
                              });

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target: data["response"]["container"],
                              timeOut: parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });
                        });


                  }
               }
            };
         }
      }
   };

   // get Filestructure
   vm.refreshTree = function () {

      AdminFilemanagerService.getStructure()
         .success(function (data) {

            vm.treeData = data;

            // If from some reason you would like to recreate the tree, the right way to do it is update the tree
            // configuration object. Once the directive will detect a change to the tree configuration
            // it will destory the tree and recreate it.
            vm.treeConfig.version++;
         });
   };


   this.refreshTree();


   vm.readyTree = function () {
      vm.ignoreChanges = false;
   };


   $(document).on('dnd_stop.vakata', function (e, data) {

      var ref = $('.jstree').jstree(true);
      var id = ref.get_node(data.element).id;
      var parentId = ref.get_node(data.element).parent;

      AdminFilemanagerService.moveDir(id, parentId)
         .success(function (data) {
         });

   });


   vm.renameNode = function (e, item) {

      AdminFilemanagerService.renameDir(item.node.id, item.node.text)
         .success(function (data) {
         });

   };
   vm.selectNode = function (e, item) {

      vm.selectedNodeId = item.node.id;

      AdminFilemanagerService.getFilesPerNode(item.node.id)
         .success(function (data) {

            $scope.gridOptions.data = data;

         });
   };
   vm.applyModelChanges = function () {
      return !vm.ignoreChanges;
   };


   vm.cancel = function () {
      $location.path('/admin');
   };


   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      '',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         50,
            width:            50,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {
            name:             'filename',
            displayName:      'Dokument',
            minWidth:         100,
            width:            150,
            enableColumnMenu: false,
            filters:          [{condition: uiGridConstants.filter.CONTAINS}]
         },
         {name: 'description', displayName: 'Beschreibung', minWidth: 200, width: 330, enableCellEdit: true, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'filesize', displayName: 'Grösse', minWidth: 70, width: 70, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'updated_at', displayName: 'Datum', minWidth: 100, width: 140, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}

      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
            //console.log('edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue);
            $scope.$apply();

            AdminFilemanagerService.updateDescription(rowEntity.id, newValue)
               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });

         });
      },
      appScopeProvider:    {
         onDelete: function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "Dokument löschen?",
                  msg1:  "Soll das folgende Dokument wirklich gelöscht werden?",
                  msg2:  row.entity.filename
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminFilemanagerService.deleteFile(row.entity.id)
                        .success(function (data) {

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target: data["response"]["container"],
                              timeOut: parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminFilemanagerService.getFilesPerNode(vm.selectedNodeId)
                              .success(function (data) {

                                 $scope.gridOptions.data = data;

                              });

                        });

                  }

               });
            });


         },
      }
   };

   $scope.gridOptions.data = [];

   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');
}

function ModalUploadDocController(nodeId, nodeName, $upload, toastr, close) {

   var vm = this;
   vm.nodeId = nodeId;
   vm.nodeName = nodeName;
   vm.description = "";

   vm.submitForm = function (isValid, files) {

      if (isValid)
      {
         if (files && files.length)
         {
            for (var i = 0; i < files.length; i++)
            {
               var file = files[i];

               $upload.upload({
                  url:    'api/v1/admin/filemanager/uploadFile',
                  fields: {'id': vm.nodeId, 'description': vm.description},
                  file:   file
               }).progress(function (evt) {
                  //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                  //console.log('progress ' + evt.config.file.name + ': ' + file.progress + '% ');

               }).success(function (data, status, headers, config) {
                  //console.log('file ' + config.file.name + ' uploaded. Response: ' + data);

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  vm.close();
               });
            }
         }

      }

   };

   vm.close = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };


   vm.cancel = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };

}






