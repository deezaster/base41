'use strict';

angular
   .module('theApp')
   .factory('AdminFilemanagerService', AdminFilemanagerService);


AdminFilemanagerService.$inject = ['$http'];


function AdminFilemanagerService($http) {

   return {

      getStructure: function () {
         return $http({method: 'GET', url: 'api/v1/admin/filemanager'});
      },
      getFilesPerNode: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/getFilesPerNode', data: {'id': id}});
      },
      deleteFile: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/deleteFile', data: {'id': id}});
      },
      createDir: function (name, id, toId) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/createDir', data: {'name': name, 'id': id, 'toId': toId}});
      },
      renameDir: function (id, name) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/renameDir', data: {'id': id, 'name': name}});
      },
      deleteDir: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/deleteDir', data: {'id': id}});
      },
      moveDir: function (fromId, toId) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/moveDir', data: {'id': fromId, 'toId': toId}});
      },
      updateDescription: function (id, description) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/updateDescription', data: {'id': id, 'description': description}});
      },
      setDownloadDir: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/setDownloadDir', data: {'id': id}});
      }
   };
}
