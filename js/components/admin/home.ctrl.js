'use strict';

angular
   .module('theApp')
   .controller('AdminHomeController', AdminHome);

AdminHome.$inject = ['AdminService','$scope', 'uiGridConstants'];


function AdminHome(AdminService, $scope, uiGridConstants) {

   // Der AdminService prüft, ob der Benutzer "admin" Rechte hat
   // Wenn kein admin-Recht für diesen Benutzer definiert ist, wir der HTTP-StatusCode 403 geliefert
   // und im httpInterceptor (File: http.service.js) behandelt.
   AdminService.init()
      .success(function (data) {
      })
      .error(function (data) {
         // Errorhandling in: http.service.js (httpInterceptor)
      });



   var vm = this;

   $scope.gridUserevents = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {name: 'event_at', displayName: 'Zeitpunkt', minWidth: 100, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}],
            sort: {
               direction: uiGridConstants.DESC,
               priority: 0
            }},
         {name: 'username', displayName: 'Benutzer', minWidth: 50, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'topictranslated', displayName: 'Bereich', minWidth: 50, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'eventtranslated', displayName: 'Ereignis', minWidth: 50, width: 250, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'detail', displayName: 'Beschreibung', minWidth: 100,  enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}],
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         }
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      }
   };

   $scope.gridUserevents.enableRowSelection = false;
   $scope.gridUserevents.enableSelectAll = false;
   $scope.gridUserevents.multiSelect = false;
   $scope.gridUserevents.enableColumnResizing = true;
   $scope.gridUserevents.enableFiltering = true;
   $scope.gridUserevents.useExternalFiltering = false;
   $scope.gridUserevents.enableGridMenu = false;
   $scope.gridUserevents.showGridFooter = false;
   $scope.gridUserevents.showColumnFooter = false;
   $scope.gridUserevents.rowHeight = 36;


   AdminService.getUserEvents()
      .success(function (data) {

         $scope.gridUserevents.data = data;
      });


}








