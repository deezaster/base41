'use strict';

angular
   .module('theApp')
   .controller('AdminMassEmailController', AdminMassEmailController)
   .controller('AdminMassEmailEditByIdController', AdminMassEmailEditByIdController)
   .controller('AdminMassEmailEditController', AdminMassEmailEditController)
   .controller('AdminMassEmailLogController', AdminMassEmailLogController)
   .controller('AdminMassEmailSendController', AdminMassEmailSendController);

AdminMassEmailController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminMassEmailService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminMassEmailEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminMassEmailService'];
AdminMassEmailEditController.$inject = ['$location', 'AdminMassEmailService', 'DataProviderService', 'toastr', '$sanitize', 'ModalService'];
AdminMassEmailLogController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminMassEmailService', 'DataProviderService', 'i18nService'];
AdminMassEmailSendController.$inject = ['$sce', '$location', 'AdminMassEmailService', 'DataProviderService', 'toastr', '$sanitize', 'ModalService'];

function AdminMassEmailEditByIdController($location, DataProviderService, $routeParams, AdminMassEmailService) {

   var id = $routeParams.massemailId;

   AdminMassEmailService.getEmail(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("email", data);
         $location.path('/admin/massemail/edit');
      })
      .error(function (data) {
      });
}


function AdminMassEmailController(uiGridConstants, $location, $scope, AdminMassEmailService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            minWidth:         160,
            width:            160,
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'id', displayName: 'ID', minWidth: 50, width: 60, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'subject', displayName: 'Titel', minWidth: 200, width: 200, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {
            name:             'attachmentsIcon',
            displayName:      'Anhang',
            minWidth:         65,
            width:            65,
            enableSorting:    false,
            enableFiltering:  false,
            enableColumnMenu: false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'sender_name', displayName: 'Absender', minWidth: 200, width: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'sender_email', displayName: 'Absender E-Mail', minWidth: 200, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'updated_at', displayName: 'Version', minWidth: 100, width: 140, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {
            name:                 'archived_at',
            displayName:          " Archiviert",
            minWidth:             100, width: 140, enableColumnMenu: false,
            filterHeaderTemplate: "<div class=\"ui-grid-filter-container\" ng-repeat=\"colFilter in col.filters\" ng-class=\"{'ui-grid-filter-cancel-button-hidden' : colFilter.disableCancelFilterButton === true }\"><div ng-if=\"colFilter.type !== 'select'\"><input type=\"text\" class=\"ui-grid-filter-input\" ng-model=\"colFilter.term\" ng-attr-placeholder=\"{{colFilter.placeholder || ''}}\"><div class=\"ui-grid-filter-button\" ng-click=\"colFilter.term = null\" ng-if=\"!colFilter.disableCancelFilterButton\"><i class=\"ui-grid-icon-cancel\" ng-show=\"colFilter.term !== undefined && colFilter.term != null\">&nbsp;</i></div></div><div ng-if=\"colFilter.type === 'select'\"><select class=\"ui-grid-filter-select\" ng-model=\"colFilter.term\" ng-attr-placeholder=\"{{colFilter.placeholder || ''}}\" ng-options=\"option.value as option.label for option in colFilter.selectOptions\"></select><div class=\"ui-grid-filter-button-select\" ng-click=\"colFilter.term = null\" ng-if=\"!colFilter.disableCancelFilterButton\"><i class=\"ui-grid-icon-cancel\" ng-show=\"colFilter.term !== undefined && colFilter.term != null\">&nbsp;</i></div></div></div>",
            headerCellTemplate:   "<div ng-class=\"{ 'sortable': sortable }\"><!-- <div class=\"ui-grid-vertical-bar\">&nbsp;</div> --><div class=\"ui-grid-cell-contents\" col-index=\"renderIndex\"><span><span class=\"fa fa-database\"></span>&nbsp;{{ col.displayName CUSTOM_FILTERS }}</span> <span ui-grid-visible=\"col.sort.direction\" ng-class=\"{ 'ui-grid-icon-up-dir': col.sort.direction == asc, 'ui-grid-icon-down-dir': col.sort.direction == desc, 'ui-grid-icon-blank': !col.sort.direction }\">&nbsp;</span></div><div class=\"ui-grid-column-menu-button\" ng-if=\"grid.options.enableColumnMenus && !col.isRowHeader  && col.colDef.enableColumnMenu !== false\" ng-click=\"toggleMenu($event)\" ng-class=\"{'ui-grid-column-menu-button-last-col': isLastCol}\"><i class=\"ui-grid-icon-angle-down\">&nbsp;</i></div><div ui-grid-filter></div></div>",
            filter:               {
               type:                      uiGridConstants.filter.SELECT,
               term:                      'n',
               selectOptions:             [{value: 'n', label: "Nein"}, {value: 'y', label: "Ja"}, {value: 'a', label: "Alle"}],
               disableCancelFilterButton: true,

               condition: function (searchTerm, cellValue) {

                  if (searchTerm == 'y')
                  {
                     if (cellValue == '')
                     {
                        return false;
                     }
                     else
                     {
                        return true;
                     }
                  }
                  if (searchTerm == 'n')
                  {
                     if (cellValue == '')
                     {
                        return true;
                     }
                     else
                     {
                        return false;
                     }
                  }
                  if (searchTerm == 'a')
                  {
                     return true;
                  }

                  return false;
               }
            }
         }
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         //gridApi.core.on.filterChanged($scope, function () {
         //   var grid = this.grid;
         //   angular.forEach(grid.columns, function (value, key) {
         //      if (value.filters[0])
         //      {
         //         console.log('FILTER TERM FOR ' + value.colDef.name + ' = ' + value.filters[0].term);
         //      }
         //   });
         //});
      },
      appScopeProvider:    {


         // Editieren
         onClick: function (row) {
            $location.path('/admin/massemail/edit/' + row.entity.id);
         },

         // Editieren
         onSentLog: function (row) {

            AdminMassEmailService.getEmail(row.entity.id)
               .success(function (data) {
                  DataProviderService.clear();
                  DataProviderService.add("email", data);
                  $location.path('/admin/massemail_log');
               })
               .error(function (data) {
               });
         },

         onArchive: function (row) {
            AdminMassEmailService.archiveEmail(row.entity.id)
               .success(function (data) {
                  DataProviderService.clear();

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  AdminMassEmailService.getEmails()
                     .success(function (data) {

                        $scope.gridOptions.data = data;
                     });
               });
         },
         onDelete:  function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "E-Mail löschen?",
                  msg1:  "Soll das folgende E-Mail wirklich gelöscht werden?",
                  msg2:  row.entity.first_name + " " + row.entity.last_name
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminMassEmailService.deleteEmail(row.entity.id)
                        .success(function (data) {
                           DataProviderService.clear();

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target: data["response"]["container"],
                              timeOut: parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminMassEmailService.getEmails()
                              .success(function (data) {

                                 $scope.gridOptions.data = data;
                              });
                        });

                  }

               });
            });


         }

      }
   };

   $scope.gridOptions.enableRowSelection = false;
   $scope.gridOptions.enableSelectAll = false;
   $scope.gridOptions.multiSelect = false;
   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.useExternalFiltering = false;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');

   // 0=aktiven 1=archivierten 2=alle
   vm.button = {
      "filter1": 0
   };

   AdminMassEmailService.getEmails()
      .success(function (data) {

         $scope.gridOptions.data = data;
      });


   vm.newEmail = function () {
      DataProviderService.clear();
      $location.path('/admin/massemail/edit');
   }

}

function AdminMassEmailEditController($location, AdminMassEmailService, DataProviderService, toastr, $sanitize, ModalService) {

   var vm = this;

   var row = DataProviderService.get("email");

   if (row)
   {
      // Mutation
      vm.id = row.id;
      vm.subject = row.subject;
      vm.sender_email = row.sender_email;
      vm.sender_name = row.sender_name;

      vm.archived_at = row.archived_at;
      vm.created_at = row.created_at;
      vm.updated_at = row.updated_at;
      vm.deleted_at = row.deleted_at;
      vm.created_by_user = row.created_by_user;
      vm.updated_by_user = row.updated_by_user;
      vm.deleted_by_user = row.deleted_by_user;

      vm.orightml = row.content;
      vm.htmlcontent = vm.orightml;
      vm.disabled = false;

      vm.docs = [];
      vm.recipients = [];

      // Dokumente lesen
      AdminMassEmailService.getDocs()
         .success(function (data) {

            vm.docOptions = data;

            if (angular.isArray(row.attachments))
            {
               var attachments = [];

               row.attachments.forEach(function (item) {
                  var id = item["id"].toString();
                  attachments.push(id);
               });

               vm.docs = attachments;
            }

         });


   }
   else
   {
      // Neuerfassung
      vm.id = -1;

      vm.orightml = "";
      vm.htmlcontent = vm.orightml;
      vm.disabled = false;
      vm.docs = [];
      vm.recipients = [];

      // Emailadresse und Name des eingeloggten Benutzers holen
      AdminMassEmailService.getUser()
         .success(function (data) {

            vm.sender_email = data["email"];
            vm.sender_name = data["first_name"] + " " + data["last_name"];
         });

      // Dokumente für die Selectbox
      AdminMassEmailService.getDocs()
         .success(function (data) {

            vm.docOptions = data;

         });

   }

   vm.createData = function() {
      return vm.id == -1;
   };

   vm.editData = function() {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   vm.isArchived = function () {

      if (vm.archived_at != null && vm.archived_at != "0000-00-00 00:00:00")
      {
         return true;
      }
      else
      {
         return false;
      }

   };


   vm.submitForm = function (isValid) {

      if (isValid)
      {
         if (vm.id != -1)
         {
            AdminMassEmailService.saveEmail(
               $sanitize(vm.id),
               $sanitize(vm.subject),
               $sanitize(vm.sender_name),
               $sanitize(vm.sender_email),
               vm.htmlcontent,
               vm.docs)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }
         else
         {
            AdminMassEmailService.insertEmail(
               $sanitize(vm.id),
               $sanitize(vm.subject),
               $sanitize(vm.sender_name),
               $sanitize(vm.sender_email),
               vm.htmlcontent,
               vm.docs)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target: data["response"]["container"],
                     timeOut: parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }

         $location.path('/admin/massemails');

      }

   };


   vm.cancel = function () {
      $location.path('/admin/massemails');
   };

   vm.archive = function () {

      AdminMassEmailService.archiveEmail(vm.id)
         .success(function (data) {
            DataProviderService.clear();

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target: data["response"]["container"],
               timeOut: parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

            $location.path('/admin/massemails');
         });
   };

   vm.unarchive = function () {

      AdminMassEmailService.unarchiveEmail(vm.id)
         .success(function (data) {
            DataProviderService.clear();

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target: data["response"]["container"],
               timeOut: parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

            $location.path('/admin/massemails');
         });
   };

   vm.delete = function () {

      // Sicherheitsabfrage
      ModalService.showModal({
         templateUrl: "views/shared/modal_yesno.html",
         controller:  "ModalController",
         inputs:      {
            title: "E-Mail löschen?",
            msg1:  "Soll dieses E-Mail wirklich gelöscht werden?",
            msg2:  vm.subject
         }
      }).then(function (modal) {
         modal.element.modal();
         modal.close.then(function (result) {
            if (result)
            {
               AdminMassEmailService.deleteEmail(vm.id)
                  .success(function (data) {
                     DataProviderService.clear();

                     toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                        target: data["response"]["container"],
                        timeOut: parseInt(data["response"]["duration"]),
                        positionClass: data["response"]["placement"]
                     });

                     $location.path('/admin/massemails');

                  });

            }

         });
      });

   };

}

function AdminMassEmailLogController(uiGridConstants, $location, $scope, AdminMassEmailService, DataProviderService, i18nService) {

   var vm = this;

   var row = DataProviderService.get("email");

   if (!row)
   {
      $location.path('/admin/massemails');
   }
   else
   {

      vm.id = row.id;
      vm.subject = row.subject;

      $scope.gridOptions = {
         paginationPageSizes: [10, 50, 100],
         paginationPageSize:  10,
         columnDefs:          [
            {name: 'sent_at', displayName: 'Gesendet', minWidth: 180, width: 180, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}],
               sort: {
                  direction: uiGridConstants.DESC,
                  priority: 0
               }},
            {name: 'type', displayName: 'Empfänger', minWidth: 50, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
            {name: 'cnt', displayName: 'Anzahl', minWidth: 50, width: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}
         ],
         onRegisterApi:       function (gridApi) {
            $scope.gridApi = gridApi;

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {

               AdminMassEmailService.getSentLogRecipients(row.entity.id)
                  .success(function (data) {

                     $scope.gridOptions2.data = data;
                  });
            });
         }
      };

      $scope.gridOptions.enableRowSelection = false;
      $scope.gridOptions.enableSelectAll = false;
      $scope.gridOptions.multiSelect = false;
      $scope.gridOptions.enableColumnResizing = false;
      $scope.gridOptions.enableFiltering = true;
      $scope.gridOptions.useExternalFiltering = false;
      $scope.gridOptions.enableGridMenu = false;
      $scope.gridOptions.showGridFooter = false;
      $scope.gridOptions.showColumnFooter = false;
      $scope.gridOptions.rowHeight = 36;


      AdminMassEmailService.getSentLog(row.id)
         .success(function (data) {

            $scope.gridOptions.data = data;
         });


      $scope.gridOptions2 = {
         paginationPageSizes: [10, 50, 100],
         paginationPageSize:  10,
         columnDefs:          [
            {name: 'recipient', displayName: 'Empfänger', enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}],
               cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
            }
         ],
         onRegisterApi:       function (gridApi) {
            $scope.gridApi = gridApi;
         }
      };

      $scope.gridOptions2.enableRowSelection = false;
      $scope.gridOptions2.enableSelectAll = false;
      $scope.gridOptions2.multiSelect = false;
      $scope.gridOptions2.enableColumnResizing = false;
      $scope.gridOptions2.enableFiltering = true;
      $scope.gridOptions2.useExternalFiltering = false;
      $scope.gridOptions2.enableGridMenu = false;
      $scope.gridOptions2.showGridFooter = false;
      $scope.gridOptions2.showColumnFooter = false;
      $scope.gridOptions2.rowHeight = 36;

      i18nService.setCurrentLang('de');

   }

}


function AdminMassEmailSendController($sce, $location, AdminMassEmailService, DataProviderService, toastr, $sanitize, ModalService) {

   var vm = this;

   var row = DataProviderService.get("email");

   if (!row)
   {
      $location.path('/admin/massemails');
   }
   else
   {
      vm.id = row.id;
      vm.subject = row.subject;
      vm.sender_email = row.sender_email;
      vm.sender_name = row.sender_name;
      vm.emailcontent = $sce.trustAsHtml(row.content);

      vm.docs = [];
      vm.to = [];
      vm.cc = [];
      vm.bcc = [];

      // Dokumente lesen
      AdminMassEmailService.getDocs()
         .success(function (data) {

            if (angular.isArray(row.attachments))
            {
               var att = row.attachments;

               // iterate over all documents
               data.forEach(function (item) {

                  // search in attachments
                  att.forEach(function (search) {
                     if (search["id"] == item["id"])
                     {
                        vm.docs.push(item);
                     }
                  })

               });
            }

         });

      // Empfänger lesen
      AdminMassEmailService.getRecipients()
         .success(function (data) {

            vm.recipientsOptions = data;

            if (angular.isArray(row.to))
            {
               var to = [];

               row.to.forEach(function (item) {
                  var id = item["id"].toString();
                  to.push(id);
               });

               vm.to = to;
            }
            if (angular.isArray(row.to))
            {
               var cc = [];

               row.cc.forEach(function (item) {
                  var id = item["id"].toString();
                  cc.push(id);
               });

               vm.cc = cc;
            }
            if (angular.isArray(row.to))
            {
               var bcc = [];

               row.bcc.forEach(function (item) {
                  var id = item["id"].toString();
                  bcc.push(id);
               });

               vm.bcc = bcc;
            }

         });

   }

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         // TODO:
         AdminMassEmailService.sendEmail(
            $sanitize(vm.id),
            vm.to,
            vm.cc,
            vm.bcc)

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

            });

         //$location.path('/admin/massemails');
      }

   };


   vm.cancel = function () {
      $location.path('/admin/massemails');
   };


}









