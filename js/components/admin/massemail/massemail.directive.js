'use strict';

angular
   .module('theApp')
   .directive('x3mMassemailTitleNavbar', x3mMassemailTitleNavbar)
   .directive('x3mMassemailTitlePanel', x3mMassemailTitlePanel)
   .directive('x3mMassemailInfoPanel', x3mMassemailInfoPanel)

   .directive('x3mMassemailLogTitleNavbar', x3mMassemailLogTitleNavbar)
   .directive('x3mMassemailLogTitlePanel', x3mMassemailLogTitlePanel);


// Aufruf: <span x3m-massemail-title-navbar></span>
function x3mMassemailTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminMassEmailEditCtrl.id != -1)
         {
            this.display = $scope.adminMassEmailEditCtrl.subject;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}

// Aufruf: <span x3m-massemail-log-title-navbar></span>
function x3mMassemailLogTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         this.display = "E-Mail Protokoll: '" + $scope.adminMassEmailLogCtrl.subject + "'";

      }
   };
   return directive;
}

function x3mMassemailTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminMassEmailEditCtrl.id != -1)
         {
            this.display1 = $scope.adminMassEmailEditCtrl.subject;
         }
         else
         {
            this.display1 = "NEUES MASSEN E-MAIL ERFASSEN";
         }

      }
   };
   return directive;
}

function x3mMassemailLogTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         this.display1 = $scope.adminMassEmailLogCtrl.subject;
      }
   };
   return directive;
}

function x3mMassemailInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show4"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ARCHIVIERT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-database text-warning"></i> {{ ctrlPanelInfo.display4 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";
         this.display4 = "";

         this.show2 = false;
         this.show3 = false;
         this.show4 = false;

         this.show1 = true;
         this.display1 = $scope.adminMassEmailEditCtrl.created_at + " " + $scope.adminMassEmailEditCtrl.created_by_user.first_name + " " + $scope.adminMassEmailEditCtrl.created_by_user.last_name + " (" + $scope.adminMassEmailEditCtrl.created_by_user.username + ")";

         if ($scope.adminMassEmailEditCtrl.updated_at != $scope.adminMassEmailEditCtrl.created_at)
         {
            this.show2 = true;
            if ($scope.adminMassEmailEditCtrl.updated_by_user)
            {
               this.display2 = $scope.adminMassEmailEditCtrl.updated_at + " " + $scope.adminMassEmailEditCtrl.updated_by_user.first_name + " " + $scope.adminMassEmailEditCtrl.updated_by_user.last_name + " (" + $scope.adminMassEmailEditCtrl.updated_by_user.username + ")";
            }
            else
            {
               this.display2 = $scope.adminMassEmailEditCtrl.updated_at;
            }
         }

         if ($scope.adminMassEmailEditCtrl.deleted_at)
         {
            this.show3 = true;
            this.show2 = false;
            if ($scope.adminMassEmailEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminMassEmailEditCtrl.deleted_at + " " + $scope.adminMassEmailEditCtrl.deleted_by_user.first_name + " " + $scope.adminMassEmailEditCtrl.deleted_by_user.last_name + " (" + $scope.adminMassEmailEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminMassEmailEditCtrl.deleted_at;
            }
         }

         //if ($scope.adminMassEmailEditCtrl.archived_at != null && $scope.adminMassEmailEditCtrl.archived_at != "0000-00-00 00:00:00")
         if ($scope.adminMassEmailEditCtrl.isArchived())
         {
            this.show4 = true;
            this.display4 = $scope.adminMassEmailEditCtrl.archived_at;
         }
      }
   };
   return directive;
}



