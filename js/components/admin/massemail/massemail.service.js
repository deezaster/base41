'use strict';

angular
   .module('theApp')
   .factory('AdminMassEmailService', AdminMassEmailService);


AdminMassEmailService.$inject = ['$http'];


function AdminMassEmailService($http) {

   return {

      getEmails: function () {
         return $http({method: 'GET', url: 'api/v1/admin/massemail'});
      },

      getEmailsForDropdown: function () {
         return $http({method: 'GET', url: 'api/v1/admin/massemail/getEmailsForDropdown'});
      },

      getEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/edit', data: {'id': id}});
      },

      saveEmail: function (id, subject, sender_name, sender_email, content, docs) {
         return $http({
            method: 'POST',
            url:    'api/v1/admin/massemail/save',
            data:   {'id': id, 'subject': subject, 'sender_name': sender_name, 'sender_email': sender_email, 'content': content, 'docs': docs}
         });
      },

      insertEmail: function (id, subject, sender_name, sender_email, content, docs) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/insert', data: {'subject': subject, 'sender_name': sender_name, 'sender_email': sender_email, 'content': content, 'docs': docs}});
      },

      insertRecipient: function (email, name, user_id) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/insertRecipient', data: {'email': email, 'name': name, 'user_id': user_id}});
      },

      deleteEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/delete', data: {'id': id}});
      },

      archiveEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/archive', data: {'id': id}});
      },

      unarchiveEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/unarchive', data: {'id': id}});
      },

      getRecipients: function () {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/getRecipients'});
      },

      getDocs: function () {
         return $http({method: 'POST', url: 'api/v1/admin/filemanager/getFiles'});
      },

      getUser: function () {
         return $http({method: 'GET', url: 'api/v1/auth/getUser'});
      },

      sendMassemailToUsers: function (emailId, recipients) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/sendToUsers', data: {'emailId': emailId, 'recipientIds': recipients}});
      },

      sendMassemailToMembers: function (emailId, recipients) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/sendToMembers', data: {'emailId': emailId, 'recipientIds': recipients}});
      },

      getSentLog: function (emailId) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/getSentLog', data: {'emailId': emailId}});
      },

      getSentLogRecipients: function (sentId) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/getSentLogRecipients', data: {'sentId': sentId}});
      },

      //sendEmail: function (id, to, cc, bcc) {
      //   return $http({method: 'POST', url: 'api/v1/admin/massemail/send', data: {'id': id, 'to': to, 'cc': cc, 'bcc': bcc}});
      //},

      setTo: function (id, to) {
         return $http({method: 'POST', url: 'api/v1/admin/massemail/setTo', data: {'id': id, 'to': to}});
      }
   };
}
