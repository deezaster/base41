'use strict';

angular
   .module('theApp')
   .controller('AdminMembersController', AdminMembersController)
   .controller('AdminMembersEditByIdController', AdminMembersEditByIdController)
   .controller('AdminMembersEditController', AdminMembersEditController)
   .controller('ModalSendMessageToMobileAppController', ModalSendMessageToMobileAppController)
   .controller('ModalSendEmailController', ModalSendEmailController)
   .controller('ModalShowEmailController', ModalShowEmailController);


AdminMembersController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminMemberService', 'AdminMassEmailService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminMembersEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminMemberService'];
AdminMembersEditController.$inject = ['uiGridConstants', '$scope', '$location', 'AdminMemberService', 'AdminMassEmailService', 'AdminTemplatesService', 'DataProviderService', 'AuthService', 'localStorageService', 'toastr', '$sanitize', 'ModalService', 'DIR_IMG_MEMBER', '$upload', '$timeout', '$routeParams'];
ModalSendMessageToMobileAppController.$inject = ['memberId', 'memberIds', 'deviceId', 'name', 'devicetoken', 'AdminMemberService', 'close', 'toastr'];
ModalSendEmailController.$inject = ['memberId', 'subject', 'content', 'sender_name', 'sender_email', 'recipient_email', 'AdminMemberService', 'close', 'toastr'];
ModalShowEmailController.$inject = ['data', 'close'];

function AdminMembersEditByIdController($location, DataProviderService, $routeParams, AdminMemberService) {

   var id = $routeParams.memberId;

   AdminMemberService.getMember(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("member", data);
         $location.path('/admin/member/edit');
      })
      .error(function (data) {
      });
}

function AdminMembersController(uiGridConstants, $location, $scope, AdminMemberService, AdminMassEmailService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         130,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'id', displayName: 'ID', minWidth: 50, width: 60, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'nachname', displayName: 'Nachname', minWidth: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'vorname', displayName: 'Vorname', minWidth: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'ort', displayName: 'Ort', minWidth: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'mobile', displayName: 'Mobile', minWidth: 100, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {
            name:             'portrait',
            displayName:      '',
            minWidth:         50,
            width:            50,
            enableSorting:    false,
            enableFiltering:  false,
            enableColumnMenu: false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span><img ng-src="{{COL_FIELD}}" class="portrait"></span></div>'
         },
         {name: 'email', displayName: 'Email', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'gebdatum', displayName: 'Gebdat', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      },
      appScopeProvider:    {


         // Editieren
         onClick:  function (row) {
            $location.path('/admin/member/edit/' + row.entity.id);
         },

         // Löschen
         onDelete: function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "Benutzer löschen?",
                  msg1:  "Soll das folgende Mitglied wirklich gelöscht werden?",
                  msg2:  row.entity.vorname + " " + row.entity.nachname
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminMemberService.deleteMember(row.entity.id)
                        .success(function (data) {
                           DataProviderService.clear();

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target:        data["response"]["container"],
                              timeOut:       parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminMemberService.getMembers()
                              .success(function (data) {

                                 $scope.gridOptions.data = data;
                              });
                        });

                  }

               });
            });


         }
      }
   };


   $scope.gridOptions.enableRowSelection = true;
   $scope.gridOptions.enableSelectAll = true;
   $scope.gridOptions.multiSelect = true;
   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');


   vm.massemails = [];
   vm.dropdown = [];

   AdminMemberService.getMembers()
      .success(function (data) {

         $scope.gridOptions.data = data;

         AdminMassEmailService.getEmailsForDropdown()
            .success(function (data) {

               vm.massemails = data;

            });
      });


   vm.newMember = function () {
      DataProviderService.clear();
      $location.path('/admin/member/edit');
   };

   vm.hasMemberSelected = function () {

      var recipients = $scope.gridApi.selection.getSelectedRows();
      if (recipients.length > 0)
      {
         return true;
      }
      return false;
   };

   vm.sendMessageToMobile = function () {

      var recipients = $scope.gridApi.selection.getSelectedRows();
      var recipientsId = [];

      recipients.forEach(function (item) {
         recipientsId.push(item["id"]);
      });

      ModalService.showModal({
         templateUrl:  "views/shared/modal_sendmessagetomobileapp.html",
         controller:   "ModalSendMessageToMobileAppController",
         controllerAs: "sendMessageToMobileAppController",
         inputs:       {
            memberId:    -1,
            memberIds:   recipientsId,
            deviceId:    -1,
            name:        -1,
            devicetoken: -1
         }
      }).then(function (modal) {
         modal.element.modal();
         modal.close.then(function (result) {


         });
      });
   };

   vm.sendMassemail = function (emailId) {

      var recipients = $scope.gridApi.selection.getSelectedRows();
      var recipientsId = [];

      recipients.forEach(function (item) {
         recipientsId.push(item["id"]);
      });

      AdminMassEmailService.sendMassemailToMembers(
         emailId,
         recipientsId)

         .success(function (data) {

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target:        data["response"]["container"],
               timeOut:       parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         });
   }

}

function AdminMembersEditController(uiGridConstants, $scope, $location, AdminMemberService, AdminMassEmailService, AdminTemplatesService, DataProviderService, AuthService, localStorageService, toastr, $sanitize, ModalService, DIR_IMG_MEMBER, $upload, $timeout, $routeParams) {

   var vm = this;

   var member = DataProviderService.get("member");

   var countSentLog = 0;

   // Geschlecht
   vm.sex = undefined;

   vm.sexEnums = [
      {value: 'm', label: 'Mann'},
      {value: 'w', label: 'Frau'}
   ];


   if (member)
   {
      // Mutation
      vm.id = member.id;
      vm.vorname = member.vorname;
      vm.nachname = member.nachname;
      vm.email = member.email;
      vm.mobile = member.mobile;
      vm.sex = member.sex;
      vm.gebdatum = member.gebdatum;
      vm.portrait = member.portrait;
      vm.isDefaultPortrait = member.isDefaultPortrait;
      vm.adresse1 = member.adresse1;
      vm.adresse2 = member.adresse2;
      vm.plz = member.plz;
      vm.ort = member.ort;
      vm.land = member.land;
      vm.tel = member.tel;
      vm.fax = member.fax;

      vm.firma = member.firma;
      vm.position = member.position;
      vm.beruf = member.beruf;
      vm.firma_adresse1 = member.firma_adresse1;
      vm.firma_adresse2 = member.firma_adresse2;
      vm.firma_plz = member.firma_plz;
      vm.firma_ort = member.firma_ort;
      vm.firma_land = member.firma_land;
      vm.firma_tel = member.firma_tel;
      vm.firma_fax = member.firma_fax;

      vm.login_username = member.login_username;
      vm.password = member.login_password;

      vm.created_at = member.created_at;
      vm.updated_at = member.updated_at;
      vm.deleted_at = member.deleted_at;
      vm.created_by_user = member.created_by_user;
      vm.updated_by_user = member.updated_by_user;
      vm.deleted_by_user = member.deleted_by_user;

      AdminMassEmailService.getEmailsForDropdown()
         .success(function (data) {
            vm.massemails = data;
         });
   }
   else
   {
      // Neuerfassung
      vm.id = -1;
      vm.password = "";

   }

   $scope.gridDevices = {
      enableFiltering:     false,
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         40,
            width:            90,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'name', displayName: 'Gerät', enableFiltering: false, minWidth: 100, width: 200, enableColumnMenu: false},
         {name: 'devicetoken', displayName: 'Token', enableFiltering: false, minWidth: 100, width: 500, enableColumnMenu: false}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      },
      appScopeProvider:    {

         onSend: function (row) {

            ModalService.showModal({
               templateUrl:  "views/shared/modal_sendmessagetomobileapp.html",
               controller:   "ModalSendMessageToMobileAppController",
               controllerAs: "sendMessageToMobileAppController",
               inputs:       {
                  memberId:    vm.id,
                  memberIds:   -1,
                  deviceId:    row.entity.id,
                  name:        row.entity.name,
                  devicetoken: row.entity.devicetoken
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {

                  AdminMemberService.getDevices(vm.id)
                     .success(function (data) {

                        $scope.gridDevices.data = data;
                     });

               });
            });

         },

         onSentLog: function (row) {

            vm.devicename = row.entity.name;

            AdminMemberService.getSentLog(row.entity.id)
               .success(function (data) {

                  $scope.gridSentLog.data = data;
                  countSentLog = data.length;
               });

         }

      }
   };

   $scope.gridDevices.enableFiltering = false;
   $scope.gridDevices.enableRowSelection = false;
   $scope.gridDevices.enableSelectAll = false;
   $scope.gridDevices.multiSelect = false;
   $scope.gridDevices.enableColumnResizing = true;
   $scope.gridDevices.enableFiltering = true;
   $scope.gridDevices.useExternalFiltering = false;
   $scope.gridDevices.enableGridMenu = false;
   $scope.gridDevices.showGridFooter = false;
   $scope.gridDevices.showColumnFooter = false;
   $scope.gridDevices.rowHeight = 36;


   AdminMemberService.getDevices(vm.id)
      .success(function (data) {

         $scope.gridDevices.data = data;
      });


   $scope.gridSentLog = {
      enableFiltering:     false,
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name: 'created_at', displayName: 'Gesendet', enableFiltering: false, minWidth: 100, width: 200, enableColumnMenu: false,
            sort: {
               direction: uiGridConstants.DESC,
               priority:  0
            }
         },
         {name: 'message', displayName: 'Nachricht', enableFiltering: false, minWidth: 100, enableColumnMenu: false}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      }
   };

   $scope.gridSentLog.enableFiltering = false;
   $scope.gridSentLog.enableRowSelection = false;
   $scope.gridSentLog.enableSelectAll = false;
   $scope.gridSentLog.multiSelect = false;
   $scope.gridSentLog.enableColumnResizing = true;
   $scope.gridSentLog.enableFiltering = true;
   $scope.gridSentLog.useExternalFiltering = false;
   $scope.gridSentLog.enableGridMenu = false;
   $scope.gridSentLog.showGridFooter = false;
   $scope.gridSentLog.showColumnFooter = false;
   $scope.gridSentLog.rowHeight = 36;


   $scope.gridEmails = {
      enableFiltering:     false,
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         40,
            width:            90,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {
            name: 'sent_at', displayName: 'Gesendet', enableFiltering: false, minWidth: 100, width: 180, enableColumnMenu: false,
            sort: {
               direction: uiGridConstants.DESC,
               priority:  0
            }
         },
         {name: 'subject', displayName: 'Betreff', enableFiltering: false, minWidth: 100, width: 300, enableColumnMenu: false},
         {name: 'email_type_display', displayName: 'Email Art', enableFiltering: false, minWidth: 100, width: 150, enableColumnMenu: false},
         {
            name:             'attachmentsIcon',
            displayName:      'Anhang',
            minWidth:         65,
            width:            65,
            enableSorting:    false,
            enableFiltering:  false,
            enableColumnMenu: false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'sender', displayName: 'Absender', enableFiltering: false, enableColumnMenu: false}

      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      },
      appScopeProvider:    {

         onShow: function (row) {

            if (row.entity.email_type == "email")
            {
               AdminMemberService.getEmail(row.entity.id)
                  .success(function (data) {

                     ModalService.showModal({
                        templateUrl:  "views/shared/modal_showemail.html",
                        controller:   "ModalShowEmailController",
                        controllerAs: "showEmailController",
                        inputs:       {
                           data: data
                        }
                     }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {

                        });
                     });
                  });
            }
            else
            {
               AdminMemberService.getMassemailBySentId(row.entity.id)
                  .success(function (data) {

                     ModalService.showModal({
                        templateUrl:  "views/shared/modal_showemail.html",
                        controller:   "ModalShowEmailController",
                        controllerAs: "showEmailController",
                        inputs:       {
                           data: data
                        }
                     }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {

                        });
                     });
                  });
            }
         }
      }
   };

   $scope.gridEmails.enableFiltering = false;
   $scope.gridEmails.enableRowSelection = false;
   $scope.gridEmails.enableSelectAll = false;
   $scope.gridEmails.multiSelect = false;
   $scope.gridEmails.enableColumnResizing = true;
   $scope.gridEmails.enableFiltering = true;
   $scope.gridEmails.useExternalFiltering = false;
   $scope.gridEmails.enableGridMenu = false;
   $scope.gridEmails.showGridFooter = false;
   $scope.gridEmails.showColumnFooter = false;
   $scope.gridEmails.rowHeight = 36;


   AdminMemberService.getEmailsByMemberId(vm.id)
      .success(function (data) {

         $scope.gridEmails.data = data;
      });

   vm.createData = function () {
      return vm.id == -1;
   };

   vm.editData = function () {
      return !(vm.id == -1);
   };

   vm.showSentLog = function () {
      return (countSentLog > 0);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   moment.locale('en');
   var gebdatumMoment = moment(vm.gebdatum);
   vm.gebdatumForm = gebdatumMoment.valueOf();


   vm.submitForm = function (isValid) {

      var gebdatumMoment = moment(vm.gebdatumForm)
      vm.gebdatum = gebdatumMoment.format("YYYY-MM-DD");

      if (isValid)
      {
         if (vm.id != -1)
         {
            AdminMemberService.saveMember(
               $sanitize(vm.id),
               $sanitize(vm.nachname),
               $sanitize(vm.vorname),
               $sanitize(vm.email),
               $sanitize(vm.sex),
               $sanitize(vm.gebdatum),
               $sanitize(vm.adresse1),
               $sanitize(vm.adresse2),
               $sanitize(vm.plz),
               $sanitize(vm.ort),
               $sanitize(vm.land),
               $sanitize(vm.mobile),
               $sanitize(vm.tel),
               $sanitize(vm.fax),
               $sanitize(vm.firma),
               $sanitize(vm.position),
               $sanitize(vm.beruf),
               $sanitize(vm.firma_adresse1),
               $sanitize(vm.firma_adresse2),
               $sanitize(vm.firma_plz),
               $sanitize(vm.firma_ort),
               $sanitize(vm.firma_land),
               $sanitize(vm.firma_tel),
               $sanitize(vm.firma_fax),
               $sanitize(vm.login_username),
               $sanitize(vm.password))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }
         else
         {
            AdminMemberService.insertMember(
               $sanitize(vm.nachname),
               $sanitize(vm.vorname),
               $sanitize(vm.email),
               $sanitize(vm.sex),
               $sanitize(vm.gebdatum),
               $sanitize(vm.adresse1),
               $sanitize(vm.adresse2),
               $sanitize(vm.plz),
               $sanitize(vm.ort),
               $sanitize(vm.land),
               $sanitize(vm.mobile),
               $sanitize(vm.tel),
               $sanitize(vm.fax),
               $sanitize(vm.firma),
               $sanitize(vm.position),
               $sanitize(vm.beruf),
               $sanitize(vm.firma_adresse1),
               $sanitize(vm.firma_adresse2),
               $sanitize(vm.firma_plz),
               $sanitize(vm.firma_ort),
               $sanitize(vm.firma_land),
               $sanitize(vm.firma_tel),
               $sanitize(vm.firma_fax),
               $sanitize(vm.login_username),
               $sanitize(vm.password))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }

         $location.path('/admin/members');

      }

   };

   vm.cancel = function () {
      $location.path('/admin/members');
   };

   vm.delete = function () {

      // Sicherheitsabfrage
      ModalService.showModal({
         templateUrl: "views/shared/modal_yesno.html",
         controller:  "ModalController",
         inputs:      {
            title: "Mitglied löschen?",
            msg1:  "Soll das folgende Mitglied wirklich gelöscht werden?",
            msg2:  vm.vorname + " " + vm.nachname
         }
      }).then(function (modal) {
         modal.element.modal();
         modal.close.then(function (result) {

            if (result)
            {
               AdminMemberService.deleteMember(vm.id)
                  .success(function (data) {
                     DataProviderService.clear();

                     toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                        target:        data["response"]["container"],
                        timeOut:       parseInt(data["response"]["duration"]),
                        positionClass: data["response"]["placement"]
                     });

                     $location.path('/admin/members');

                  });
            }

         });
      });

   };

   vm.sendEmailLoginCredentials = function () {

      AdminTemplatesService.getEmailCompiled("Mitglied.Zugangsdaten", vm.id)
         .success(function (data) {

            ModalService.showModal({
               templateUrl:  "views/shared/modal_sendemail.html",
               controller:   "ModalSendEmailController",
               controllerAs: "sendEmailController",
               inputs:       {
                  memberId:        vm.id,
                  subject:         data.subject,
                  content:         data.content,
                  sender_name:     localStorageService.get("userDisplay"),
                  sender_email:    localStorageService.get("userEmail"),
                  recipient_email: vm.email
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {

                  AdminMemberService.getEmailsByMemberId(vm.id)
                     .success(function (data) {

                        $scope.gridEmails.data = data;
                     });

               });
            });
         })
         .error(function (data) {
         });


   };

   vm.sendMassEmail = function (emailId) {

      var recipientsId = [];
      recipientsId.push(vm.id);

      AdminMassEmailService.sendMassemailToMembers(
         emailId,
         recipientsId)

         .success(function (data) {

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target:        data["response"]["container"],
               timeOut:       parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         });

   };


   vm.hasNoPortrait = function () {
      return vm.isDefaultPortrait;
   };

   vm.upload = function (files) {

      if (files && files.length)
      {
         for (var i = 0; i < files.length; i++)
         {
            var file = files[i];

            $upload.upload({
               url:    'api/v1/admin/members/uploadPortrait',
               fields: {'id': vm.id},
               file:   file
            }).progress(function (evt) {
               //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
               //console.log('progress ' + evt.config.file.name + ': ' + file.progress + '% ');
            }).success(function (data, status, headers, config) {
               //console.log('file ' + config.file.name + ' uploaded. Response: ' + data);

               // Portrait-Bild (neu) aufbereiten
               var random = Math.floor(Math.random() * 10000);
               vm.portrait = DIR_IMG_MEMBER + vm.id + ".jpg?v=" + random;
               vm.isDefaultPortrait = false;

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target:        data["response"]["container"],
                  timeOut:       parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

               vm.remove();
            });
         }
      }

   };

   // Portrait löschen
   vm.deletePortrait = function () {

      AdminMemberService.deletePortrait(vm.id)
         .success(function (data) {

            // Portrait-Bild (neu) aufbereiten
            var imgName = "male.jpg";
            if (vm.sex == 'm')
            {
               imgName = "male.jpg";
            }
            else
            {
               imgName = "female.jpg";
            }

            var random = Math.floor(Math.random() * 10000);
            vm.portrait = DIR_IMG_MEMBER + imgName + "?v=" + random;
            vm.isDefaultPortrait = true;

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target:        data["response"]["container"],
               timeOut:       parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         })
         .error(function (data) {
         });
   };

   vm.remove = function () {
      // clear array
      vm.picFile = [];
   };

   vm.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

   vm.generateThumb = function (file) {
      if (file != null)
      {
         if (vm.fileReaderSupported && file.type.indexOf('image') > -1)
         {
            $timeout(function () {
               var fileReader = new FileReader();
               fileReader.readAsDataURL(file);
               fileReader.onload = function (e) {
                  $timeout(function () {
                     file.dataUrl = e.target.result;
                  });
               }
            });
         }
      }
   };


}


function ModalSendMessageToMobileAppController(memberId, memberIds, deviceId, name, devicetoken, AdminMemberService, close, toastr) {

   var vm = this;
   vm.id = memberId;
   vm.ids = memberIds;
   vm.deviceId = deviceId;
   vm.name = name;
   vm.devicetoken = devicetoken;
   vm.message = "";

   // Zertifikat
   vm.certificate = 'prod';

   vm.certificateEnums = [
      {value: 'prod', label: 'Produktiv'},
      {value: 'dev', label: 'Test'}
   ];

   // BadgeIcon
   vm.badgeicon = '0';

   vm.badgeiconEnums = [
      {value: '0', label: 'Nein'},
      {value: '1', label: '1'},
      {value: '2', label: '2'},
      {value: '3', label: '3'},
      {value: '4', label: '4'},
      {value: '5', label: '5'}
   ];

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         if (vm.id == -1)
         {
            AdminMemberService.sendNotificationToSelectedMembers(
               vm.ids,
               vm.certificate,
               vm.badgeicon,
               vm.message)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }
         else
         {
            AdminMemberService.sendNotification(
               vm.id,
               vm.deviceId,
               vm.certificate,
               vm.badgeicon,
               vm.message)

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }

         vm.close();
      }

   };

   vm.close = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };


   vm.cancel = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };

}

function ModalSendEmailController(memberId, subject, content, sender_name, sender_email, recipient_email, AdminMemberService, close, toastr) {

   var vm = this;
   vm.id = memberId;
   vm.subject = subject;
   vm.content = content;
   vm.sender_name = sender_name;
   vm.sender_email = sender_email;
   vm.recipient_email = recipient_email;

   vm.htmlcontent = content;
   vm.disabled = false;

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AdminMemberService.sendEmailLoginCredentials(
            vm.id, vm.subject, vm.htmlcontent, vm.recipient_email, vm.sender_email, vm.sender_name)

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target:        data["response"]["container"],
                  timeOut:       parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

            });

         vm.close();
      }

   };

   vm.close = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };


   vm.cancel = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };

}

function ModalShowEmailController(data, close) {

   var vm = this;
   vm.data = data;
   vm.disabled = false;

   // split recipients
   var result = '';
   var recipients = vm.data.recipient.split('#');
   if (recipients.length > 1)
   {
      for (var i = 0; i < recipients.length - 1; i++)
      {
         result += recipients[i];
         if (i < recipients.length - 2)
         {
            result += '<br />';
         }
      }
   }
   else
   {
      result = vm.data.recipient
   }
   vm.data.recipients_display = result;

   // split attachments
   var result = '';
   var attachments = vm.data.files.split('#');
   if (attachments.length > 1)
   {
      for (var i = 0; i < attachments.length - 1; i++)
      {
         result += attachments[i];
         if (i < attachments.length - 2)
         {
            result += '<br />';
         }
      }
   }
   else
   {
      result = vm.data.files
   }

   vm.data.attachments_display = result;

   vm.close = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };


   vm.cancel = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };

}








