'use strict';

angular
   .module('theApp')
   .directive('x3mMemberTitleNavbar', x3mMemberTitleNavbar)
   .directive('x3mMemberTitlePanel', x3mMemberTitlePanel)
   .directive('x3mMemberSubtitlePanel', x3mMemberSubtitlePanel)
   .directive('x3mMemberInfoPanel', x3mMemberInfoPanel)
   .directive('x3mMemberLogTitlePanel', x3mMemberLogTitlePanel);


// Aufruf: <span x3m-member-title-navbar></span>
function x3mMemberTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminMemberEditCtrl.id != -1)
         {
            this.display = $scope.adminMemberEditCtrl.vorname + " " + $scope.adminMemberEditCtrl.nachname;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}

// Aufruf: <span x3m-member-title-panel></span>
function x3mMemberTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminMemberEditCtrl.id != -1)
         {
            this.display1 = $scope.adminMemberEditCtrl.vorname + " " + $scope.adminMemberEditCtrl.nachname;
         }
         else
         {
            this.username = "";
            this.display1 = "NEUES MITGLIED ERFASSEN";
         }

      }
   };
   return directive;
}

// Aufruf: <span x3m-member-subtitle-panel></span>
function x3mMemberSubtitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span class="text-muted"><i class="fa fa-envelope-o" style="font-size: 15px;"></i> {{ ctrlPanelSubtitle.display2 }} &nbsp;&nbsp;&nbsp; <i class="ion-pound" style="font-size: 17px;"></i> {{ ctrlPanelSubtitle.display3 }}</span>',
      controllerAs: 'ctrlPanelSubtitle',
      controller:   function ($scope) {

         if ($scope.adminMemberEditCtrl.id != -1)
         {
            this.display2 = $scope.adminMemberEditCtrl.email;
            this.display3 = $scope.adminMemberEditCtrl.id;
         }
         else
         {
            this.display2 = "";
            this.display3 = "";
         }

      }
   };
   return directive;
}


// Aufruf: <span x3m-member-info-panel></span>
function x3mMemberInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";

         this.show2 = false;
         this.show3 = false;

         this.display1 = $scope.adminMemberEditCtrl.created_at + " " + $scope.adminMemberEditCtrl.created_by_user.first_name + " " + $scope.adminMemberEditCtrl.created_by_user.last_name + " (" + $scope.adminMemberEditCtrl.created_by_user.username + ")";

         if ($scope.adminMemberEditCtrl.updated_at != $scope.adminMemberEditCtrl.created_at)
         {
            this.show2 = true;
            if ($scope.adminMemberEditCtrl.updated_by_user)
            {
               this.display2 = $scope.adminMemberEditCtrl.updated_at + " " + $scope.adminMemberEditCtrl.updated_by_user.first_name + " " + $scope.adminMemberEditCtrl.updated_by_user.last_name + " (" + $scope.adminMemberEditCtrl.updated_by_user.username + ")";
            }
            else
            {
               this.display2 = $scope.adminMemberEditCtrl.updated_at;
            }
         }

         if ($scope.adminMemberEditCtrl.deleted_at)
         {
            this.show3 = true;
            this.show2 = false;
            if ($scope.adminMemberEditCtrl.deleted_by_user)
            {
               this.display3 = $scope.adminMemberEditCtrl.deleted_at + " " + $scope.adminMemberEditCtrl.deleted_by_user.first_name + " " + $scope.adminMemberEditCtrl.deleted_by_user.last_name + " (" + $scope.adminMemberEditCtrl.deleted_by_user.username + ")";
            }
            else
            {
               this.display3 = $scope.adminMemberEditCtrl.deleted_at;
            }
         }

      }
   };
   return directive;
}

function x3mMemberLogTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         this.display1 = $scope.adminMemberEditCtrl.devicename;
      }
   };
   return directive;
}