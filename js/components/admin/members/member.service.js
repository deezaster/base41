'use strict';

angular
   .module('theApp')
   .factory('AdminMemberService', AdminMemberService);


AdminMemberService.$inject = ['$http'];


function AdminMemberService($http) {

   return {

      getMembers: function () {
         return $http({method: 'GET', url: 'api/v1/admin/members'});
      },

      getMember: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/members/edit', data: {'id': id}});
      },

      getDevices: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/members/getDevices', data: {'id': id}});
      },

      getEmailsByMemberId: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getEmailsByMemberId', data: {'id': id}});
      },

      getEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getEmail', data: {'id': id}});
      },

      getMassemailBySentId: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getMassemailBySentId', data: {'id': id}});
      },

      sendNotification: function (id, deviceId, certificate, badgeicon, message) {
         return $http({method: 'POST', url: 'api/v1/admin/members/sendNotification', data: {'id': id, 'deviceId': deviceId, 'certificate': certificate, 'badgeicon': badgeicon, 'message': message}});
      },

      sendNotificationToSelectedMembers: function (recipients, certificate, badgeicon, message) {
         return $http({method: 'POST', url: 'api/v1/admin/members/sendNotificationToSelectedMembers', data: {'recipientIds': recipients, 'certificate': certificate, 'badgeicon': badgeicon, 'message': message}});
      },

      getSentLog: function (deviceId) {
         return $http({method: 'POST', url: 'api/v1/admin/members/getSentLog', data: {'deviceId': deviceId}});
      },

      saveMember: function (id, nachname, vorname, email, sex, gebdatum, adresse1, adresse2, plz, ort, land, mobile, tel, fax, firma, position, beruf, firma_adresse1, firma_adresse2, firma_plz, firma_ort, firma_land, firma_tel, firma_fax, username, password) {
         return $http({method: 'POST', url: 'api/v1/admin/members/save', data: {'id': id, 'nachname': nachname, 'vorname': vorname, 'email': email, 'sex': sex, 'gebdatum': gebdatum, 'adresse1': adresse1, 'adresse2': adresse2, 'plz': plz, 'ort': ort, 'land': land, 'mobile': mobile, 'tel': tel, 'fax': fax, 'firma': firma, 'position': position, 'beruf': beruf, 'firma_adresse1': firma_adresse1, 'firma_adresse2': firma_adresse2, 'firma_plz': firma_plz, 'firma_ort': firma_ort, 'firma_land': firma_land, 'firma_tel': firma_tel, 'firma_fax': firma_fax, 'login_username': username, 'login_password': password}});
      },

      insertMember: function (nachname, vorname, email, sex, gebdatum, adresse1, adresse2, plz, ort, land, mobile, tel, fax, firma, position, beruf, firma_adresse1, firma_adresse2, firma_plz, firma_ort, firma_land, firma_tel, firma_fax, username, password) {
         return $http({method: 'POST', url: 'api/v1/admin/members/insert', data: {'nachname': nachname, 'vorname': vorname, 'email': email, 'sex': sex, 'gebdatum': gebdatum, 'adresse1': adresse1, 'adresse2': adresse2, 'plz': plz, 'ort': ort, 'land': land, 'mobile': mobile, 'tel': tel, 'fax': fax, 'firma': firma, 'position': position, 'beruf': beruf, 'firma_adresse1': firma_adresse1, 'firma_adresse2': firma_adresse2, 'firma_plz': firma_plz, 'firma_ort': firma_ort, 'firma_land': firma_land, 'firma_tel': firma_tel, 'firma_fax': firma_fax, 'login_username': username, 'login_password': password}});
      },
      
      deleteMember: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/members/delete', data: {'id': id}});
      },

      deletePortrait:function(id){
         return $http({method:'POST', url:'api/v1/admin/members/deletePortrait', data: {'id': id}});
      },

      sendEmailLoginCredentials: function (memberId, subject, content, recipient_email, sender_email, sender_name) {
         return $http({method: 'POST', url: 'api/v1/admin/email/sendEmailLoginCredentials', data: {'memberId': memberId, 'subject': subject, 'content': content, 'recipient_email': recipient_email, 'sender_email': sender_email, 'sender_name': sender_name}});
      }

   };
}
