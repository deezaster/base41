'use strict';

angular
   .module('theApp')
   .factory('AdminTemplatesService', AdminTemplatesService);


AdminTemplatesService.$inject = ['$http'];


function AdminTemplatesService($http) {

   return {

      getEmailCompiled: function (name, id) {
         return $http({method: 'POST', url: 'api/v1/admin/templates/getEmailCompiled', data: {'name': name, 'id': id}});
      }
   };
}
