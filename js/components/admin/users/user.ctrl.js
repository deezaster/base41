'use strict';

angular
   .module('theApp')
   .controller('AdminUsersController', AdminUsersController)
   .controller('AdminUsersEditByIdController', AdminUsersEditByIdController)
   .controller('AdminUsersEditController', AdminUsersEditController);

AdminUsersController.$inject = ['uiGridConstants', '$location', '$scope', 'AdminUserService', 'AdminMassEmailService', 'DataProviderService', 'i18nService', 'toastr', 'ModalService'];
AdminUsersEditByIdController.$inject = ['$location', 'DataProviderService', '$routeParams', 'AdminUserService'];
AdminUsersEditController.$inject = ['$rootScope', '$scope', '$location', 'uiGridConstants', 'AdminUserService', 'AdminMassEmailService', 'DataProviderService', 'AuthService', 'localStorageService', 'toastr', '$sanitize', 'ModalService', 'DIR_IMG_USER', '$upload', '$timeout'];


function AdminUsersEditByIdController($location, DataProviderService, $routeParams, AdminUserService) {

   var id = $routeParams.userId;

   AdminUserService.getUser(id)
      .success(function (data) {
         DataProviderService.clear();
         DataProviderService.add("user", data);
         $location.path('/admin/user/edit');
      })
      .error(function (data) {
      });
}

function AdminUsersController(uiGridConstants, $location, $scope, AdminUserService, AdminMassEmailService, DataProviderService, i18nService, toastr, ModalService) {

   var vm = this;

   $scope.gridOptions = {
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         130,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {
            name:             'username',
            displayName:      'Benutzername',
            minWidth:         150,
            enableColumnMenu: false,
            filters:          [{condition: uiGridConstants.filter.CONTAINS}],
            cellClass:        function (grid, row, col, rowRenderIndex, colRenderIndex) {


               // wen banned, dann kennzeichnen
               var colBanned = grid.columns[10]; // 10 = banned
               if (grid.getCellValue(row, colBanned) === true)
               {
                  return 'text-warning bg-warning';
               }

               // wen nicht aktiviert, dann kennzeichnen
               var colActivated = grid.columns[11]; // 11 = activated
               if (grid.getCellValue(row, colActivated) === false)
               {
                  return 'text-info bg-info';
               }


               return '';

            }
         },
         {name: 'id', displayName: 'ID', minWidth: 50, width: 60, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'last_name', displayName: 'Nachname', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'first_name', displayName: 'Vorname', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {
            name:             'portrait',
            displayName:      '',
            minWidth:         50,
            width:            50,
            enableSorting:    false,
            enableFiltering:  false,
            enableColumnMenu: false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span><img ng-src="{{COL_FIELD}}" class="portrait"></span></div>'
         },
         {name: 'email', displayName: 'Email', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'last_login', displayName: 'Letztes Login', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'usergroup', displayName: 'Benutzergruppe', minWidth: 150, enableColumnMenu: false, filters: [{condition: uiGridConstants.filter.CONTAINS}]},
         {name: 'banned', displayName: '', visible: false},
         {name: 'activated', displayName: '', visible: false}
      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;

         //gridApi.selection.on.rowSelectionChanged($scope,function(row){
         //   var msg = 'row selected ' + row.isSelected;
         //   console.log(msg);
         //});
      },
      appScopeProvider:    {


         // Editieren

         onClick:  function (row) {
            $location.path('/admin/user/edit/' + row.entity.id);
         },
         onDelete: function (row) {

            // Sicherheitsabfrage
            ModalService.showModal({
               templateUrl: "views/shared/modal_yesno.html",
               controller:  "ModalController",
               inputs:      {
                  title: "Benutzer löschen?",
                  msg1:  "Soll der folgende Benutzer wirklich gelöscht werden?",
                  msg2:  row.entity.first_name + " " + row.entity.last_name
               }
            }).then(function (modal) {
               modal.element.modal();
               modal.close.then(function (result) {
                  if (result)
                  {
                     AdminUserService.deleteUser(row.entity.id)
                        .success(function (data) {
                           DataProviderService.clear();

                           toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                              target:        data["response"]["container"],
                              timeOut:       parseInt(data["response"]["duration"]),
                              positionClass: data["response"]["placement"]
                           });

                           AdminUserService.getUsers()
                              .success(function (data) {

                                 $scope.gridOptions.data = data;
                              });
                        });

                  }

               });
            });


         },

         onBan: function (row) {

            AdminUserService.banUser(row.entity.id)
               .success(function (data) {
                  DataProviderService.clear();

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  AdminUserService.getUsers()
                     .success(function (data) {

                        $scope.gridOptions.data = data;
                     });

               });
         },

         onUnban: function (row) {

            AdminUserService.unbanUser(row.entity.id)
               .success(function (data) {
                  DataProviderService.clear();

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  AdminUserService.getUsers()
                     .success(function (data) {

                        $scope.gridOptions.data = data;
                     });

               });
         },

         onActivate: function (row) {

            AdminUserService.activateUser(row.entity.id)
               .success(function (data) {
                  DataProviderService.clear();

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

                  AdminUserService.getUsers()
                     .success(function (data) {

                        $scope.gridOptions.data = data;
                     });

               });
         }
      }
   };


   $scope.gridOptions.enableRowSelection = true;
   $scope.gridOptions.enableSelectAll = true;
   $scope.gridOptions.multiSelect = true;
   $scope.gridOptions.enableColumnResizing = true;
   $scope.gridOptions.enableFiltering = true;
   $scope.gridOptions.enableGridMenu = false;
   $scope.gridOptions.showGridFooter = true;
   $scope.gridOptions.showColumnFooter = false;
   $scope.gridOptions.rowHeight = 36;

   i18nService.setCurrentLang('de');


   vm.massemails = [];
   vm.dropdown = [];

   AdminUserService.getUsers()
      .success(function (data) {

         $scope.gridOptions.data = data;

         AdminMassEmailService.getEmailsForDropdown()
            .success(function (data) {

               vm.massemails = data;

            });

      });


   vm.newUser = function () {
      DataProviderService.clear();
      $location.path('/admin/user/edit');
   };

   vm.hasUserSelected = function () {

      var recipients = $scope.gridApi.selection.getSelectedRows();
      if (recipients.length > 0)
      {
         return true;
      }
      return false;
   };

   vm.sendMassemail = function (emailId) {

      var recipients = $scope.gridApi.selection.getSelectedRows();
      var recipientsId = [];

      recipients.forEach(function (item) {
         recipientsId.push(item["id"]);
      });

      AdminMassEmailService.sendMassemailToUsers(
         emailId,
         recipientsId)

         .success(function (data) {

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target:        data["response"]["container"],
               timeOut:       parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         });
   }

}

function AdminUsersEditController($rootScope, $scope, $location, uiGridConstants, AdminUserService, AdminMassEmailService, DataProviderService, AuthService, localStorageService, toastr, $sanitize, ModalService, DIR_IMG_USER, $upload, $timeout) {

   var vm = this;

   var user = DataProviderService.get("user");

   // Geschlecht
   vm.sex = undefined;

   vm.sexEnums = [
      {value: 'm', label: 'Mann'},
      {value: 'w', label: 'Frau'}
   ];

   // Benuzergruppe
   vm.group = undefined;
   vm.groupEnums = [];

   AuthService.groups()
      .success(function (data) {
         var log = [];
         angular.forEach(data, function (value, key) {
            this.push({value: value.id, label: value.name});

            //if (value.name.toLowerCase() == 'user') {
            //   vm.group = value.id;
            //}

         }, vm.groupEnums);
      });


   if (user)
   {
      // Mutation
      vm.id = user.id;
      vm.username = user.username;
      vm.username_current = user.username;
      vm.first_name = user.first_name;
      vm.last_name = user.last_name;
      vm.email = user.email;
      vm.email_current = user.email;
      vm.sex = user.sex;
      vm.group = user.group;
      vm.portrait = user.portrait;
      vm.isDefaultPortrait = user.isDefaultPortrait;
      vm.isSystemUser = user.isSystemUser;

      vm.created_at = user.created_at;
      vm.updated_at = user.updated_at;
      vm.deleted_at = user.deleted_at;

      vm.created_by_user = user.created_by_user;
      vm.updated_by_user = user.updated_by_user;
      vm.deleted_by_user = user.deleted_by_user;

      vm.activated_at = user.activated_at;
      vm.last_login = user.last_login;

      vm.attempts = user.attempts;
      vm.suspended_at = user.suspended_at;
      if (user.suspendet_remaining > "0")
      {
         vm.suspended_at += " (noch " + user.suspendet_remaining + " Minuten)";
      }
      vm.banned_at = user.banned_at;
      vm.act_code = user.act_code;
      vm.res_pwd_code = user.res_pwd_code;

      AdminMassEmailService.getEmailsForDropdown()
         .success(function (data) {

            vm.massemails = data;
         });

   }
   else
   {
      // Neuerfassung
      vm.id = -1;

   }

   vm.createData = function () {
      return vm.id == -1;
   };

   vm.editData = function () {
      return !(vm.id == -1);
   };

   vm.isDeleted = function () {
      return !(vm.deleted_at == null);
   };

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         if (vm.id != -1)
         {
            AdminUserService.saveUser(
               $sanitize(vm.id),
               $sanitize(vm.username),
               $sanitize(vm.password),
               $sanitize(vm.last_name),
               $sanitize(vm.first_name),
               $sanitize(vm.email),
               $sanitize(vm.sex),
               $sanitize(vm.group))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }
         else
         {
            AdminUserService.insertUser(
               $sanitize(vm.username),
               $sanitize(vm.password),
               $sanitize(vm.last_name),
               $sanitize(vm.first_name),
               $sanitize(vm.email),
               $sanitize(vm.sex),
               $sanitize(vm.group))

               .success(function (data) {

                  toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                     target:        data["response"]["container"],
                     timeOut:       parseInt(data["response"]["duration"]),
                     positionClass: data["response"]["placement"]
                  });

               });
         }

         $location.path('/admin/users');

      }

   };

   vm.cancel = function () {
      $location.path('/admin/users');
   };

   vm.delete = function () {

      // Sicherheitsabfrage
      ModalService.showModal({
         templateUrl: "views/shared/modal_yesno.html",
         controller:  "ModalController",
         inputs:      {
            title: "Benutzer löschen?",
            msg1:  "Soll der folgende Benutzer wirklich gelöscht werden?",
            msg2:  vm.first_name + " " + vm.last_name
         }
      }).then(function (modal) {
         modal.element.modal();
         modal.close.then(function (result) {
            if (result)
            {
               AdminUserService.deleteUser(vm.id)
                  .success(function (data) {
                     DataProviderService.clear();

                     toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                        target:        data["response"]["container"],
                        timeOut:       parseInt(data["response"]["duration"]),
                        positionClass: data["response"]["placement"]
                     });

                     $location.path('/admin/users');

                  });

            }

         });
      });

   };


   vm.sendEmail = function (emailId) {


      var recipientsId = [];
      recipientsId.push(vm.id);

      AdminMassEmailService.sendMassemailToUsers(
         emailId,
         recipientsId)

         .success(function (data) {

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target:        data["response"]["container"],
               timeOut:       parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         });

   };


   vm.isThisASystemUser = function () {
      return vm.isSystemUser;
   };

   vm.hasNoPortrait = function () {
      return vm.isDefaultPortrait;
   };

   vm.upload = function (files) {

      if (files && files.length)
      {
         for (var i = 0; i < files.length; i++)
         {
            var file = files[i];

            $upload.upload({
               url:    'api/v1/admin/users/uploadPortrait',
               fields: {'id': vm.id},
               file:   file
            }).progress(function (evt) {
               //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
               //console.log('progress ' + evt.config.file.name + ': ' + file.progress + '% ');
            }).success(function (data, status, headers, config) {
               //console.log('file ' + config.file.name + ' uploaded. Response: ' + data);

               // Portrait-Bild (neu) aufbereiten
               var random = Math.floor(Math.random() * 10000);
               vm.portrait = DIR_IMG_USER + vm.id + ".jpg?v=" + random;
               vm.isDefaultPortrait = false;

               // Mein eigenes Profil? Dann in der Navigationbar anpassen
               if (localStorageService.get("userId") == vm.id)
               {
                  $rootScope.UserPortrait = {imageSrc: DIR_IMG_USER + vm.id + ".jpg"};
                  var random = Math.floor(Math.random() * 10000);
                  $rootScope.UserPortrait.random = "?v=" + random;
               }

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target:        data["response"]["container"],
                  timeOut:       parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

               vm.remove();
            });
         }
      }

   };

   // Portrait löschen
   vm.deletePortrait = function () {

      AdminUserService.deletePortrait(vm.id)
         .success(function (data) {

            // Portrait-Bild (neu) aufbereiten
            var imgName = "male.jpg";
            if (vm.sex == 'm')
            {
               imgName = "male.jpg";
            }
            else
            {
               imgName = "female.jpg";
            }

            var random = Math.floor(Math.random() * 10000);
            vm.portrait = DIR_IMG_USER + imgName + "?v=" + random;
            vm.isDefaultPortrait = true;

            // Mein eigenes Profil? Dann in der Navigationbar anpassen
            if (localStorageService.get("userId") == vm.id)
            {
               $rootScope.UserPortrait = {imageSrc: DIR_IMG_USER + imgName};
               var random = Math.floor(Math.random() * 10000);
               $rootScope.UserPortrait.random = "?v=" + random;
            }

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target:        data["response"]["container"],
               timeOut:       parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         })
         .error(function (data) {
         });
   };

   vm.remove = function () {
      console.log("REMOVE");
      // clear array
      vm.picFile = [];
   };

   vm.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

   vm.generateThumb = function (file) {
      if (file != null)
      {
         if (vm.fileReaderSupported && file.type.indexOf('image') > -1)
         {
            $timeout(function () {
               var fileReader = new FileReader();
               fileReader.readAsDataURL(file);
               fileReader.onload = function (e) {
                  $timeout(function () {
                     file.dataUrl = e.target.result;
                  });
               }
            });
         }
      }
   };


   $scope.gridEmails = {
      enableFiltering:     false,
      paginationPageSizes: [10, 50, 100],
      paginationPageSize:  10,
      columnDefs:          [
         {
            name:             'fnc',
            displayName:      'Funktionen',
            enableColumnMenu: false,
            enableHiding:     false,
            enablePinning:    false,
            pinnedLeft:       false,
            enableFiltering:  false,
            enableSorting:    false,
            minWidth:         40,
            width:            90,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {
            name: 'sent_at', displayName: 'Gesendet', enableFiltering: false, minWidth: 100, width: 180, enableColumnMenu: false,
            sort: {
               direction: uiGridConstants.DESC,
               priority:  0
            }
         },
         {name: 'subject', displayName: 'Betreff', enableFiltering: false, minWidth: 100, width: 300, enableColumnMenu: false},
         {name: 'email_type_display', displayName: 'Email Art', enableFiltering: false, minWidth: 100, width: 150, enableColumnMenu: false},
         {
            name:             'attachmentsIcon',
            displayName:      'Anhang',
            minWidth:         65,
            width:            65,
            enableSorting:    false,
            enableFiltering:  false,
            enableColumnMenu: false,
            cellTemplate:     '<div class="ui-grid-cell-contents"><span x3m-compile-html="{{COL_FIELD}}"></span></div>'
         },
         {name: 'sender', displayName: 'Absender', enableFiltering: false, enableColumnMenu: false}

      ],
      onRegisterApi:       function (gridApi) {
         $scope.gridApi = gridApi;
      },
      appScopeProvider:    {

         onShow: function (row) {

            if (row.entity.email_type == "email")
            {
               AdminUserService.getEmail(row.entity.id)
                  .success(function (data) {

                     ModalService.showModal({
                        templateUrl:  "views/shared/modal_showemail.html",
                        controller:   "ModalShowEmailController",
                        controllerAs: "showEmailController",
                        inputs:       {
                           data: data
                        }
                     }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {

                        });
                     });
                  });
            }
            else
            {
               AdminUserService.getMassemailBySentId(row.entity.id)
                  .success(function (data) {

                     ModalService.showModal({
                        templateUrl:  "views/shared/modal_showemail.html",
                        controller:   "ModalShowEmailController",
                        controllerAs: "showEmailController",
                        inputs:       {
                           data: data
                        }
                     }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {

                        });
                     });
                  });
            }
         }
      }
   };

   $scope.gridEmails.enableFiltering = false;
   $scope.gridEmails.enableRowSelection = false;
   $scope.gridEmails.enableSelectAll = false;
   $scope.gridEmails.multiSelect = false;
   $scope.gridEmails.enableColumnResizing = true;
   $scope.gridEmails.enableFiltering = true;
   $scope.gridEmails.useExternalFiltering = false;
   $scope.gridEmails.enableGridMenu = false;
   $scope.gridEmails.showGridFooter = false;
   $scope.gridEmails.showColumnFooter = false;
   $scope.gridEmails.rowHeight = 36;


   AdminUserService.getEmailsByUserId(vm.id)
      .success(function (data) {

         $scope.gridEmails.data = data;
      });

}






