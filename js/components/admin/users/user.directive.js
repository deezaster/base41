'use strict';

angular
   .module('theApp')
   .directive('x3mUserTitleNavbar', x3mUserTitleNavbar)
   .directive('x3mUserTitlePanel', x3mUserTitlePanel)
   .directive('x3mUserSubtitlePanel', x3mUserSubtitlePanel)
   .directive('x3mUserInfoPanel', x3mUserInfoPanel);


// Aufruf: <span x3m-user-title-navbar></span>
function x3mUserTitleNavbar() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlNavbar.display }}</span>',
      controllerAs: 'ctrlNavbar',
      controller:   function ($scope) {

         if ($scope.adminUserEditCtrl.id != -1)
         {
            this.display = $scope.adminUserEditCtrl.first_name + " " + $scope.adminUserEditCtrl.last_name;
         }
         else
         {
            this.display = "ERFASSEN";
         }

      }
   };
   return directive;
}

// Aufruf: <span x3m-user-title-panel></span>
function x3mUserTitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span>{{ ctrlPanel.display1 }}</span>',
      controllerAs: 'ctrlPanel',
      controller:   function ($scope) {

         if ($scope.adminUserEditCtrl.id != -1)
         {
            this.display1 = $scope.adminUserEditCtrl.first_name + " " + $scope.adminUserEditCtrl.last_name;
         }
         else
         {
            this.username = "";
            this.display1 = "NEUEN BENUTZER ERFASSEN";
         }

      }
   };
   return directive;
}

// Aufruf: <span x3m-user-subtitle-panel></span>
function x3mUserSubtitlePanel() {

   var directive = {
      restrict:     'A',
      replace:      true,
      template:     '<span class="text-muted"><i class="glyphicon glyphicon-user" style="font-size: 15px;"></i> {{ ctrlPanelSubtitle.display1 }} &nbsp;&nbsp;&nbsp;  <i class="fa fa-envelope-o" style="font-size: 15px;"></i> {{ ctrlPanelSubtitle.display2 }} &nbsp;&nbsp;&nbsp; <i class="ion-pound" style="font-size: 17px;"></i> {{ ctrlPanelSubtitle.display3 }}</span>',
      controllerAs: 'ctrlPanelSubtitle',
      controller:   function ($scope) {

         if ($scope.adminUserEditCtrl.id != -1)
         {
            this.display1 = $scope.adminUserEditCtrl.username;
            this.display2 = $scope.adminUserEditCtrl.email;
            this.display3 = $scope.adminUserEditCtrl.id;
         }
         else
         {
            this.display1 = "";
            this.display2 = "";
            this.display3 = "";
         }

      }
   };
   return directive;
}


// Aufruf: <span x3m-user-info-panel></span>
function x3mUserInfoPanel() {

   var directive = {
      restrict:     'A',
      replace:      false,
      template:     '<span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="ERFASSUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-plus-square text-success"></i> {{ ctrlPanelInfo.display1 }}</span>'
                    + '<span ng-if="ctrlPanelInfo.show2"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTE MUTATION" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-pencil text-primary"></i> {{ ctrlPanelInfo.display2 }}</span></span>'
                    + '<span ng-if="ctrlPanelInfo.show3"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="GELÖSCHT" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-trash text-danger"></i> {{ ctrlPanelInfo.display3 }}</span></span>'
                    + '<span ng-if="ctrlPanelInfo.show4"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="AKTIVIERUNG" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-check-circle text-success"></i> {{ ctrlPanelInfo.display4 }}</span></span>'
      + '<span ng-if="ctrlPanelInfo.show5"><span style="font-size: 12px; margin-right: 20px;" tooltips="" tooltip-title="LETZTES LOGIN" tooltip-side="top" tooltip-size="small" tooltip-try="0"><i class="fa fa-sign-in text-primary"></i> {{ ctrlPanelInfo.display5 }}</span></span>',
      controllerAs: 'ctrlPanelInfo',
      controller:   function ($scope) {

         this.display2 = "";
         this.display3 = "";
         this.display4 = "";
         this.display5 = "";

         this.show2 = false;
         this.show3 = false;
         this.show4 = false;
         this.show5 = false;

         if ($scope.adminUserEditCtrl.id != -1)
         {
            this.display1 = $scope.adminUserEditCtrl.created_at;

            if ($scope.adminUserEditCtrl.created_by_user)
            {
               this.display1 = $scope.adminUserEditCtrl.created_at + " " + $scope.adminUserEditCtrl.created_by_user.first_name + " " + $scope.adminUserEditCtrl.created_by_user.last_name + " (" + $scope.adminUserEditCtrl.created_by_user.username + ")";
            }


            if ($scope.adminUserEditCtrl.updated_at)
            {
               this.show2 = true;
               this.display2 = $scope.adminUserEditCtrl.updated_at;

               if ($scope.adminUserEditCtrl.updated_by_user)
               {
                  this.display2 = $scope.adminUserEditCtrl.updated_at + " " + $scope.adminUserEditCtrl.updated_by_user.first_name + " " + $scope.adminUserEditCtrl.updated_by_user.last_name + " (" + $scope.adminUserEditCtrl.updated_by_user.username + ")";
               }
            }

            if ($scope.adminUserEditCtrl.deleted_at)
            {
               this.show3 = true;
               this.display3 = $scope.adminUserEditCtrl.deleted_at;

               if ($scope.adminUserEditCtrl.deleted_by_user)
               {
                  this.display3 = $scope.adminUserEditCtrl.deleted_at + " " + $scope.adminUserEditCtrl.deleted_by_user.first_name + " " + $scope.adminUserEditCtrl.deleted_by_user.last_name + " (" + $scope.adminUserEditCtrl.deleted_by_user.username + ")";
               }
            }

            if ($scope.adminUserEditCtrl.activated_at)
            {
               this.show4 = true;
               this.display4 = $scope.adminUserEditCtrl.activated_at;
            }

            if ($scope.adminUserEditCtrl.last_login)
            {
               this.show5 = true;
               this.display5 = $scope.adminUserEditCtrl.last_login;
            }

         }


      }
   };
   return directive;
}