'use strict';

angular
   .module('theApp')
   .factory('AdminUserService', AdminUserService);


AdminUserService.$inject = ['$http'];


function AdminUserService($http) {

   return {

      getUsers: function () {
         return $http({method: 'GET', url: 'api/v1/admin/users'});
      },

      getUser: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/users/edit', data: {'id': id}});
      },

      saveUser: function (id, username, password, last_name, first_name, email, sex, group) {
         return $http({method: 'POST', url: 'api/v1/admin/users/save', data: {'id': id, 'username': username, 'password': password, 'last_name': last_name, 'first_name': first_name, 'email': email, 'sex': sex, 'group': group}});
      },

      insertUser: function (username, password, last_name, first_name, email, sex, group) {
         return $http({method: 'POST', url: 'api/v1/admin/users/insert', data: {'username': username, 'password': password, 'last_name': last_name, 'first_name': first_name, 'email': email, 'sex': sex, 'group': group}});
      },
      deleteUser: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/users/delete', data: {'id': id}});
      },
      banUser: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/users/ban', data: {'id': id}});
      },
      unbanUser: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/users/unban', data: {'id': id}});
      },
      activateUser: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/users/activate', data: {'id': id}});
      },
      deletePortrait:function(id){
         return $http({method:'POST', url:'api/v1/admin/users/deletePortrait', data: {'id': id}});
      },

      getEmailsByUserId: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getEmailsByUserId', data: {'id': id}});
      },

      getEmail: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getEmail', data: {'id': id}});
      },

      getMassemailBySentId: function (id) {
         return $http({method: 'POST', url: 'api/v1/admin/email/getMassemailBySentId', data: {'id': id}});
      }
   };
}
