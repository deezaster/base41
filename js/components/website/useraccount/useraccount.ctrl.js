'use strict';

angular
   .module('theApp')
   .controller('WebsiteUserAccountController', WebsiteUserAccountController);

WebsiteUserAccountController.$inject = ['DIR_IMG_USER', '$rootScope', 'toastr', '$location', '$sanitize', 'UserAccountService'];


function WebsiteUserAccountController(DIR_IMG_USER, $rootScope, toastr, $location, $sanitize, UserAccountService) {

   var vm = this;

   // Geschlecht
   vm.sex = undefined;

   vm.sexEnums = [
      {value: 'm', label: 'Mann'},
      {value: 'w', label: 'Frau'}
   ];


   UserAccountService.getUseraccount()

      .success(function (data) {

         var user = data["user"];

         vm.id = user.id;
         vm.last_name = user.last_name;
         vm.first_name = user.first_name;
         vm.username = user.username;
         vm.email = user.email;
         vm.sex = user.sex;
         vm.has_portrait = user.has_portrait;

      })
      .error(function (data) {
         // Errorhandling in: http.service.js (httpInterceptor)
      });



   vm.cancel = function()
   {
      $location.path('/');
   };

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         UserAccountService.saveUseraccount(
            $sanitize(vm.username),
            $sanitize(vm.last_name),
            $sanitize(vm.first_name),
            $sanitize(vm.email),
            $sanitize(vm.sex))

            .success(function (data) {

               // Portrait-Bild (neu) aufbereiten
               var imgName = "male.jpg";

               if (vm.has_portrait == 'y')
               {
                  imgName = vm.id + ".jpg";
               }
               else
               {
                  if (vm.sex == 'm')
                  {
                     imgName = "male.jpg";
                  }
                  else
                  {
                     imgName = "female.jpg";
                  }
               }

               $rootScope.UserPortrait = {imageSrc: DIR_IMG_USER + imgName};

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

            });
      }

   };


}









