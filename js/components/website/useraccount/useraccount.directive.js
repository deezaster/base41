angular
   .module('theApp')
   .directive('x3mPortrait', x3mPortrait)
   .directive('x3mPortraitNavbar', x3mPortraitNavbar);

x3mPortrait.$inject = ['$rootScope'];
x3mPortraitNavbar.$inject = ['$rootScope'];

function x3mPortrait($rootScope) {

   if (!$rootScope.UserPortrait)
   {
      $rootScope.UserPortrait = {imageSrc: ""};
   }

   var random =  Math.floor(Math.random() * 10000);
   $rootScope.UserPortrait.random = "?v="+random;

   var directive = {
      restrict: 'A',
      replace:  true,
      template: '<img ng-src="{{ UserPortrait.imageSrc }}{{ UserPortrait.random}}" alt="" class="thumb img-thumbnail" />'
   };
   return directive;

};

function x3mPortraitNavbar($rootScope) {

   if (!$rootScope.UserPortrait)
   {
      $rootScope.UserPortrait = {imageSrc: ""};
   }

   var random =  Math.floor(Math.random() * 10000);
   $rootScope.UserPortrait.random = "?v="+random;


   var directive = {
      restrict: 'A',
      replace:  true,
      template: '<span class="portrait" style="background-image: url( {{ UserPortrait.imageSrc }}{{ UserPortrait.random}} ); "></span>'
   };
   return directive;

};
