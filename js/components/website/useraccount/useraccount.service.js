'use strict';

angular
   .module('theApp')
   .factory('UserAccountService', UserAccountService)

UserAccountService.$inject = ['$http'];


function UserAccountService($http) {

   return{

      getUseraccount: function () {
         return $http({method: 'GET', url: 'api/v1/useraccount/get'});
      },

      saveUseraccount:function(username, last_name, first_name, email, sex){
         return $http({method:'POST',url:'api/v1/useraccount/save', data:{'username': username, 'last_name': last_name, 'first_name': first_name, 'email': email, 'sex': sex}});
      },

      changePassword:function(password){
         return $http({method:'POST',url:'api/v1/useraccount/save_pwd', data:{ 'password': password}});
      },

      portraitExists:function(url){
         return $http({method:'HEAD',url:url});
      },

      deletePortrait:function(){
         return $http({method:'GET',url:'api/v1/useraccount/deletePortrait'});
      }

   };
}
