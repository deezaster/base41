'use strict';

angular
   .module('theApp')
   .controller('WebsiteUserAccountPortraitController', WebsiteUserAccountPortraitController);

WebsiteUserAccountPortraitController.$inject = ['$rootScope', '$scope', 'toastr', '$upload', '$timeout', 'DIR_IMG_USER', 'UserAccountService'];


function WebsiteUserAccountPortraitController($rootScope, $scope, toastr, $upload, $timeout, DIR_IMG_USER, UserAccountService) {

   var vm = this;

   UserAccountService.getUseraccount()

      .success(function (data) {

         var user = data["user"];

         vm.id = user.id;
         vm.last_name = user.last_name;
         vm.first_name = user.first_name;
         vm.username = user.username;
         vm.email = user.email;
         vm.sex = user.sex;

      })
      .error(function (data) {
         console.log("WebsiteUserAccountController > ERROR");

         // Errorhandling in: http.service.js
      });


   $scope.upload = function (files) {

      if (files && files.length)
      {
         for (var i = 0; i < files.length; i++)
         {
            var file = files[i];
            $upload.upload({
               url:  'api/v1/useraccount/uploadPortrait',
               //fields: {'id': "1"},
               file: file
            }).progress(function (evt) {
               //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
               //console.log('progress ' + evt.config.file.name + ': ' + file.progress + '% ');
            }).success(function (data, status, headers, config) {
               //console.log('file ' + config.file.name + ' uploaded. Response: ' + data);

               // Portrait-Bild (neu) aufbereiten
               $rootScope.UserPortrait = {imageSrc: DIR_IMG_USER + vm.id + ".jpg"};

               var random =  Math.floor(Math.random() * 10000);
               $rootScope.UserPortrait.random = "?v="+random;

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

               //$scope.picFile = [];
               $scope.remove();
            });
         }
      }

   };

   // Portrait löschen
   $scope.deletePortrait = function () {

      UserAccountService.deletePortrait()
         .success(function (data) {

            // Portrait-Bild (neu) aufbereiten
            var imgName = "male.jpg";
            if (vm.sex == 'm')
            {
               imgName = "male.jpg";
            }
            else
            {
               imgName = "female.jpg";
            }

            $rootScope.UserPortrait = {imageSrc: DIR_IMG_USER + imgName};

            var random =  Math.floor(Math.random() * 10000);
            $rootScope.UserPortrait.random = "?v="+random;

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target: data["response"]["container"],
               timeOut: parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         })
         .error(function (data) {
         });
   };

   $scope.remove = function () {
      // clear array
      $scope.picFile = [];
   };

   $scope.fileReaderSupported = window.FileReader != null && (window.FileAPI == null || FileAPI.html5 != false);

   $scope.generateThumb = function (file) {
      if (file != null)
      {
         if ($scope.fileReaderSupported && file.type.indexOf('image') > -1)
         {
            $timeout(function () {
               var fileReader = new FileReader();
               fileReader.readAsDataURL(file);
               fileReader.onload = function (e) {
                  $timeout(function () {
                     file.dataUrl = e.target.result;
                  });
               }
            });
         }
      }
   }


}









