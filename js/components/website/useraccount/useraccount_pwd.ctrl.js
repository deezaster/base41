'use strict';

angular
   .module('theApp')
   .controller('WebsiteUserAccountPwdController', WebsiteUserAccountPwdController);

WebsiteUserAccountPwdController.$inject = ['toastr', '$location', '$sanitize', 'UserAccountService'];


function WebsiteUserAccountPwdController(toastr, $location, $sanitize, UserAccountService) {

   var vm = this;

      UserAccountService.getUseraccount()

         .success(function (data) {
            var user = data["user"];

            vm.last_name = user.last_name;
            vm.first_name = user.first_name;

         })
         .error(function (data) {
            // Errorhandling in: http.service.js
         });


   vm.cancel = function()
   {
      $location.path('/');
   };

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         UserAccountService.changePassword(
            $sanitize(vm.password))

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

            });
      }

   };

}









