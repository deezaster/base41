'use strict';

angular
   .module('theApp')
   .controller('WebsiteLoginController', WebsiteLogin)
   .controller('WebsiteRegisterController', WebsiteRegister)
   .controller('WebsiteActivateAccountController', WebsiteActivateAccount)
   .controller('WebsiteRequestPwdController', WebsiteRequestPwd)
   .controller('WebsiteResetPwdController', WebsiteResetPwd);

WebsiteLogin.$inject = ['DIR_IMG_USER', '$rootScope', '$sanitize', '$location', 'AuthService', 'localStorageService', 'toastr'];
WebsiteRegister.$inject = ['$sanitize', '$location', 'AuthService', 'toastr'];
WebsiteActivateAccount.$inject = ['$sanitize', '$location', 'AuthService', 'toastr'];
WebsiteRequestPwd.$inject = ['$sanitize', '$location', 'AuthService', 'toastr'];


function WebsiteLogin(DIR_IMG_USER, $rootScope, $sanitize, $location, AuthService, localStorageService, toastr) {

   var vm = this;
   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AuthService.login(
            $sanitize(vm.username),
            $sanitize(vm.password),
            $sanitize(vm.remember))

            .success(function (data) {

               localStorageService.clearAll();
               localStorageService.set("isLoggedin", 'y');
               localStorageService.set("userDisplay", data["user"]["first_name"] + " " + data["user"]["last_name"]);
               localStorageService.set("userEmail", data["user"]["email"]);
               localStorageService.set("userId", data["user"]["id"]);

               if (data["user"]["is_admin"] == 'y')
               {
                  localStorageService.set("isAdmin", 'y');
               }
               else
               {
                  localStorageService.set("isAdmin", 'n');
               }


               // Portrait-Bild aufbereiten
               // var user = data["user"];
               var imgName = "male.jpg";

               if (data["user"]["has_portrait"] == 'y')
               {
                  imgName = data["user"]["id"] + ".jpg";
               }
               else
               {
                  if (data["user"]["sex"] == 'm')
                  {
                     imgName = "male.jpg";
                  }
                  else
                  {
                     imgName = "female.jpg";
                  }
               }

               localStorageService.set("userPortrait",DIR_IMG_USER + imgName );
               $rootScope.UserPortrait = {imageSrc: localStorageService.get("userPortrait")};


               //console.log("auth.ctrl.js > rootScope.UserPortrait = " + $rootScope.UserPortrait);
               //console.log("auth: localstorage userportrait: " + localStorageService.get("userPortrait"));


               // Redirect to...
               if (localStorageService.get("isAdmin") == 'y')
               {
                  $location.path('/admin');
                  //$window.location.href = URL_ADMIN;
               }
               else
               {
                  $location.path('/');
                  //$window.location.href = URL_LOGGEDIN;
               }


               var msg = {
                  'title': 'WILLKOMMEN',
                  'msg':   "Hallo " + data["user"]["first_name"] + " " + data["user"]["last_name"],
                  'type':  'success',
                  'container': "body",
                  'duration': "5000",
                  'placement': 'toast-top-right'
               };

               //localStorageService.set("msg", msg);
               toastr.show(msg["type"], msg["msg"], msg["title"], {
                  target: msg["container"],
                  timeOut: parseInt(msg["duration"]),
                  positionClass: msg["placement"]
               });


            })
            .error(function (data) {
               document.getElementById("inputUsername").focus();

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

            });
      }

   };

}

function WebsiteRegister($sanitize, $location, AuthService, toastr) {

   var vm = this;

   vm.sexEnums = [
      {value: 'm', label: 'Mann'},
      {value: 'w', label: 'Frau'}
   ];

   vm.sex = 'm';

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AuthService.register(
            $sanitize(vm.username),
            $sanitize(vm.password),
            $sanitize(vm.password_confirm),
            $sanitize(vm.last_name),
            $sanitize(vm.first_name),
            $sanitize(vm.email),
            $sanitize(vm.sex))

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });


               if (data["response"]["type"] == "success")
               {
                  $location.path('/auth/login')
               }

            });
      }

   };

}

function WebsiteActivateAccount($sanitize, $location, AuthService, toastr) {

   var vm = this;

   // $location.path() => /auth/activateaccount/HVjtu4CHXA4htfIxI65ZOdLMgQStQyJqniD8i5Rs04
   var code = $location.path().split("/")[3] || "???";

   if (code != "???")
   {
      AuthService.activateaccount(code)

         .success(function (data) {

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target: data["response"]["container"],
               timeOut: parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

            if (data["response"]["type"] == "success")
            {
               $location.path('/auth/login')
            }
            else
            {
               vm.activationcode = code;
            }

         });
   }


   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AuthService.activateaccount($sanitize(vm.activationcode))

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

               if (data["response"]["type"] == "success")
               {
                  $location.path('/auth/login')
               }

            });
      }

   };

}

function WebsiteRequestPwd($sanitize, $location, AuthService, toastr) {

   var vm = this;
   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AuthService.reqpwd(
            $sanitize(vm.username),
            $sanitize(vm.password),
            $sanitize(vm.password_confirm),
            $sanitize(vm.last_name),
            $sanitize(vm.first_name),
            $sanitize(vm.email))

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

               if (data["response"]["type"] == "success")
               {
                  $location.path('/auth/login')
               }

            });
      }

   };

}
function WebsiteResetPwd($sanitize, $location, AuthService, toastr) {

   var vm = this;

   // $location.path() => /auth/resetpwd/HVjtu4CHXA4htfIxI65ZOdLMgQStQyJqniD8i5Rs04
   var code = $location.path().split("/")[3] || "???";

   if (code != "???")
   {
      vm.resetcode = code;
   }
   else
   {
      vm.resetcode = "";
   }


   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AuthService.resetpwd($sanitize(vm.resetcode), $sanitize(vm.password), $sanitize(vm.password_confirm))

            .success(function (data) {

               toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
                  target: data["response"]["container"],
                  timeOut: parseInt(data["response"]["duration"]),
                  positionClass: data["response"]["placement"]
               });

               if (data["response"]["type"] == "success")
               {
                  $location.path('/auth/login')
               }


            });
      }

   };

}








