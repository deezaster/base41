'use strict';

angular
   .module('theApp')
   .factory('AuthService', AuthService)

AuthService.$inject = ['$http'];


function AuthService($http) {

   return{

      isLoggedin:function(){
         return $http({method:'GET',url:'api/v1/auth/isLoggedin'});
      },

      login:function(usr, pwd, remember){
         return $http({method:'POST',url:'api/v1/auth/login', data:{ 'username': usr, 'password': pwd, 'remember': remember}});
      },

      logoff:function(){
         return $http({method:'GET',url:'api/v1/auth/logoff'});
      },

      register:function(usr, pwd, pwd_confirm, last_name, first_name, email, sex){
         return $http({method:'POST',url:'api/v1/auth/register', data:{ 'username': usr, 'password': pwd, 'password_confirm': pwd_confirm, 'last_name': last_name, 'first_name': first_name, 'email': email, 'sex': sex}});
      },

      activateaccount:function(code){
         return $http({method:'POST',url:'api/v1/auth/activateaccount', data:{ 'activation_code': code}});
      },

      reqpwd:function(usr){
         return $http({method:'POST',url:'api/v1/auth/requestpwd', data:{ 'username': usr}});
      },

      resetpwd:function(code, pwd, pwd_confirm){
         return $http({method:'POST',url:'api/v1/auth/resetpwd', data:{ 'reset_code': code, 'password': pwd, 'password_confirm': pwd_confirm}});
      },

      isUsernameUnique:function(usr, except){
         return $http({method:'POST',url:'api/v1/auth/register/isUsernameUnique', data:{ 'username': usr, 'except': except}});
      },

      isEmailUnique:function(email, except){
         return $http({method:'POST',url:'api/v1/auth/register/isEmailUnique', data:{ 'email': email, 'except': except}});
      },

      groups:function(){
         return $http({method:'GET',url:'api/v1/auth/groups'});
      }

   };
}
