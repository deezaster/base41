angular
   .module('theApp')
   .directive('x3mEqual', x3mEqual)
   .directive('x3mUniqueUsername', x3mUniqueUsername)
   .directive('x3mUniqueEmail', x3mUniqueEmail)
   .directive('x3mEmail', x3mEmail);

x3mUniqueEmail.$inject = ['AuthService', '$parse'];
x3mUniqueUsername.$inject = ['AuthService', '$parse'];


// Prüft ob 2 Felder identisch sind (ideal für Passwort und Passwort wiederholen)
function x3mEqual() {

   var directive = {
      restrict: 'A',
      require:  'ngModel',
      link:     link
   };

   return directive;

   function link(scope, element, attrs, ngModel) {

      if (!ngModel)
      {
         return;
      }

      attrs.$observe('x3mEqual', function () {
         ngModel.$validate();
      });

      // value = Wert 2. Feld
      // attrs.x3mEqual = Wert 1. Feld
      ngModel.$validators.equals = function (value) {
         return value === attrs.x3mEqual;
      };

   }
}

// Prüft via AuthService, ob die Emailadresse schon vergeben ist
function x3mUniqueEmail(AuthService, $parse) {

   var directive = {
      restrict: 'A',
      require:  'ngModel',
      link:     link
   };

   return directive;

   function link(scope, element, attrs, ngModel) {

      element.bind('blur', function (e) {

         if (!ngModel || !element.val())
         {
            return;
         }

         var currentValue = element.val();

         var value = $parse(attrs.x3mUniqueEmail)(scope);
         var except = false;
         if (value)
         {
            if (value == "except_me")
            {
               // Das Attribut "except_me" bedeutet, dass die E-Mailadresse des aktuell angemeldeten Benutzers nicht überprüft wird
               except = true;
            }
            if (value.length > 0)
            {
               // Eine E-Mailadresse, die bei der Validierung ignoriert wird
               except = value;
            }
         }

         AuthService.isEmailUnique(currentValue, except)

            .success(function (data) {
               //Ensure value that being checked hasn't changed since the Ajax call was made
               if (currentValue == element.val())
               {
                  ngModel.$setValidity('unique', data["response"]);
               }
            }, function () {
               //Probably want a more robust way to handle an error. For this demo we'll set unique to true though
               ngModel.$setValidity('unique', true);
            });

      });
   }
}

// Prüft via AuthService, ob der Benutzername schon vergeben ist
function x3mUniqueUsername(AuthService, $parse) {

   var directive = {
      restrict: 'A',
      require:  'ngModel',
      link:     link
   };

   return directive;

   function link(scope, element, attrs, ngModel) {

      element.bind('blur', function (e) {

         if (!ngModel || !element.val())
         {
            return;
         }

         var currentValue = element.val();

         var value = $parse(attrs.x3mUniqueUsername)(scope);
         var except = false;
         if (value)
         {
            if (value == "except_me")
            {
               // Das Attribut "except_me" bedeutet, dass der Benutzername des aktuell angemeldeten Benutzers nicht überprüft wird
               except = true;
            }
            if (value.length > 0)
            {
               // Eine E-Mailadresse, die bei der Validierung ignoriert wird
               except = value;
            }
         }

         AuthService.isUsernameUnique(currentValue, except)

            .success(function (data) {
               //Ensure value that being checked hasn't changed since the Ajax call was made
               if (currentValue == element.val())
               {
                  ngModel.$setValidity('unique', data["response"]);
               }
            }, function () {
               //Probably want a more robust way to handle an error. For this demo we'll set unique to true though
               ngModel.$setValidity('unique', true);
            });


      });
   }
}


// Erweitert den input[email] Validator
//
// Hätte man auch mit der bestehenden Direktiven ng-pattern lösen können,
// anstatt eine eigene Direktive zu schreiben ;-)
function x3mEmail() {

   var EMAIL_REGEXP = /^[a-z0-9\._-]+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z]+\.)?([a-z]+)$/i;

   return {
      require:  'ngModel',
      restrict: '',
      link:     function (scope, elm, attrs, ctrl) {

         // only apply the validator if ngModel is present and Angular has added the email validator
         if (ctrl && ctrl.$validators.email)
         {

            // this will overwrite the default Angular email validator
            ctrl.$validators.email = function (modelValue) {
               return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
            };
         }
      }
   };
}
