'use strict';

angular
   .module('theApp')
   .factory('httpInterceptor', httpInterceptor);


httpInterceptor.$inject = ['$location', '$q', 'localStorageService', '$injector'];


function httpInterceptor($location, $q, localStorageService, $injector) {

   return {

      'request': function(config) {
         return config;
      },

      'requestError': function(rejection) {
         return $q.reject(rejection);
      },

      'response': function(response) {
         return response;
      },

      'responseError': function(response) {

         // injected manually to get around circular dependency problem.
         var toastr = $injector.get('toastr');


         // 400 allgemeiner Fehler
         // 401 Login failed
         // 403 not loggedin (timeout?)
         // 418 hacker? no csrf_token found!
         // 500 Fehler Backend
         if (response.status == 400){

            //console.log("CATCH HTTP 400");

            localStorageService.clearAll();

            toastr.show(response.data["response"]["type"], response.data["response"]["msg"], response.data["response"]["title"], {
               target: response.data["response"]["container"],
               timeOut: parseInt(response.data["response"]["duration"]),
               positionClass: response.data["response"]["placement"]
            });

            //$location.path('/auth/login');
         }
         if (response.status == 403){

            // sicherstellen, dass jeweils nur 1 x den 403 abgehandelt wird. sonst erscheinen zu viele toastr ;-)
            if (!localStorageService.get("hasLoggedout"))
            {
               localStorageService.set("hasLoggedout", 'y');

               //console.log("CATCH HTTP 403");
               toastr.show(response.data["response"]["type"], response.data["response"]["msg"], response.data["response"]["title"], {
                  target:        response.data["response"]["container"],
                  timeOut:       parseInt(response.data["response"]["duration"]),
                  positionClass: response.data["response"]["placement"]
               });
            }
            localStorageService.remove("isLoggedin");
            localStorageService.remove("isAdmin");
            $location.path('/auth/login');
         }
         if (response.status == 418){

            localStorageService.clearAll();

            //console.log("CATCH HTTP 418");
            toastr.show(response.data["response"]["type"], response.data["response"]["msg"], response.data["response"]["title"], {
               target: response.data["response"]["container"],
               timeOut: parseInt(response.data["response"]["duration"]),
               positionClass: response.data["response"]["placement"]
            });

            $location.path('/auth/login');
         }
         if (response.status == 500){

            localStorageService.clearAll();

            //console.log("CATCH HTTP 500");

            toastr.show("error", "Ein bisher unbekannter Fehler ist aufgetreten. Versuche es nochmals oder wende dich direkt an den Support.", "FEHLER");

            $location.path('/');
         }

         return $q.reject(response)
      }
   };
}
