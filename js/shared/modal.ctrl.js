'use strict';

angular
   .module('theApp')
   .controller('ModalController', ModalController);

ModalController.$inject = ['$scope', 'title', 'msg1', 'msg2', 'close'];

function ModalController($scope, title, msg1, msg2, close) {

   $scope.title = title;
   $scope.msg1 = msg1;
   $scope.msg2 = msg2;

   $scope.close = function (result) {
      close(result, 500); // close, but give 500ms for bootstrap to animate
   };

}