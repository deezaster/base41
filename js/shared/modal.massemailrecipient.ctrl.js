'use strict';

angular
   .module('theApp')
   .controller('ModalMassEmailRecipientController', ModalMassEmailRecipientController);

ModalMassEmailRecipientController.$inject = ['$alert', 'close', '$sanitize', 'AdminMassEmailService'];


function ModalMassEmailRecipientController($alert, close, $sanitize, AdminMassEmailService) {

   var vm = this;

   vm.submitForm = function (isValid) {

      if (isValid)
      {
         AdminMassEmailService.insertRecipient(
            $sanitize(vm.email),
            $sanitize(vm.name),
            null)

            .success(function (data) {

               $alert({
                  title:     data["response"]["title"],
                  content:   data["response"]["msg"],
                  type:      data["response"]["type"],
                  placement: data["response"]["placement"],
                  container: data["response"]["container"],
                  keyboard:  true,
                  animation: 'am-fade-and-slide-top',
                  duration:  data["response"]["duration"]
               });

               vm.close();
            });

      }

   };

   vm.close = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };


   vm.cancel = function () {
      close({}, 500); // close, but give 500ms for bootstrap to animate
   };

}