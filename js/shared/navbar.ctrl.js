'use strict';

angular
   .module('theApp')
   .controller('WebsiteNavbarController', WebsiteNavbar);

WebsiteNavbar.$inject = ['$location', 'AuthService', 'localStorageService', 'toastr', 'APP_VERSION'];


function WebsiteNavbar($location, AuthService, localStorageService, toastr, APP_VERSION) {

   var vm = this;

   vm.app_version = APP_VERSION;
   vm.userDisplay = localStorageService.get("userDisplay");

   vm.isActive = function (location) {
      return location === $location.path();
   };

   vm.isLoggedin = function () {
      return localStorageService.get("isLoggedin") == 'y';
   };

   vm.isAdmin = function () {
      return localStorageService.get("isAdmin") == 'y';
   };


   vm.logoff = function () {

      localStorageService.clearAll();
      localStorageService.set("hasLoggedout", 'y');

      AuthService.logoff()
         .success(function (data) {

            $location.path('/');

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target: data["response"]["container"],
               timeOut: parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         })
         .error(function (data) {

            $location.path('/');

            toastr.show(data["response"]["type"], data["response"]["msg"], data["response"]["title"], {
               target: data["response"]["container"],
               timeOut: parseInt(data["response"]["duration"]),
               positionClass: data["response"]["placement"]
            });

         });


   };
}









