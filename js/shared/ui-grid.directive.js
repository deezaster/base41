angular
   .module('theApp')
   .directive('x3mCompileHtml', x3mCompileHtml);

x3mCompileHtml.$inject = ['$compile'];


function x3mCompileHtml($compile) {

   var directive = {
      restrict: 'A',
      replace: true,
      link:     link
   };

   return directive;

   function link(scope, element, attrs) {

      attrs.$observe('x3mCompileHtml', function (val) {

         element.html(val);
         $compile(element.contents())(scope);
      });

   }

};