# BASE41 - WebAppStarter Projekt

BASE41 ist ein Starterprojekt für Webapplikationen, realisiert mit **Laravel 4.2** (als Backendservice) und **AngularJS 1.3** (als Frontendkomponente).

Demo: [www.x3m.ch/base41](http://www.x3m.ch/base41)


![ScreenShot](https://raw.githubusercontent.com/deezaster/BASE41/master/doc/base41_login.png)

![ScreenShot](https://raw.githubusercontent.com/deezaster/BASE41/master/doc/base41_admin_user.png)

![ScreenShot](https://raw.githubusercontent.com/deezaster/BASE41/master/doc/base41_filemanager.png)

![ScreenShot](https://raw.githubusercontent.com/deezaster/BASE41/master/doc/base41_send_notification.png)


 
## Frameworks  & Libraries ##

### Backend: Laravel 4.2 ####

* **Laravel 4.2** - http://http://laravel.com/http://laravel.com/
* **Sentry** - the authentication & authorization system: https://github.com/cartalyst/sentry

### Frontend: AngularJS 1.3+ ###

* **AngularJS 1.3+** [https://github.com/angular/angular.js](https://github.com/angular/angular.js)
* **Angular Strap** - Native directives for Bootstrap 3: [https://github.com/mgcrea/angular-strap](https://github.com/mgcrea/angular-strap)
* **Bootstrap 3** [http://getbootstrap.com](http://getbootstrap.com)
* **UI Grid** - An Angular Data Grid: [https://github.com/angular-ui/ng-grid](https://github.com/angular-ui/ng-grid) 
* **Angular Modal Service** [https://github.com/dwmkerr/angular-modal-service](https://github.com/dwmkerr/angular-modal-service)
* **Angular Loading Bar** [https://chieffancypants.github.io/angular-loading-bar](https://chieffancypants.github.io/angular-loading-bar)
* **Angular Toastr** [https://github.com/Foxandxss/angular-toastr](https://github.com/Foxandxss/angular-toastr)
* **Angular Local Storage** [https://github.com/grevory/angular-local-storage](https://github.com/grevory/angular-local-storage)
* **Angular File Upload** [https://github.com/danialfarid/angular-file-upload](https://github.com/danialfarid/angular-file-upload)
* **Angular Tooltips** [https://github.com/720kb/angular-tooltips](https://github.com/720kb/angular-tooltips)
* **textAngular** [https://github.com/fraywing/textAngular](https://github.com/fraywing/textAngular)
* **Angular JS Tree** [https://github.com/ezraroi/ngJsTree](https://github.com/ezraroi/ngJsTree)
* **Angular JS ui-select** [https://github.com/angular-ui/ui-select](https://github.com/angular-ui/ui-select)
* **Bootstrap Theme: Spacelab** [http://bootswatch.com/spacelab/](http://bootswatch.com/spacelab/)
* **JavaScript Date Library** [https://github.com/moment/moment/](https://github.com/moment/moment/)






# Installation #

## 1. Voraussetzungen
Das Versionsverwaltungssystem **Git** (http://git-scm.com) und der PHP Dependency Manager **composer** (https://getcomposer.org) sind installiert.


## 2. Installation

### 2.1. Download Projekt
Projekt klonen oder downloaden und in das Webrootverzeichnis des Webservers kopieren.

Composer starten:
```bat
composer update
```
oder
```bat
php composer.phar update
```

### 2.2. Datenbank

Eine Datenbank anlegen und die Konfiguration in **app/config/database.php** anpassen (und evtl. auch in app/config/local/database.php).

#### 2.3. Tabellen erstellen

```bat
php artisan migrate
```

#### 2.4. Initialload

```bat
php artisan db:seed
```
 

## 3. Konfiguration

Im Verzeichnis **app/config** (und evtl. auch in app/config/local) folgende Dateien bei Bedarf anpassen:

* app.php
* mail.php
* log.php

## 4. Testbenutzer

Der Initialload (siehe Kapitel 2.4.) hat folgende 2 Benutzer angelegt:

+ Benutzer: **admin** Password: **admin** (In der Gruppe **Administrator**)
+ Benutzer: **user1** Password: **user** (In der Gruppe **User**)
+ Benutzer: **user2** Password: **user** (In der Gruppe **User**, Account nicht aktiviert)
+ Benutzer: **user3** Password: **user** (In der Gruppe **User**)
+ Benutzer: **user4** Password: **user** (In der Gruppe **User**)
+ Benutzer: **user5** Password: **user** (In der Gruppe **User**, Account gesperrt)

